package org.offjob.boss.domain.knowledge.dao;

import org.offjob.boss.domain.dao.BaseMapper;

public interface TypeMapper<T> extends BaseMapper<T> {
}
