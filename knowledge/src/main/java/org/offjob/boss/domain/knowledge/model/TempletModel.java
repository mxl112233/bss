package org.offjob.boss.domain.knowledge.model;

import org.offjob.boss.domain.model.BaseModel;

public class TempletModel extends BaseModel{

       private Integer id;  
       private String name;  
       private String comments;  
       private Integer hasSummary;  
       private Integer hasInfo;  
       private Integer hasText;  
       private Integer hasDocument;  
       private Integer hasVideo;  
       private Integer hasAudio;  
       private Integer hasTinyint;  
   	
   	public Integer getId(){
	   return id;
	}
   
	public void setId(Integer id){
	   this.id=id;
	}
	
   	public String getName(){
	   return name;
	}
   
	public void setName(String name){
	   this.name=name;
	}
	
   	public String getComments(){
	   return comments;
	}
   
	public void setComments(String comments){
	   this.comments=comments;
	}
	
   	public Integer getHasSummary(){
	   return hasSummary;
	}
   
	public void setHasSummary(Integer hasSummary){
	   this.hasSummary=hasSummary;
	}
	
   	public Integer getHasInfo(){
	   return hasInfo;
	}
   
	public void setHasInfo(Integer hasInfo){
	   this.hasInfo=hasInfo;
	}
	
   	public Integer getHasText(){
	   return hasText;
	}
   
	public void setHasText(Integer hasText){
	   this.hasText=hasText;
	}
	
   	public Integer getHasDocument(){
	   return hasDocument;
	}
   
	public void setHasDocument(Integer hasDocument){
	   this.hasDocument=hasDocument;
	}
	
   	public Integer getHasVideo(){
	   return hasVideo;
	}
   
	public void setHasVideo(Integer hasVideo){
	   this.hasVideo=hasVideo;
	}
	
   	public Integer getHasAudio(){
	   return hasAudio;
	}
   
	public void setHasAudio(Integer hasAudio){
	   this.hasAudio=hasAudio;
	}
	
   	public Integer getHasTinyint(){
	   return hasTinyint;
	}
   
	public void setHasTinyint(Integer hasTinyint){
	   this.hasTinyint=hasTinyint;
	}
	
       public String toString(){
	  return com.alibaba.fastjson.JSON.toJSONString(this);
    }
	
}