package org.offjob.boss.domain.knowledge.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import org.offjob.boss.domain.knowledge.dao.TypeMapper;
import org.offjob.boss.domain.service.BaseServiceImpl;

@Service("typeService")
public class TypeServiceImpl<T> extends BaseServiceImpl<T>{
	
	@Resource(name = "typeMapper")
    private TypeMapper<T> mapper;
    
    public TypeMapper<T> getMapper() {
		return mapper;
	}
}
