package org.offjob.boss.domain.knowledge.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import org.offjob.boss.domain.knowledge.dao.TempletMapper;
import org.offjob.boss.domain.service.BaseServiceImpl;

@Service("templetService")
public class TempletServiceImpl<T> extends BaseServiceImpl<T>{
	
	@Resource(name = "templetMapper")
    private TempletMapper<T> mapper;
    
    public TempletMapper<T> getMapper() {
		return mapper;
	}
}
