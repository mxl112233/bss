package org.offjob.boss.domain.knowledge.model;

import org.offjob.boss.domain.model.BaseModel;

public class TypeModel extends BaseModel{

       private Integer id;  
       private String name;  
       private Integer parentId;  
       private String comments;  
       private Integer templetId;  
   	
   	public Integer getId(){
	   return id;
	}
   
	public void setId(Integer id){
	   this.id=id;
	}
	
   	public String getName(){
	   return name;
	}
   
	public void setName(String name){
	   this.name=name;
	}
	
   	public Integer getParentId(){
	   return parentId;
	}
   
	public void setParentId(Integer parentId){
	   this.parentId=parentId;
	}
	
   	public String getComments(){
	   return comments;
	}
   
	public void setComments(String comments){
	   this.comments=comments;
	}
	
   	public Integer getTempletId(){
	   return templetId;
	}
   
	public void setTempletId(Integer templetId){
	   this.templetId=templetId;
	}
	
       public String toString(){
	  return com.alibaba.fastjson.JSON.toJSONString(this);
    }
	
}