package org.offjob.boss.view.knowledge;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.offjob.boss.utils.NumberUtils;
import org.offjob.boss.utils.PageParams;
import org.offjob.boss.view.BaseController;
import org.offjob.boss.domain.knowledge.model.TypeModel;
import org.offjob.boss.domain.knowledge.service.TypeServiceImpl;



@Controller
@RequestMapping("/admin/knowledge/Type")
public class TypeController extends BaseController {
	private final static Logger log = Logger
			.getLogger(TypeController.class);
	
	@Resource(name="typeService")
	private TypeServiceImpl<TypeModel> typeService;

	@RequestMapping("index.htm")
	public ModelAndView index(String id, ModelMap modelMap,
			HttpServletRequest request) {
				return new ModelAndView("view/admin/knowledge/Type/index", modelMap);
			}

	@RequestMapping("baseDlg.htm")
	public String baseDlg() {
				return "view/admin/knowledge/Type/baseDlg";
			}

	@RequestMapping("data.htm")
	@ResponseBody
	public String data(PageParams pageParams, TypeModel typeModel)
			throws Exception {
		log.info("pageParams:" + pageParams + "|tbsTypeModel:"
				+ typeModel);
		typeModel.getPageUtil().setPageId(NumberUtils.parseInt(pageParams.getPage(), 1)); // 当前页
		typeModel.getPageUtil().setPageSize(NumberUtils.parseInt(pageParams.getRows(), 10));// 显示X条

		StringBuilder center = new StringBuilder();
		if ("1".equalsIgnoreCase(pageParams.getSearchType())) {
			typeModel.getPageUtil().setLike(false);
		}
		List<TypeModel> listTypeModel = typeService.selectByModelPaging(typeModel);
		center.append("{\"total\":\""+ typeModel.getPageUtil().getRowCount()+ "\",\"rows\":");
		if (listTypeModel == null || listTypeModel.isEmpty()) {
			center.append("[]");
		} else {
			center.append(JSON.toJSONString(listTypeModel,SerializerFeature.WriteDateUseDateFormat));
		}
		center.append("}");
		return center.toString();
	}

	@RequestMapping("add.htm")
	public void add(TypeModel typeModel, HttpServletResponse response) {
		log.info("typeModel:" + typeModel.toString());
		try {
			if (typeService.insert(typeModel) > 0) {
				this.toJsonMsg(response, 0, null);
				return;
			}
		} catch (Exception e) {
			log.error("typeService.insert fail.......",e);
		}
		this.toJsonMsg(response, 1, null);
	}

	@RequestMapping("save.htm")
	public void save(TypeModel typeModel, HttpServletResponse response) {
		try {
			if (typeService.updateByPrimaryKey(typeModel) > 0) {
				this.toJsonMsg(response, 0, null);
				return;
			}
		} catch (Exception e) {
			log.error("typeService.updateByPrimaryKey fail.......",e);
			this.toJsonMsg(response, 1, null);
		}
	}

	@RequestMapping("del.htm")
		public void del(Integer[] ids, HttpServletResponse response) {
    		log.info("del-ids:" + Arrays.toString(ids));
		try {
			if (null != ids && ids.length > 0&&typeService.deleteByPrimaryKeys(ids) > 0) {
				this.toJsonMsg(response, 0, null);
			}
		} catch (Exception e) {
			this.toJsonMsg(response, 1, null);
			log.error("del fail.....",e);
		}
	}
}
