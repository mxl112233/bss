package org.offjob.boss.view.knowledge;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.offjob.boss.utils.NumberUtils;
import org.offjob.boss.utils.PageParams;
import org.offjob.boss.view.BaseController;
import org.offjob.boss.domain.knowledge.model.TempletModel;
import org.offjob.boss.domain.knowledge.service.TempletServiceImpl;



@Controller
@RequestMapping("/admin/knowledge/Templet")
public class TempletController extends BaseController {
	private final static Logger log = Logger
			.getLogger(TempletController.class);
	
	@Resource(name="templetService")
	private TempletServiceImpl<TempletModel> templetService;

	@RequestMapping("index.htm")
	public ModelAndView index(String id, ModelMap modelMap,
			HttpServletRequest request) {
				return new ModelAndView("view/admin/knowledge/Templet/index", modelMap);
			}

	@RequestMapping("baseDlg.htm")
	public String baseDlg() {
				return "view/admin/knowledge/Templet/baseDlg";
			}

	@RequestMapping("data.htm")
	@ResponseBody
	public String data(PageParams pageParams, TempletModel templetModel)
			throws Exception {
		log.info("pageParams:" + pageParams + "|tbsTempletModel:"
				+ templetModel);
		templetModel.getPageUtil().setPageId(NumberUtils.parseInt(pageParams.getPage(), 1)); // 当前页
		templetModel.getPageUtil().setPageSize(NumberUtils.parseInt(pageParams.getRows(), 10));// 显示X条

		StringBuilder center = new StringBuilder();
		if ("1".equalsIgnoreCase(pageParams.getSearchType())) {
			templetModel.getPageUtil().setLike(false);
		}
		List<TempletModel> listTempletModel = templetService.selectByModelPaging(templetModel);
		center.append("{\"total\":\""+ templetModel.getPageUtil().getRowCount()+ "\",\"rows\":");
		if (listTempletModel == null || listTempletModel.isEmpty()) {
			center.append("[]");
		} else {
			center.append(JSON.toJSONString(listTempletModel,SerializerFeature.WriteDateUseDateFormat));
		}
		center.append("}");
		return center.toString();
	}

	@RequestMapping("add.htm")
	public void add(TempletModel templetModel, HttpServletResponse response) {
		log.info("templetModel:" + templetModel.toString());
		try {
			if (templetService.insert(templetModel) > 0) {
				this.toJsonMsg(response, 0, null);
				return;
			}
		} catch (Exception e) {
			log.error("templetService.insert fail.......",e);
		}
		this.toJsonMsg(response, 1, null);
	}

	@RequestMapping("save.htm")
	public void save(TempletModel templetModel, HttpServletResponse response) {
		try {
			if (templetService.updateByPrimaryKey(templetModel) > 0) {
				this.toJsonMsg(response, 0, null);
				return;
			}
		} catch (Exception e) {
			log.error("templetService.updateByPrimaryKey fail.......",e);
			this.toJsonMsg(response, 1, null);
		}
	}

	@RequestMapping("del.htm")
		public void del(Integer[] ids, HttpServletResponse response) {
    		log.info("del-ids:" + Arrays.toString(ids));
		try {
			if (null != ids && ids.length > 0&&templetService.deleteByPrimaryKeys(ids) > 0) {
				this.toJsonMsg(response, 0, null);
			}
		} catch (Exception e) {
			this.toJsonMsg(response, 1, null);
			log.error("del fail.....",e);
		}
	}
}
