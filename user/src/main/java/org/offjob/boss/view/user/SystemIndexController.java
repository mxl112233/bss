package org.offjob.boss.view.user;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.offjob.boss.utils.Contants.StringCharsetContants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.util.IOUtils;
import org.offjob.boss.domain.user.model.LoginLogModel;
import org.offjob.boss.domain.user.model.MenuModel;
import org.offjob.boss.domain.user.model.UserModel;
import org.offjob.boss.domain.user.service.LoginLogServiceImpl;
import org.offjob.boss.domain.user.service.MenuServiceImpl;
import org.offjob.boss.domain.user.service.UserServiceImpl;
import org.offjob.boss.utils.MD5Coder;
import org.offjob.boss.utils.MapBeanUtil;
import org.offjob.boss.utils.MethodUtil;
import org.offjob.boss.view.BaseController;
import org.offjob.boss.view.interceptor.SessionUtil;
import org.offjob.boss.view.interceptor.UserToken;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

@Controller
@RequestMapping("/admin")
public class SystemIndexController extends BaseController {
	private final static Logger log = Logger.getLogger(SystemIndexController.class);

	@Resource(name = "userService")
	private UserServiceImpl<UserModel> tbsUserService;
	@Resource(name = "menuService")
	private MenuServiceImpl<MenuModel> tbsMenuService;
	@Resource(name = "loginLogService")
	private LoginLogServiceImpl<LoginLogModel> tbsLoginLogService;

	@Value("#{settings[common_login_debug]}")
	private Boolean isDebug;

	@RequestMapping(value = "/login.htm", method = RequestMethod.GET)
	public String from(HttpServletRequest request, ModelMap modelMap) {
		UserToken tbsUserModel = SessionUtil.getUser(request);
		if (tbsUserModel == null || !tbsUserModel.isHasLogin()) {
			showtitle(modelMap);
			return "/admin/login";
		}
		return "redirect:/admin/index.htm";
	}

	@RequestMapping(value = "/portal.htm")
	public String portal(HttpServletRequest request, ModelMap modelMap) {
		return "/admin/portal";
	}

	@RequestMapping(value = "/login.htm", method = RequestMethod.POST)
	public void submit(UserModel tbsUserModel, LoginLogModel tbsLoginLogModel, HttpServletResponse response,
			HttpServletRequest request) throws Exception {
		String sessionVerifyCode = (String) SessionUtil.getVerifyCode(request);// session验证码
		SessionUtil.removeVerifyCode(request);
		String msg;
		String ip = MethodUtil.getInstance().getIpAddr(request);
		String verifyCode = request.getParameter("verifyCode"); // 递交的验证码
		tbsLoginLogModel.setUsername(tbsUserModel.getUsername());
		tbsLoginLogModel.setIp(ip);
		if (isDebug && StringUtils.isEmpty(tbsUserModel.getUsername())) {
			tbsUserModel.setUsername("admin");
			tbsUserModel.setPassword("123456");
		} else {
			tbsUserModel.setIp(ip);
			if (null == sessionVerifyCode || null == verifyCode || verifyCode.trim().length() != 4
					|| !sessionVerifyCode.toUpperCase().equals(verifyCode.toUpperCase())) {
				msg = "验证码有误或已失效";
				this.toJsonMsg(response, 2, msg);
				tbsLoginLogModel.setMsg(msg);
				saveLog(tbsLoginLogModel);
				return;
			}
			if (StringUtils.isEmpty(tbsUserModel.getUsername()) || StringUtils.isEmpty(tbsUserModel.getPassword())) {
				msg = "用户名或密码为空,请重新输入.";
				this.toJsonMsg(response, 2, msg);
				tbsLoginLogModel.setMsg(msg);
				saveLog(tbsLoginLogModel);
				return;
			}
		}
		UserModel model = new UserModel();
		model.setUsername(tbsUserModel.getUsername());
		model.setPassword(MD5Coder.encodeMD5Hex(tbsUserModel.getPassword()).toLowerCase());
		List<UserModel> ltub = tbsUserService.selectByModel(model);
		if (null == ltub || ltub.size() != 1) {
			msg = "用户名密码有误";
			this.toJsonMsg(response, 1, msg);
			//
			tbsLoginLogModel.setMsg(msg);
			saveLog(tbsLoginLogModel);
			return;
		}
		tbsUserModel = ltub.get(0);
		if (null == tbsUserModel.getState() || 1 != tbsUserModel.getState()) {
			msg = "用户状态异常,请联系管理员解锁";
			this.toJsonMsg(response, 1, msg);
			tbsLoginLogModel.setMsg(msg);
			saveLog(tbsLoginLogModel);
			return;
		}

		SessionUtil.createSession(request, tbsUserModel);

		this.toJsonMsg(response, 0, null);
		//
		try {
			tbsLoginLogModel.setStatus(0);// 成功
			tbsLoginLogModel.setMsg("用户:" + tbsUserModel.getUsername() + ",登录成功,角色:"
					+ (tbsUserModel.getIsAdmin() == 0 ? "超级管理员" : "授权管理员"));
			saveLog(tbsLoginLogModel);
			tbsUserModel.setIp(ip);
			tbsUserService.updateByPrimaryKey(tbsUserModel);
		} catch (Exception e) {
			log.error("login save log error....", e);
		}
	}

	private void saveLog(LoginLogModel tbsLoginLogModel) {
		try {
			tbsLoginLogService.insert(tbsLoginLogModel);
		} catch (Exception e) {
			log.error("insert login log error....", e);
		}

	}

	@RequestMapping("/index.htm")
	public String index(HttpServletRequest request, ModelMap modelMap) throws Exception {
		UserToken tbsUserModel = SessionUtil.getUser(request);
		if (!tbsUserModel.isHasLogin()) {
			if (null == tbsUserModel.getIsAdmin() || tbsUserModel.getIsAdmin() != 0) { // 管理员
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("cloumn", "menu_IdFun");
				map.put("userId", tbsUserModel.getId());
				List<Map<String, Object>> childMenu = tbsUserService.selectByRoleUrls(map);
				if (childMenu != null && childMenu.size() > 0) { // 添加授权地址
					Set<String> authUrls = tbsUserModel.getAuthorizedUrl();
					for (int i = 0; i < childMenu.size(); i++) {
						String roleUrls = (String) childMenu.get(i).get("url");
						String[] urls = roleUrls.split("\\,");
						for (int j = 0; j < urls.length; j++) {
							if (StringUtils.isNotEmpty(urls[j])) {
								authUrls.add("/" + urls[j]); // 权限URL
							}
						}
					}
				}
			}
			SessionUtil.updateSession(request, tbsUserModel);
		}
		modelMap.put("UserModel", tbsUserModel);
		showtitle(modelMap);
		return "admin/index";
	}

	private void showtitle(ModelMap modelMap) {
		modelMap.put("websiteTitle", "");
		modelMap.put("websiteName", "");
		modelMap.put("websiteLogo", "");
		modelMap.put("websiteStyle", "");
		modelMap.put("websiteKey", "");
		modelMap.put("websiteDesc", "");
		modelMap.put("websitePublish", "");
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/menu.htm")
	public String menu(HttpServletRequest request, ModelMap modelMap) throws Exception {
		// 其他用户
		UserToken tbsUserModel = SessionUtil.getUser(request);
		List<MenuModel> parentMenu = tbsUserModel.getMenuModelList();
		if (!tbsUserModel.isHasLogin()) {
			MenuModel model = new MenuModel();
			model.getPageUtil().setAndCondition("parent_Id IS NULL and status=0");
			model.getPageUtil().setOrderByCondition("sort_number");
			parentMenu = tbsMenuService.selectByModel(model);
			modelMap.put("UserModel", tbsUserModel);
			if (null != tbsUserModel && 0 == tbsUserModel.getIsAdmin()) { // 管理员
				for (int i = 0; i < parentMenu.size(); i++) {
					Integer id = parentMenu.get(i).getId();
					model = new MenuModel();
					model.setParentId(id);
					model.setStatus(0);
					model.getPageUtil().setOrderByCondition("sort_number");
					List<MenuModel> child = tbsMenuService.selectByModel(model);
					for (int j = 0; j < child.size(); j++) {
						if (parentMenu.get(i).getListTbsMenuModel() == null) {
							parentMenu.get(i).setListTbsMenuModel(new java.util.ArrayList<MenuModel>());
						}
						parentMenu.get(i).getListTbsMenuModel().add(child.get(j));
					}
				}
				tbsUserModel.setMenuModelList(parentMenu);
				tbsUserModel.setHasLogin(true);
				SessionUtil.updateSession(request, tbsUserModel);
				modelMap.put("listMenuModel", parentMenu);
				return "admin/menu";
			}

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("cloumn", "menu_id");
			map.put("userId", tbsUserModel.getId());
			List<Map<String, Object>> childMenu = tbsUserService.selectByRoleUrls(map);
			for (int i = 0; i < parentMenu.size(); i++) { // 主菜单找子菜单
				MenuModel tbsMenuModel = parentMenu.get(i);
				if (null != childMenu && childMenu.size() > 0) {
					for (int j = 0; j < childMenu.size(); j++) {
						Map<String, Object> childMap = childMenu.get(j);
						Integer parentId = (Integer) childMap.get("parentId");
						if (tbsMenuModel != null && tbsMenuModel.getId().equals(parentId)) {
							MenuModel bean = MapBeanUtil.mapToBean(childMap, MenuModel.class);
							tbsUserModel.getAuthorizedUrl().add("/" + bean.getUrl()); // 权限URL
							if (parentMenu.get(i).getListTbsMenuModel() == null) {
								parentMenu.get(i).setListTbsMenuModel(new java.util.ArrayList<MenuModel>());
							}
							parentMenu.get(i).getListTbsMenuModel().add(bean);
							continue;
						}
					}
				}
				if (tbsMenuModel.getType() == 0 && (tbsMenuModel.getListTbsMenuModel() == null
						|| tbsMenuModel.getListTbsMenuModel().size() == 0)) { // 没找到子菜单
					// 删除自己
					parentMenu.remove(i);
					i--;
				}
			}
			tbsUserModel.setMenuModelList(parentMenu);
			tbsUserModel.setHasLogin(true);
			SessionUtil.updateSession(request, tbsUserModel);
		}

		modelMap.put("listMenuModel", parentMenu);
		return "admin/menu";
	}

	@RequestMapping("/exit.htm")
	public String exit(HttpServletRequest request, ModelMap modelMap) {
		SessionUtil.removeSessionAll(request);
		showtitle(modelMap);
		return "/admin/login";
	}

	// @RequestMapping("/dirJson.htm")
	// @ResponseBody
	// public synchronized String dirJson(HttpServletRequest request,
	// HttpServletResponse response)
	// throws UnsupportedEncodingException {
	// String path = request.getSession().getServletContext().getRealPath("/");
	// log.debug("RealPath:" + path);
	// if (sb.length() > 0)
	// sb.delete(0, sb.length());// 清除缓存
	// String commonPathJson = this.getJsonData(new File(path)); // 项目路径Json
	// commonPathJson = "[" + commonPathJson + "]";
	// log.debug("dirJson:" + commonPathJson);
	// return commonPathJson;
	// }

	@RequestMapping("/asyJson.htm")
	@ResponseBody
	public synchronized String asyJson(String id, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String asyJson = null;
		String path = null;
		if (null == id) {
			path = request.getSession().getServletContext().getRealPath("/");
		} else {
			path = new String(new BASE64Decoder().decodeBuffer(URLDecoder.decode(id, StringCharsetContants.CHARSET_UTF8)), StringCharsetContants.CHARSET_UTF8);
		}
		log.debug("id:" + id + "|asyJsonPath:" + path);
		asyJson = "[" + this.getAsyJsonData(new File(path)) + "]";
		log.debug(asyJson);
		return asyJson;
	}

	@RequestMapping("/openFile.htm")
	public String getTreeOpenFile(String path, String id, ModelMap modelMap) throws IOException {
		log.debug("path:" + path);
		modelMap.addAttribute("path", path); // base64加密路径
		path = new String(new BASE64Decoder().decodeBuffer(URLDecoder.decode(id, StringCharsetContants.CHARSET_UTF8)),StringCharsetContants.CHARSET_UTF8);
		File file = new File(path);
		if (!file.exists()) {
			return "error";
		}
		log.debug("path:" + path);
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), StringCharsetContants.CHARSET_UTF8));
		String line = null;
		StringBuffer sb = new StringBuffer();
		while ((line = br.readLine()) != null) {
			sb.append(line).append("\r");
		}
		String text = sb.toString();
		text = text.replaceAll("<", "&lt");
		modelMap.addAttribute("str", text);
		modelMap.addAttribute("id", MethodUtil.getInstance().getMD5(id, null, 1));
		return "admin/SystemTree/openFile";
	}

	@RequestMapping("/saveFile.htm")
	public void getTreeSave(String path, HttpServletRequest request, HttpServletResponse response)
			throws UnsupportedEncodingException, IOException {
		// if (true) {
		// this.toJsonMsg(response, 1, "演示项目不允许被修改"); // 不让文件被恶意修改
		// return;
		// }
		if (null == path) { // 路径为空
			this.toJsonMsg(response, 1, null);
			return;
		}
		path = new String(new BASE64Decoder().decodeBuffer(URLDecoder.decode(path, StringCharsetContants.CHARSET_UTF8)),StringCharsetContants.CHARSET_UTF8);
		String textarea = request.getParameter("textarea") == null ? "" : request.getParameter("textarea"); // 写入内容
		textarea = textarea.trim();
		log.debug("savePath:" + path);
		// log.debug("textarea:" + textarea);
		File file = new File(path);
		if (!file.exists()) {
			this.toJsonMsg(response, 1, null);
			return;
		}
		file.delete(); // 存在删除文件
		OutputStreamWriter outputStreamWriter = null;
		try {
			outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file), StringCharsetContants.CHARSET_UTF8);
			outputStreamWriter.write(textarea);
			outputStreamWriter.flush();
			this.toJsonMsg(response, 0, null);
			return;
		} catch (Exception e) {
			log.error("", e);
		} finally {
			IOUtils.close(outputStreamWriter);
		}
		this.toJsonMsg(response, 1, null);
	}

	// private synchronized String getJsonData(File file) throws
	// UnsupportedEncodingException {
	// if (!file.exists()) {
	// return null;
	// }
	// boolean isFile = file.isFile();
	// boolean isDir = file.isDirectory();
	// String type = "file";
	// if (isDir)
	// type = "folder";
	//
	// String fileName = file.getName();
	// String filePath = file.getPath();
	// String md5Str = util.getMD5(filePath, null, 1);
	// String base64Encoder = URLEncoder.encode(new
	// BASE64Encoder().encode(filePath.getBytes()), StringCharsetContants.CHARSET_UTF8);
	// String url = "admin/openFile.htm"; // 路径
	// // log.debug(base64Encoder);
	// sb.append("{"); //
	// ,\"attributes\":{\"url\":\"/admin/tree/openFile.htm\",\"target\":\"mainFrame\"
	// sb.append("\"id\":\"" + md5Str + "\",\"text\":\"" + fileName + "\"");
	// sb.append(",\"attributes\":{\"text\":\"" + fileName + "\",\"url\":\"" +
	// url + "\",\"type\":\"" + type
	// + "\",\"path\":\"" + base64Encoder + "\"}");
	// if (isDir) {
	// File fileList[] = file.listFiles(new FileFilter() {
	// public boolean accept(File pathname) {
	// String fileNameLower = pathname.getName().toLowerCase();
	// if (pathname.isHidden())
	// return false;
	// /** ********* 隐藏文件过滤 ********** */
	// if
	// (fileNameLower.matches(".*(meta-inf|templates)$|.*.(gif|jpg|png|ico|class|.jar)$"))
	// {
	// return false;
	// }
	// return true;
	// }
	// });
	// //
	// sb.append(",\"attributes\":{\"id\":\""+md5Str+"\",\"path\":\""+base64Encoder+"\"}");
	// if (fileList.length > 0) {
	// sb.append(",\"state\":\"closed\",\"children\":[");
	// for (int i = 0; i < fileList.length; i++) {
	// if (i > 0)
	// sb.append(",");
	// this.getJsonData(fileList[i]);
	// }
	// sb.append("]");
	// }
	// }
	// // target="mainFrame"
	// if (isFile) {
	// // sb.append(",\"state\":\"closed\""); //\"target\":\"mainFrame\",
	// //
	// sb.append(",\"attributes\":{\"id\":\""+md5Str+"\",\"path\":\""+base64Encoder+"\"}");
	// }
	// sb.append("}");
	// return sb.toString();
	// }

	private synchronized String getAsyJsonData(File file) throws UnsupportedEncodingException {
		if (!file.exists()) {
			return null;
		}
		File fileList[] = file.listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				String fileNameLower = pathname.getName().toLowerCase();
				if (pathname.isHidden())
					return false;
				/** ********* 隐藏文件过滤 ********** */
				if (fileNameLower.matches(
						".*(meta-inf|templates)$|.*.(gif|jpg|png|ico|class|.jar|.zip|.gz|.sql|.exe|.bt|.sh)$")) {
					return false;
				}
				return true;
			}
		});
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < fileList.length; i++) {
			file = fileList[i];
			boolean isDir = file.isDirectory();
			String type = "file";
			String state = "open";
			if (isDir) {
				type = "folder";
				state = "closed";
			}
			String fileName = file.getName();
			String filePath = file.getPath();
			String base64Encoder = URLEncoder.encode(new BASE64Encoder().encode(filePath.getBytes(StringCharsetContants.CHARSET_UTF8)), StringCharsetContants.CHARSET_UTF8);
			String url = "admin/openFile.htm"; // 路径
			// log.debug(base64Encoder);
			sb.append("{"); //
			sb.append("\"id\":\"" + base64Encoder + "\",\"text\":\"" + fileName + "\",\"state\":\"" + state + "\"");
			sb.append(",\"attributes\":{\"text\":\"" + fileName + "\",\"url\":\"" + url + "\",\"type\":\"" + type
					+ "\",\"path\":\"" + base64Encoder + "\"}");
			sb.append("},");
		}
		if (fileList.length > 0) {
			sb.delete(sb.length() - 1, sb.length());
		} else {
			sb.append("");
		}
		return sb.toString();
	}

}
