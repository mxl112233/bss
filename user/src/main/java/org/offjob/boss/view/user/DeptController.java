package org.offjob.boss.view.user;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.offjob.boss.domain.user.model.DeptModel;
import org.offjob.boss.domain.user.service.DeptServiceImpl;
import org.offjob.boss.utils.NumberUtils;
import org.offjob.boss.utils.PageParams;
import org.offjob.boss.view.BaseController;

@Controller
@RequestMapping("/admin/Dept")
public class DeptController extends BaseController {
	private final static Logger log = Logger.getLogger(DeptController.class);

	// 服务类
	@Resource(name = "deptService")
	private DeptServiceImpl<DeptModel> deptService;

	@RequestMapping("index.htm")
	public ModelAndView index(String id, ModelMap modelMap, HttpServletRequest request) {
		return new ModelAndView("admin/Dept/index", modelMap);
	}

	@RequestMapping("baseDlg.htm")
	public String baseDlg() {
		return "admin/Dept/baseDlg";
	}

	@RequestMapping("importDlg.htm")
	public String importDlg() {
		return "admin/Dept/importDlg";
	}

	@RequestMapping("data.htm")
	@ResponseBody
	public String data(PageParams pageParams, DeptModel deptModel) throws Exception {

		deptModel.getPageUtil().setPageId(NumberUtils.parseInt(pageParams.getPage(), 1)); // 当前页
		deptModel.getPageUtil().setPageSize(NumberUtils.parseInt(pageParams.getRows(), 10));// 显示X条

		String str = "";
		String suffix = "}";
		if (pageParams.getGridName() != null) {
			str = "[";
			suffix = "]}";
		}
		List<DeptModel> listDeptModel = null;
		StringBuilder center = new StringBuilder();

		if (pageParams.getSearchType() != null) {
			deptModel.getPageUtil().setLike("1".equalsIgnoreCase(pageParams.getSearchType()));
			listDeptModel = deptService.selectByModel(deptModel);
			center.append("{\"total\":\"" + deptModel.getPageUtil().getRowCount() + "\",\"rows\":" + str);
		} else {
			if (pageParams.getGridName() == null) {
				listDeptModel = deptService.selectByModel(deptModel);
				center.append("{\"total\":\"" + deptModel.getPageUtil().getRowCount() + "\",\"rows\":");
				suffix = "}";
			} else {
				if (deptModel.getId() == null) { // 首次打开
					deptModel.getPageUtil().setAndCondition("parent_id IS NULL");
					listDeptModel = deptService.selectByModel(deptModel);
					center.append("{\"total\":\"" + deptModel.getPageUtil().getRowCount() + "\",\"rows\":" + str);
				} else { // 子节点加载
					deptModel.setParentId(deptModel.getId());
					deptModel.setId(null);
					listDeptModel = deptService.selectByModel(deptModel);
					center.append("[");
					suffix = "]";
				}
			}
		}

		if (pageParams.getGridName() == null) { // datagrid
			center.append(JSON.toJSONString(listDeptModel));
		} else {
			for (int i = 0; i < listDeptModel.size(); i++) {
				DeptModel model = listDeptModel.get(i);
				String json = JSON.toJSONString(model, SerializerFeature.WriteDateUseDateFormat);
				DeptModel model2 = new DeptModel();
				model2.setParentId(model.getId());
				if (deptService.selectByModelCount(model2) > 0) {
					json = json.substring(0, json.length() - 1);
					json += ",\"state\":\"closed\"}";
				}
				center.append(json + ",");
			}
			center.delete(center.length() - 1, center.length());
		}
		center.append(suffix);
		return center.toString();
	}

	@RequestMapping("add.htm")
	public void add(DeptModel deptModel, HttpServletResponse response) {
		try {
			if (null != deptModel.getParentId() && deptModel.getParentId() <= 0) {
				deptModel.setParentId(null);
			}
			if (deptService.insert(deptModel) > 0) {
				this.toJsonMsg(response, 0, null);
				return;
			}
		} catch (Exception e) {
			log.error("", e);
		}
		this.toJsonMsg(response, 1, null);
	}

	@RequestMapping("save.htm")
	public void save(DeptModel deptModel, HttpServletResponse response) {
		try {
			if (null != deptModel.getParentId() && deptModel.getParentId() <= 0) {
				deptModel.setParentId(null);
			}
			if (deptService.updateByPrimaryKey(deptModel) > 0) {
				this.toJsonMsg(response, 0, null);
				return;
			}
		} catch (Exception e) {
			this.toJsonMsg(response, 1, null);
			log.error("", e);
		}
	}

	@RequestMapping("del.htm")
	public void del(Integer[] ids, HttpServletResponse response) {
		log.debug("del-ids:" + Arrays.toString(ids));
		try {
			if (null != ids && ids.length > 0&&deptService.deleteByPrimaryKeys(ids) > 0) {
				this.toJsonMsg(response, 0, null);
				return;
			}
		} catch (Exception e) {
			this.toJsonMsg(response, 1, null);
			log.error("", e);
		}
	}

	@RequestMapping("comboxTree.htm")
	@ResponseBody
	public String comboxTree(String id, Integer type) throws Exception {
		List<Map<String, Object>> lm;
		Map<String, Object> map = new HashMap<String, Object>();
		if (null == id) {
			map.put("andCondition", "parent_id IS NULL");
			lm = deptService.selectByDeptOther(map);
		} else {
			map.put("parent_id", id);
			lm = deptService.selectByDeptOther(map);
		}
		map.clear();
		if (lm != null && lm.size() > 0) {
			StringBuffer sb = new StringBuffer();
			sb.append("[");
			for (int i = 0; i < lm.size(); i++) {
				sb.append("{");
				map = lm.get(i);
				Long child = (Long) map.get("child");
				Integer pkid = (Integer) map.get("id");
				if (child > 0) {
					sb.append("\"state\":\"open\",");
					if (type != null && type != 0) { // 同步递归
						sb.append("\"children\":").append(this.comboxTree(String.valueOf(pkid), type) + ",");
					}
				}
				sb.append("\"id\":" + "\"" + pkid + "\",");
				sb.append("\"text\":" + "\"" + map.get("name") + "\"");
				sb.append("},");
			}
			sb = sb.delete(sb.length() - 1, sb.length());
			sb.append("]");
			return sb.toString();
		}
		return "[]";
	}

}
