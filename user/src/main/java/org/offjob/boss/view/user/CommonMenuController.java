package org.offjob.boss.view.user;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import org.offjob.boss.domain.user.model.DeptModel;
import org.offjob.boss.domain.user.model.UserModel;
import org.offjob.boss.domain.user.service.DeptServiceImpl;
import org.offjob.boss.domain.user.service.UserServiceImpl;
import org.offjob.boss.utils.MD5Coder;
import org.offjob.boss.utils.NumberUtils;
import org.offjob.boss.utils.PageParams;
import org.offjob.boss.view.BaseController;
import org.offjob.boss.view.interceptor.SessionUtil;
import org.offjob.boss.view.interceptor.UserToken;

@Controller
@RequestMapping("/User")
public class CommonMenuController extends BaseController {
	private final static Logger log = Logger.getLogger(CommonMenuController.class);
	// 服务类
	@Resource(name = "userService")
	private UserServiceImpl<UserModel> userService;

	// 服务类
	@Resource(name = "deptService")
	private DeptServiceImpl<DeptModel> deptService;

	@RequestMapping("upInfoDlg.htm")
	public void upInfoDlg(HttpServletRequest request, HttpServletResponse response) {
		UserToken token = SessionUtil.getUser(request);
		UserModel tbsUserModel = new UserModel();
		tbsUserModel.setId(token.getId());
		tbsUserModel.setRealname(request.getParameter("realname"));
		tbsUserModel.setWorkPlace(request.getParameter("hometown"));
		tbsUserModel.setMail(request.getParameter("mail"));
		tbsUserModel.setPhone(request.getParameter("phone"));
		tbsUserModel.setTelephone(request.getParameter("telephone"));
		tbsUserModel.setSex(request.getParameter("sex"));
		save(tbsUserModel, response);
	}

	@RequestMapping("uppwd.htm")
	public void uppwd(HttpServletRequest request, HttpServletResponse response) throws Exception {
		UserToken token = SessionUtil.getUser(request);
		UserModel tbsUserModel = new UserModel();
		tbsUserModel.setId(token.getId());
		tbsUserModel = userService.selectByPrimaryKey(token.getId());
		String oldPwd = request.getParameter("passwordOld");
		if (!MD5Coder.encodeMD5Hex(oldPwd).toLowerCase().equals(tbsUserModel.getPassword())) {
			this.toJsonMsg(response, 1, "原密码不正确,不能修改!");
			return;
		}
		tbsUserModel.setId(Integer.valueOf(request.getParameter("id")));
		tbsUserModel.setPassword(MD5Coder.encodeMD5Hex(request.getParameter("password1")).toLowerCase());
		save(tbsUserModel, response);
	}

	private void save(UserModel UserModel,HttpServletResponse response) {
		try {
			if (userService.updateByPrimaryKey(UserModel) > 0) {
				this.toJsonMsg(response, 0, "更新成功");
			}
		} catch (Exception e) {
			this.toJsonMsg(response, 1, "操作失败,请重试或者联系管理员!");
		}
	}

	@RequestMapping("updatepwd.htm")
	public ModelAndView updatepwd(HttpServletRequest request, HttpServletResponse response) {
		UserToken tbsUserModel = SessionUtil.getUser(request);
		return new ModelAndView("common/updatepwdDlg").addObject("UserModel", tbsUserModel);
	}

	@RequestMapping("myIndex.htm")
	public ModelAndView info(HttpServletRequest request, HttpServletResponse response) throws Exception {
		UserToken token = SessionUtil.getUser(request);
		UserModel tbsUserModel = new UserModel();
		tbsUserModel.setId(token.getId());
		tbsUserModel = userService.selectByPrimaryKey(token.getId());
		return new ModelAndView("common/myInfoDlg").addObject("UserModel", tbsUserModel);
	}

	@RequestMapping("companyAddress.htm")
	public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView("common/companyAddress");
	}

	@RequestMapping("data.htm")
	@ResponseBody
	public String data(PageParams pageParams, UserModel UserModel) throws Exception {
		UserModel.getPageUtil().setPageId(NumberUtils.parseInt(pageParams.getPage(), 1)); // 当前页
		UserModel.getPageUtil().setPageSize(NumberUtils.parseInt(pageParams.getRows(), 10));// 显示X条
		// 正序与倒序
		String str = "";
		String suffix = "}";
		if (pageParams.getGridName() != null) {
			str = "[";
			suffix = "]}";
		}
		List<UserModel> listUserModel = null;
		StringBuilder center = new StringBuilder();

		if (pageParams.getSearchType() != null) {
			if (pageParams.getSearchType().equals("1")) { // 模糊搜索
				UserModel.getPageUtil().setLike(true);
				listUserModel = userService.selectByModel(UserModel);
				center.append("{\"total\":\"" + UserModel.getPageUtil().getRowCount() + "\",\"rows\":" + str);
			}
		} else {
			if (pageParams.getGridName() == null) {
				listUserModel = userService.selectByModel(UserModel);
				center.append("{\"total\":\"" + UserModel.getPageUtil().getRowCount() + "\",\"rows\":");
				suffix = "}";
			}
		}

		if (pageParams.getGridName() == null) { // datagrid
			center.append(JSON.toJSONString(listUserModel));
		}
		center.append(suffix);
		return center.toString();
	}

	@RequestMapping("comboxTree.htm")
	@ResponseBody
	public String comboxTree(String id) throws Exception {
		List<Map<String, Object>> lm;
		Map<String, Object> map = new HashMap<String, Object>();
		// TbsRoleDeptModel tbsRoleDeptModel = new TbsRoleDeptModel();
		if (null == id) {
			map.put("andCondition", "parent_id IS NULL");
			lm = deptService.selectByDeptOther(map);
		} else {
			map.put("parentId", id);
			lm = deptService.selectByDeptOther(map);
		}
		map.clear();
		if (lm != null && lm.size() > 0) {
			StringBuffer sb = new StringBuffer();
			sb.append("[");
			for (int i = 0; i < lm.size(); i++) {
				sb.append("{");
				map = lm.get(i);
				Long child = (Long) map.get("child");
				Integer pkid = (Integer) map.get("id");
				if (child > 0) {
					sb.append("\"state\":\"closed\",");
				}
				sb.append("\"id\":" + "\"" + pkid + "\",");
				sb.append("\"text\":" + "\"" + map.get("name") + "\"");
				sb.append("},");
			}
			sb = sb.delete(sb.length() - 1, sb.length());
			sb.append("]");
			log.debug("json:" + sb.toString());
			return sb.toString();
		}
		return "[]";
	}
}
