package org.offjob.boss.view.interceptor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.offjob.boss.domain.user.model.MenuModel;

public class UserToken implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4167414793277969861L;

	private Integer id;
	private String username;
	private String realname;
	private Integer isAdmin;
	private boolean hasLogin;
	private Map<String, String> params = new HashMap<String, String>();

	private List<MenuModel> MenuModelList;

	private Set<String> authorizedUrl = new HashSet<String>();

	public List<MenuModel> getMenuModelList() {
		return MenuModelList;
	}

	public void setMenuModelList(List<MenuModel> menuModelList) {
		MenuModelList = menuModelList;
	}

	public boolean isHasLogin() {
		return hasLogin;
	}

	public void setHasLogin(boolean hasLogin) {
		this.hasLogin = hasLogin;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public Map<String, String> getParams() {
		return params;
	}

	public void put(String key, String value) {
		params.put(key, value);
	}

	public Integer getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Integer isAdmin) {
		this.isAdmin = isAdmin;
	}

	public Set<String> getAuthorizedUrl() {
		return authorizedUrl;
	}

}
