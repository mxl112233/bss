package org.offjob.boss.view.interceptor;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import org.offjob.boss.domain.user.model.UserModel;

public final class SessionUtil {
	protected static final Logger log = Logger.getLogger(SessionUtil.class);
	private static final String USER = "userToken"; // 用户
	private static final String VERIFY_CODE = "verifyCode";// 验证码

	private static void setAttr(HttpServletRequest request, String key, Object value) {
		request.getSession(true).setAttribute(key, value);
	}
	//
	// public static Object getAttr(HttpServletRequest request, String key) {
	// return request.getSession(true).getAttribute(key);
	// }
	//

	private static void removeAttr(HttpServletRequest request, String key) {
		request.getSession(true).removeAttribute(key);
	}

	public static UserToken getUser(HttpServletRequest request) {
		return (UserToken) request.getSession(true).getAttribute(USER);
	}

	public static void setVerifyCode(HttpServletRequest request, String sessionVerifyCode) {
		request.getSession(true).setAttribute(VERIFY_CODE, sessionVerifyCode);
	}

	public static String getVerifyCode(HttpServletRequest request) {
		return (String) request.getSession(true).getAttribute(VERIFY_CODE);
	}

	public static void removeVerifyCode(HttpServletRequest request) {
		removeAttr(request, VERIFY_CODE);
	}

	public static void removeSessionAll(HttpServletRequest request) {
		removeAttr(request, USER);
		if (request.getSession(true) != null) {
			request.getSession(true).invalidate();
		}
	}

	public static void createSession(HttpServletRequest request, UserModel userModel) {
		UserToken token = new UserToken();
		token.setId(userModel.getId());
		token.setRealname(userModel.getRealname());
		token.setUsername(userModel.getUsername());
		token.setIsAdmin(userModel.getIsAdmin());
		token.getAuthorizedUrl().add("/admin/index.htm");
		setAttr(request, USER, token);
	}

	public static void updateSession(HttpServletRequest request, UserToken token) {
		setAttr(request, USER, token);
	}
}
