package org.offjob.boss.view.user;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.offjob.boss.domain.user.model.RoleModel;
import org.offjob.boss.domain.user.model.RoleUserModel;
import org.offjob.boss.domain.user.model.UserModel;
import org.offjob.boss.domain.user.service.RoleServiceImpl;
import org.offjob.boss.domain.user.service.RoleUserServiceImpl;
import org.offjob.boss.domain.user.service.UserServiceImpl;
import org.offjob.boss.utils.MD5Coder;
import org.offjob.boss.utils.NumberUtils;
import org.offjob.boss.utils.PageParams;
import org.offjob.boss.view.BaseController;

@Controller
@RequestMapping("/admin/User")
public class UserController extends BaseController {
	private final static Logger log = Logger.getLogger(UserController.class);
	// 服务类
	@Resource(name = "userService")
	private UserServiceImpl<UserModel> userService;

	@RequestMapping("index.htm")
	public ModelAndView index(String id, ModelMap modelMap, HttpServletRequest request) {
		return new ModelAndView("admin/User/index", modelMap);
	}

	@RequestMapping("baseDlg.htm")
	public String baseDlg() {
		return "admin/User/baseDlg";
	}

	@RequestMapping("importDlg.htm")
	public String importDlg() {
		return "admin/User/importDlg";
	}

	@RequestMapping("data.htm")
	@ResponseBody
	public String data(PageParams pageParams, UserModel UserModel) throws Exception {
		UserModel.getPageUtil().setPageId(NumberUtils.parseInt(pageParams.getPage(), 1)); // 当前页
		UserModel.getPageUtil().setPageSize(NumberUtils.parseInt(pageParams.getRows(), 10));// 显示X条
		// if (pageParams.getSort() != null) {
		// try {
		// UserModel.getPageUtil().setOrderByCondition(pageParams.getSort()); //
		// 排序字段名称
		// } catch (Exception e) {
		// log.error("", e);
		// }
		// }

		// 正序与倒序
		String str = "";
		String suffix = "}";
		if (StringUtils.isNotEmpty(pageParams.getGridName())) {
			str = "[";
			suffix = "]}";
		}
		List<UserModel> listUserModel = null;
		StringBuilder center = new StringBuilder();
		if (StringUtils.isNotEmpty(pageParams.getSearchType())) {
			if (pageParams.getSearchType().equals("1")) { // 模糊搜索
				UserModel.getPageUtil().setLike(true);
				listUserModel = userService.selectByModelPaging(UserModel);
				center.append("{\"total\":\"" + UserModel.getPageUtil().getRowCount() + "\",\"rows\":" + str);
			}
		} else {
			if (pageParams.getGridName() == null) {
				listUserModel = userService.selectByModelPaging(UserModel);
				center.append("{\"total\":\"" + UserModel.getPageUtil().getRowCount() + "\",\"rows\":");
				suffix = "}";
			}
		}

		if (pageParams.getGridName() == null) { // datagrid
			center.append(JSON.toJSONString(listUserModel,SerializerFeature.WriteDateUseDateFormat));
		}
		center.append(suffix);
		log.debug(center);
		return center.toString();
	}

	@RequestMapping("myData.htm")
	@ResponseBody
	public String myData(PageParams pageParams, UserModel UserModel) throws Exception {
		return data(pageParams, UserModel);
	}

//	@RequestMapping("export.htm")
//	public void UserExport(HttpServletResponse response, UserModel UserModel) {
//		List<UserModel> listUserModel = null;
//		OutputStream out = null;
//		try {
//			listUserModel = userService.selectByModel(UserModel);
//		} catch (Exception e1) {
//			log.error("", e1);
//		}
//		ExcelUtil excelUtil = new ExcelUtil();
//		Map<String, List<UserModel>> exportMap = new HashMap<String, List<UserModel>>();
//		exportMap.put("sheet", listUserModel);
//		Workbook wb = excelUtil.writeExcel2(exportMap, null, 1);
//		String filename = MethodUtil.getInstance().getDate(1, null) + ".xls";
//		response.setContentType("application/vnd.ms-excel");
//		response.addHeader("Content-Disposition", "attachment;filename=" + filename);
//		// response.addHeader("Content-Length", "");
//		try {
//			out = response.getOutputStream();
//			wb.write(out);
//			out.flush();
//		} catch (IOException e) {
//			log.error("export error.....", e);
//		} finally {
//			IOUtils.closeQuietly(out);
//		}
//	}

	// @RequestMapping("import.htm")
	// public void UserImport(HttpServletRequest request, HttpServletResponse
	// response) {
	// MultipartHttpServletRequest multipartRequest =
	// (MultipartHttpServletRequest) request;
	// Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
	// for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
	// MultipartFile mf = entity.getValue();
	// ExcelUtil excelUtil = new ExcelUtil();
	// boolean flg = true;
	// if (entity.getKey().endsWith("xlsx")) {
	// flg = false;
	// }
	// try {
	// Map<String, List<UserModel>> ml = excelUtil.readExcelToModel(new
	// ByteArrayInputStream(mf.getBytes()),
	// UserModel.class, flg);
	// RoleModel roleModel = null;
	// for (Map.Entry<String, List<UserModel>> map : ml.entrySet()) {
	// List<UserModel> lt = map.getValue();
	// for (int i = 0; i < lt.size(); i++) {
	// UserModel umodel = lt.get(i);
	// umodel.setId(MethodUtil.getInstance().getUidString());
	// umodel.setState(1);
	// umodel.setIsAdmin(1);
	// try {
	// RoleUserModel tbsRoleUserBean = new RoleUserModel();
	// // 查找权限id
	// roleModel = new RoleModel();
	// roleModel.setName(umodel.getPostid());
	// List<RoleModel> elist = tbsRoleService.selectByModel(roleModel);
	// if (elist != null && !elist.isEmpty()) {
	// tbsRoleUserBean.setId(MethodUtil.getInstance().getUidString());
	// tbsRoleUserBean.setRoleId(elist.get(0).getId());
	// tbsRoleUserBean.setUserId(umodel.getId());
	// tbsRoleUserService.insert(tbsRoleUserBean);
	// }
	// userService.insert(umodel);
	// } catch (Exception e) {
	// log.error("", e);
	// }
	// }
	// }
	// } catch (IOException e) {
	// log.error("", e);
	// this.toJsonMsg(response, 1, null);
	// return;
	// }
	// }
	// this.toJsonMsg(response, 0, null);
	// }

	// ///////////////////////////////////////////////////////////////////////////////////////////
	// ///////////////////////////////////在下面添加自定义的方法///////////////////////////////////
	@Resource(name = "roleUserService")
	RoleUserServiceImpl<RoleUserModel> tbsRoleUserService;

	// 服务类
	@Resource(name = "roleService")
	// 自动注入，不需要生成set方法了，required=false表示没有实现类，也不会报错。
	private RoleServiceImpl<RoleModel> tbsRoleService;

	@RequestMapping("/save.htm")
	public void save(UserModel tbsUserBean, HttpServletResponse response) {
		try {
			if (tbsUserBean.getId() != null) {
				RoleUserModel tbsRoleUserBean = new RoleUserModel();
				tbsRoleUserBean.setUserId(tbsUserBean.getId());
				// tbsRoleUserService.deleteByEntity(tbsRoleUserBean);
				// 查找权限id
				RoleModel entity = new RoleModel();
				entity.setName(tbsUserBean.getPostid());
				List<RoleModel> elist = tbsRoleService.selectByModel(entity);
				if (elist != null) {
					tbsRoleUserBean.setRoleId(elist.get(0).getId());
					tbsRoleUserBean.setUserId(tbsUserBean.getId());
					tbsRoleUserService.insert(tbsRoleUserBean);
				}

				if ("123456".equalsIgnoreCase(tbsUserBean.getPassword())) {
					tbsUserBean.setPassword(MD5Coder.encodeMD5Hex(tbsUserBean.getPassword()).toLowerCase());
				}
				userService.updateByPrimaryKey(tbsUserBean);
				this.toJsonMsg(response, 0, null);
			}
		} catch (Exception e) {
			this.toJsonMsg(response, 1, null);
			log.error("", e);
		}
	}

	@RequestMapping("/del.htm")
	public void del(Integer[] ids, HttpServletResponse response) {
		log.info("del-ids:" + Arrays.toString(ids));
		try {
			userService.deleteByPrimaryKeys(ids);
			RoleUserModel tbsRoleUserBean = new RoleUserModel();
			for (int i = 0; i < ids.length; i++) {
				tbsRoleUserBean.setUserId(Integer.valueOf(ids[i]));
				tbsRoleUserService.deleteByEntity(tbsRoleUserBean);
			}
			this.toJsonMsg(response, 0, null);
		} catch (Exception e) {
			this.toJsonMsg(response, 1, null);
			log.error("", e);
		}
	}

	@RequestMapping("/add.htm")
	public void add(UserModel tbsUserBean, HttpServletResponse response) {
		try {
			tbsUserBean.setPassword(MD5Coder.encodeMD5Hex(tbsUserBean.getPassword()).toLowerCase());
			tbsUserBean.setIsAdmin(1);
			tbsUserBean.setDeptid("总公司");
			userService.insert(tbsUserBean);// 入库

			RoleUserModel tbsRoleUserBean = new RoleUserModel();
			// 查找权限id
			RoleModel entity = new RoleModel();
			entity.setName(tbsUserBean.getPostid());
			List<RoleModel> elist = tbsRoleService.selectByModel(entity);
			if (elist != null && !elist.isEmpty()) {
				tbsRoleUserBean.setId(null);
				tbsRoleUserBean.setRoleId(elist.get(0).getId());
				tbsRoleUserBean.setUserId(tbsUserBean.getId());
				tbsRoleUserService.insert(tbsRoleUserBean);
			}
			// tbsUserBean.setPassword(util.getDES("desKey!@#",
			this.toJsonMsg(response, 0, null);
		} catch (Exception e) {
			this.toJsonMsg(response, 1, null);
			log.error("", e);
		}
	}

	@RequestMapping("usemenu.htm")
	public ModelAndView usemenu(String id, ModelMap modelMap, HttpServletRequest request) {
		// 角色名称
		return new ModelAndView("admin/User/fromIndex", modelMap);
	}

}
