package org.offjob.boss.view.user;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.offjob.boss.domain.user.model.MenuModel;
import org.offjob.boss.domain.user.model.RoleMenuModel;
import org.offjob.boss.domain.user.service.MenuServiceImpl;
import org.offjob.boss.domain.user.service.RoleMenuServiceImpl;
import org.offjob.boss.utils.NumberUtils;
import org.offjob.boss.utils.PageParams;
import org.offjob.boss.view.BaseController;
import org.offjob.boss.view.interceptor.SessionUtil;
import org.offjob.boss.view.interceptor.UserToken;

@Controller
@RequestMapping("/admin/Menu")
public class MenuController extends BaseController {
	private final static Logger log = Logger.getLogger(MenuController.class);

	// 服务类
	@Resource(name = "menuService")
	private MenuServiceImpl<MenuModel> tbsMenuService;

	@RequestMapping("index.htm")
	public ModelAndView index(String id, ModelMap modelMap, HttpServletRequest request) {
		List<String> buttons = new java.util.ArrayList<String>();
		MenuModel tbsMenuModel = new MenuModel();
		tbsMenuModel.setParentId(Integer.valueOf(id));
		tbsMenuModel.getPageUtil().setOrderByCondition("sort_number");
		UserToken tbsUserModel = SessionUtil.getUser(request);
		List<MenuModel> list = null;
		try {
			if (null != tbsUserModel && 0 == tbsUserModel.getIsAdmin()) {// 超管不需要验证权限
				list = tbsMenuService.selectByModel(tbsMenuModel);
			} else {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("parent_id", id);
				map.put("orderCondition", "sort_number");
				list = tbsMenuService.selectByButtons(map);
			}
			if (list != null && list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					tbsMenuModel = list.get(i);
					String button = tbsMenuModel.getButton();
					if (null != button && "null" != button) {
						buttons.add(button);// .replaceAll("&apos;",
						// "'").replaceAll("&quot;", "\"")
					}
				}
			}
		} catch (Exception e) {
			log.error("", e);
		}
		modelMap.addAttribute("buttons", buttons);
		return new ModelAndView("admin/Menu/index", modelMap);
	}

	@RequestMapping("baseDlg.htm")
	public String baseDlg() {
		return "admin/Menu/baseDlg";
	}

	@RequestMapping("data.htm")
	@ResponseBody
	public String data(PageParams pageParams, MenuModel tbsMenuModel) throws Exception {
		tbsMenuModel.getPageUtil().setPageId(NumberUtils.parseInt(pageParams.getPage(), 1)); // 当前页
		tbsMenuModel.getPageUtil().setPageSize(NumberUtils.parseInt(pageParams.getRows(), 10));// 显示X条
		String str = "";
		String suffix = "}";
		if (pageParams.getGridName() != null) {
			str = "[";
			suffix = "]}";
		}
		List<MenuModel> listTbsMenuModel = null;
		StringBuilder center = new StringBuilder();

		if (pageParams.getSearchType() != null) {
			if (pageParams.getSearchType().equals("1")) { // 模糊搜索
				tbsMenuModel.getPageUtil().setLike(true);
			}
			listTbsMenuModel = tbsMenuService.selectByModel(tbsMenuModel);
			center.append("{\"total\":\"" + tbsMenuModel.getPageUtil().getRowCount() + "\",\"rows\":" + str);
		} else {
			if (pageParams.getGridName() == null) {
				listTbsMenuModel = tbsMenuService.selectByModel(tbsMenuModel);
				center.append("{\"total\":\"" + tbsMenuModel.getPageUtil().getRowCount() + "\",\"rows\":");
				suffix = "}";
			} else {
				if (tbsMenuModel.getId() == null) { // 首次打开
					tbsMenuModel.getPageUtil().setAndCondition("parent_id IS NULL");
					tbsMenuModel.getPageUtil().setOrderByCondition("sort_number");
					listTbsMenuModel = tbsMenuService.selectByModel(tbsMenuModel);
					center.append("{\"total\":\"" + tbsMenuModel.getPageUtil().getRowCount() + "\",\"rows\":" + str);
				} else { // 子节点加载
					tbsMenuModel.setParentId(tbsMenuModel.getId());
					tbsMenuModel.setId(null);
					listTbsMenuModel = tbsMenuService.selectByModel(tbsMenuModel);
					center.append("[");
					suffix = "]";
				}
			}
		}

		if (pageParams.getGridName() == null) { // datagrid
			center.append(JSON.toJSONString(listTbsMenuModel));
		} else {
			if (listTbsMenuModel.size() > 0) { // TreeGrid
				for (int i = 0; i < listTbsMenuModel.size(); i++) {
					MenuModel model = listTbsMenuModel.get(i);
					String json = JSON.toJSONString(model, SerializerFeature.WriteDateUseDateFormat);
					MenuModel model2 = new MenuModel();
					model2.setParentId(model.getId());
					if (tbsMenuService.selectByModelCount(model2) > 0) {
						json = json.substring(0, json.length() - 1);
						json += ",\"state\":\"closed\"}";
					}
					center.append(json + ",");
				}
				center.delete(center.length() - 1, center.length());
			}
		}
		center.append(suffix);
		return center.toString();
	}

	@RequestMapping("add.htm")
	public void add(MenuModel tbsMenuModel, HttpServletResponse response) {
		log.info("tbsMenuModel:" + tbsMenuModel.toString());
		try {
			if (null != tbsMenuModel.getParentId()) {
				tbsMenuModel.setParentId(null);
			}
			if (tbsMenuService.insert(tbsMenuModel) > 0) {
				this.toJsonMsg(response, 0, null);
				return;
			}
		} catch (Exception e) {
			log.error("", e);
		}
		this.toJsonMsg(response, 1, null);
	}

	@RequestMapping("save.htm")
	public void save(MenuModel tbsMenuModel, HttpServletResponse response) {
		try {
			if (null != tbsMenuModel.getParentId()) {
				tbsMenuModel.setParentId(null);
			}
			if (tbsMenuService.updateByPrimaryKey(tbsMenuModel) > 0) {
				this.toJsonMsg(response, 0, null);
				return;
			}
		} catch (Exception e) {
			this.toJsonMsg(response, 1, null);
			log.error("", e);
		}
	}

	@RequestMapping("del.htm")
	public void del(Integer[] ids, HttpServletResponse response) {
		log.debug("del-ids:" + Arrays.toString(ids));
		try {
			if (null != ids && ids.length > 0&&tbsMenuService.deleteByPrimaryKeys(ids) > 0) {
				this.toJsonMsg(response, 0, null);
			}
		} catch (Exception e) {
			this.toJsonMsg(response, 1, null);
			log.error("", e);
		}
	}

	@RequestMapping("gridTree.htm")
	@ResponseBody
	public String gridTree(HttpServletResponse response, String id, String name, String value) throws Exception {
		List<Map<String, Object>> lm;
		Map<String, Object> map = new HashMap<String, Object>();
		if (null == id && null == name) {
			map.put("andCondition", "parent_id IS NULL");
			map.put("orderCondition", "sortNumber");
			lm = tbsMenuService.selectByMenuOther(map);
		} else {
			if (null != id) {
				map.put("parent_id", id);
				lm = tbsMenuService.selectByMenuOther(map);
			} else {
				map.put(name, value);
				lm = tbsMenuService.selectByMenuOther(map);
			}
		}
		map.clear();
		if (lm != null && lm.size() > 0) {
			StringBuffer sb = new StringBuffer();
			sb.append("[");
			for (int i = 0; i < lm.size(); i++) {
				sb.append("{");
				map = lm.get(i);
				Long child = (Long) map.get("child");
				String menuId = (String) map.get("id");
				if (child > 0) {
					sb.append("\"state\":\"closed\",");
				}
				sb.append("\"id\":" + "\"" + menuId + "\",");
				sb.append("\"url\":" + "\"" + map.get("url") + "\",");
				sb.append("\"createTime\":" + "\"" + map.get("createTime") + "\",");
				sb.append("\"parentId\":" + "\"" + map.get("parentId") + "\",");
				sb.append("\"type\":" + "\"" + map.get("type") + "\",");
				sb.append("\"sortNumber\":" + "\"" + map.get("sortNumber") + "\",");
				sb.append("\"parent\":" + "\"" + map.get("parent") + "\",");
				sb.append("\"child\":" + "\"" + map.get("child") + "\",");
				sb.append("\"isMenu\":" + "\"" + map.get("isMenu") + "\",");
				sb.append("\"status\":" + "\"" + map.get("status") + "\",");
				// sb.append("\"button\":" + "\"" + map.get("button") + "\",");
				sb.append("\"name\":" + "\"" + map.get("name") + "\"");
				sb.append("},");
			}
			sb = sb.delete(sb.length() - 1, sb.length());
			sb.append("]");
			return sb.toString();
		}
		return "[]";
	}

	@Resource(name = "roleMenuService")
	RoleMenuServiceImpl<RoleMenuModel> tbsRoleMenuService;

	@RequestMapping("comboxTree.htm")
	@ResponseBody
	public String comboxTree(Integer id, String roleId, Integer type, Map<Integer, Integer> roleMenuList)
			throws Exception {
		List<Map<String, Object>> lm;
		Map<String, Object> map = new HashMap<String, Object>();
		if (null == id) {
			map.put("andCondition", "parent_id IS NULL and status=0");
			lm = tbsMenuService.selectByMenuOther(map);
		} else {
			map.put("parentId", id);
			map.put("status", 0);
			lm = tbsMenuService.selectByMenuOther(map);
		}
		map.clear();
		if (lm != null && lm.size() > 0) {
			StringBuffer sb = new StringBuffer();
			sb.append("[");
			for (int i = 0; i < lm.size(); i++) {
				sb.append("{");
				map = lm.get(i);
				Long child = (Long) map.get("child");
				Integer menuId = Integer.valueOf(map.get("id").toString());

				if (roleId != null && (roleMenuList == null || roleMenuList.isEmpty())) {
					RoleMenuModel tbsRoleMenuModel = new RoleMenuModel();
					tbsRoleMenuModel.setRoleId(Integer.valueOf(roleId));
					// tbsRoleMenuModel.setMenuIdFun(menuId);
					roleMenuList = new HashMap<Integer, Integer>();
					List<RoleMenuModel> roleList = tbsRoleMenuService.selectByModel(tbsRoleMenuModel);
					if (roleList != null) {
						for (RoleMenuModel m : roleList) {
							roleMenuList.put(m.getMenuIdFun(), 1);
						}
					}

				}
				if (roleMenuList != null && roleMenuList.containsKey(menuId)) {
					sb.append("\"checked\":true,");
				}
				// Integer parent=(Integer) map.get("parent");
				if (child > 0) {
					sb.append("\"state\":\"open\",");
					if (type != null && type != 0) { // 同步递归
						sb.append("\"children\":").append(this.comboxTree(menuId, roleId, type, roleMenuList) + ",");
					}
				}
				sb.append("\"id\":" + "\"" + menuId + "\",");
				sb.append("\"text\":" + "\"" + map.get("name") + "\"");
				sb.append("},");
			}
			sb = sb.delete(sb.length() - 1, sb.length());
			sb.append("]");
			return sb.toString();
		}
		return "[]";
	}

	public static void main(String[] args) {
		MenuModel m = new MenuModel();
		m.setCreateTime(Timestamp.valueOf("2013-09-11 12:11:11"));

		System.err.println(JSON.toJSONString(m, SerializerFeature.WriteDateUseDateFormat));
	}
}
