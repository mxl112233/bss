package org.offjob.boss.view.user;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.offjob.boss.domain.user.model.ConfigModel;
import org.offjob.boss.domain.user.service.ConfigServiceImpl;
import org.offjob.boss.utils.NumberUtils;
import org.offjob.boss.utils.PageParams;
import org.offjob.boss.view.BaseController;

@Controller
@RequestMapping("/admin/Config")
public class ConfigController extends BaseController {
	private final static Logger log = Logger.getLogger(ConfigController.class);

	// 服务类
	@Resource(name = "configService")
	private ConfigServiceImpl<ConfigModel> configService;

	@RequestMapping("index.htm")
	public ModelAndView index(String id, ModelMap modelMap, HttpServletRequest request) {
		return new ModelAndView("admin/Config/index", modelMap);
	}

	// @RequestMapping("show.htm")
	// public ModelAndView show(ModelMap modelMap, HttpServletRequest request) {
	// return new ModelAndView("admin/Config/setbaseDlg")
	// .addAllObjects(SystemCache.getSystemAll());
	// }

	// @RequestMapping("set.htm")
	// public void set(HttpServletResponse response, HttpServletRequest request)
	// {
	// MultipartHttpServletRequest multipartRequest =
	// (MultipartHttpServletRequest) request;
	// Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
	// try {
	// String logo = this.uploadLogo(request, fileMap);
	// changeValue("websiteLogo", logo);
	// changeValue("websiteName", request.getParameter("websiteName"));
	// changeValue("websiteTitle", request.getParameter("websiteTitle"));
	// changeValue("websiteStyle", request.getParameter("websiteStyle"));
	// changeValue("websiteKey", request.getParameter("websiteKey"));
	// changeValue("websiteDesc", request.getParameter("websiteDesc"));
	// changeValue("websitePublish", request
	// .getParameter("websitePublish"));
	// this.toJsonMsg(response, 0, null);
	// } catch (Exception e) {
	// util.toJsonMsg(response, 1, null);
	// log.error("ConfigModel save........", e);
	// }
	// }

	// private void changeValue(String key, String value) throws Exception {
	// if (!StringUtils.isEmpty(value)
	// && !value.equalsIgnoreCase(SystemCache.get(key))) {
	// ConfigModel configModel = new ConfigModel();
	// configModel.setConfigKey(key);
	// configModel.setConfigValue(value);
	// if (configService.updateByPrimaryKey(configModel) > 0) {
	// SystemCache.put(configModel.getConfigKey(), configModel
	// .getConfigValue());
	// }
	// }
	// }

	@RequestMapping("baseDlg.htm")
	public String baseDlg() {
		return "admin/Config/baseDlg";
	}

	@RequestMapping("data.htm")
	@ResponseBody
	public String data(PageParams pageParams, ConfigModel configModel) throws Exception {
		configModel.getPageUtil().setPageId(NumberUtils.parseInt(pageParams.getPage(), 1)); // 当前页
		configModel.getPageUtil().setPageSize(NumberUtils.parseInt(pageParams.getRows(), 10));// 显示X条
		StringBuilder center = new StringBuilder();
		List<ConfigModel> listConfigModel = configService.selectByModelPaging(configModel);
		center.append("{\"total\":\"" + configModel.getPageUtil().getRowCount() + "\",\"rows\":");
		center.append(JSON.toJSONString(listConfigModel, SerializerFeature.WriteDateUseDateFormat));
		center.append("}");
		return center.toString();
	}

	@RequestMapping("add.htm")
	public void add(ConfigModel configModel, HttpServletResponse response) {
		log.info("configModel:" + configModel.toString());
		try {
			if (configService.insert(configModel) > 0) {
				// SystemCache.put(configModel.getConfigKey(),
				// configModel.getConfigValue());
				this.toJsonMsg(response, 0, null);
				return;
			}
		} catch (Exception e) {
			log.error("ConfigModel add........", e);
		}
		this.toJsonMsg(response, 1, null);
	}

	@RequestMapping("save.htm")
	public void save(ConfigModel configModel, HttpServletResponse response) {
		try {
			if (configService.updateByPrimaryKey(configModel) > 0) {
				// SystemCache.put(configModel.getConfigKey(),
				// configModel.getConfigValue());
				this.toJsonMsg(response, 0, null);
				return;
			}
		} catch (Exception e) {
			this.toJsonMsg(response, 1, null);
			log.error("ConfigModel save........", e);
		}
	}

	@RequestMapping("del.htm")
	public void del(Integer[] ids, HttpServletResponse response) {
		log.info("del-ids:" + Arrays.toString(ids));
		try {
			if (null != ids && ids.length > 0&&configService.deleteByPrimaryKeys(ids) > 0) {
				this.toJsonMsg(response, 0, null);
				return;
			}
		} catch (Exception e) {
			this.toJsonMsg(response, 1, null);
			log.error("ConfigModel del..", e);
		}
	}
}
