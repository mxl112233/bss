package org.offjob.boss.view.interceptor;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import org.offjob.boss.utils.MethodUtil;

public class SecurityInterceptor implements HandlerInterceptor {

	private List<String> excludeUrls;

	public List<String> getExcludeUrls() {
		return excludeUrls;
	}

	public void setExcludeUrls(List<String> excludeUrls) {
		this.excludeUrls = excludeUrls;
	}

	/**
	 * 完成页面的render后调用
	 */
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object object,
			Exception exception) throws Exception {

	}

	/**
	 * 在调用controller具体方法后拦截
	 */
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object object,
			ModelAndView modelAndView) throws Exception {

	}

	/**
	 * 在调用controller具体方法前拦截
	 */
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {
		String requestUri = request.getRequestURI();
		String contextPath = request.getContextPath();
		String url = requestUri.substring(contextPath.length());
		if (excludeUrls.contains(url)) {
			return true;
		} else {
			UserToken tbsUserModel = SessionUtil.getUser(request);
			if (null != tbsUserModel && 0 == tbsUserModel.getIsAdmin()) {// 超管不需要验证权限
				return true;
			} else {
				@SuppressWarnings("unchecked")
				Set<String> urls = tbsUserModel.getAuthorizedUrl();
				if (null != urls && urls.contains(url)) {
					return true;
				} else {
					MethodUtil.getInstance().writer(response,
							"您没有访问此资源的权限！<br/>请联系管理员赋予您<br/>[" + url + "]<br/>的资源访问权限！");
					return false;
				}
			}
		}
	}
}
