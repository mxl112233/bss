package org.offjob.boss.view.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import org.offjob.boss.utils.MethodUtil;

public class SessionInterceptor implements HandlerInterceptor {

	private List<String> excludeUrls;

	public List<String> getExcludeUrls() {
		return excludeUrls;
	}

	public void setExcludeUrls(List<String> excludeUrls) {
		this.excludeUrls = excludeUrls;
	}

	/**
	 * 完成页面的render后调用
	 */
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object object,
			Exception exception) throws Exception {

	}

	/**
	 * 在调用controller具体方法后拦截
	 */
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object object,
			ModelAndView modelAndView) throws Exception {

	}

	/**
	 * 在调用controller具体方法前拦截
	 */
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {
		String requestUri = request.getRequestURI();
		String contextPath = request.getContextPath();
		String url = requestUri.substring(contextPath.length());
		if (excludeUrls.contains(url) || url.indexOf("/static/") >= 0 || !url.endsWith(".htm")) {
			return true;
		} else {
			UserToken tbsUserModel = SessionUtil.getUser(request);
			if (tbsUserModel != null && !StringUtils.isEmpty(tbsUserModel.getUsername())) {
				return true;
			} else {
				StringBuffer sb = new StringBuffer();
				sb.append("if(confirm(\"");
				sb.append("您还没有登录或登录已超时，请重新登录，然后再刷新本功能！");
				sb.append("\")){");
				sb.append("window.location.href =\"" + contextPath + "/admin/login.htm\"");
				sb.append("}");
				MethodUtil.getInstance().toSript(response, sb.toString());
				return false;
			}
		}
	}

}
