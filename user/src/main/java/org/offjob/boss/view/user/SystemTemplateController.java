package org.offjob.boss.view.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import org.offjob.boss.view.BaseController;

@Controller
@RequestMapping("/admin/template")
public class SystemTemplateController extends BaseController {

	protected static final Logger log = Logger.getLogger(SystemTemplateController.class);

	@RequestMapping("/index.htm")
	public String index() {
		return "/admin/SystemTemplate/template";
	}

	@RequestMapping("/save.htm")
	@ResponseBody
	public String save(HttpServletRequest request, HttpServletResponse response) {
		try {
			//String sql = request.getParameter("sql");
			// VelocityUtil.budileProject(sql, request.getParameter("pageName"),
			// request.getParameter("codePath"),
			// request);
			this.toJsonMsg(response, 0, null);

		} catch (Exception e) {
			this.toJsonMsg(response, 1, null);
			log.error("SystemTemplateController save fail.....", e);
		}
		return null;
	}
}
