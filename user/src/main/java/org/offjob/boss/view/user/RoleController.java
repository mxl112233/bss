package org.offjob.boss.view.user;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import org.offjob.boss.domain.user.model.MenuModel;
import org.offjob.boss.domain.user.model.RoleMenuModel;
import org.offjob.boss.domain.user.model.RoleModel;
import org.offjob.boss.domain.user.model.RoleUserModel;
import org.offjob.boss.domain.user.service.MenuServiceImpl;
import org.offjob.boss.domain.user.service.RoleMenuServiceImpl;
import org.offjob.boss.domain.user.service.RoleServiceImpl;
import org.offjob.boss.domain.user.service.RoleUserServiceImpl;
import org.offjob.boss.utils.NumberUtils;
import org.offjob.boss.utils.PageParams;
import org.offjob.boss.view.BaseController;
import org.offjob.boss.view.interceptor.SessionUtil;
import org.offjob.boss.view.interceptor.UserToken;

@Controller
@RequestMapping("/admin/Role")
public class RoleController extends BaseController {
	private final static Logger log = Logger.getLogger(RoleController.class);

	@Resource(name = "menuService")
	MenuServiceImpl<MenuModel> tbsMenuService;

	// 服务类
	@Resource(name = "roleService")
	private RoleServiceImpl<RoleModel> tbsRoleService;

	@RequestMapping("index.htm")
	public ModelAndView index(String id, ModelMap modelMap, HttpServletRequest request) {
		List<String> buttons = new java.util.ArrayList<String>();
		MenuModel tbsMenuModel = new MenuModel();
		MenuModel menuModel = new MenuModel();
		menuModel.setParentId(Integer.valueOf(id));
		menuModel.getPageUtil().setOrderByCondition("sort_number");
		UserToken tbsUserModel = SessionUtil.getUser(request);
		System.out.println("1111");
		List<MenuModel> list = null;
		try {
			if (null != tbsUserModel && 0 == tbsUserModel.getIsAdmin()) {// 超管不需要验证权限

				list = tbsMenuService.selectByModel(menuModel);
			} else {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("parent_id", id);
				map.put("orderByCondition", "sort_number");
				list = tbsMenuService.selectByButtons(map);
			}
			if (list != null && list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					tbsMenuModel = list.get(i);
					String button = tbsMenuModel.getButton();
					if (null != button && "null" != button) {
						buttons.add(button);// .replaceAll("&apos;",
											// "'").replaceAll("&quot;", "\"")
					}
				}
			}
		} catch (Exception e) {
			log.error("", e);
		}
		modelMap.addAttribute("buttons", buttons);
		return new ModelAndView("admin/Role/index", modelMap);
	}

	@RequestMapping("baseDlg.htm")
	public String baseDlg() {
		return "admin/Role/baseDlg";
	}

	@RequestMapping("importDlg.htm")
	public String importDlg() {
		return "admin/Role/importDlg";
	}

	@RequestMapping("searchDlg.htm")
	public String searchDlg() {
		return "admin/Role/searchDlg";
	}

	@RequestMapping("data.htm")
	@ResponseBody
	public String data(PageParams pageParams, RoleModel tbsRoleModel) throws Exception {
		tbsRoleModel.getPageUtil().setPageId(NumberUtils.parseInt(pageParams.getPage(), 1)); // 当前页
		tbsRoleModel.getPageUtil().setPageSize(NumberUtils.parseInt(pageParams.getRows(), 10));// 显示X条

		String str = "";
		String suffix = "}";
		if (pageParams.getGridName() != null) {
			str = "[";
			suffix = "]}";
		}
		List<RoleModel> listTbsRoleModel = null;
		StringBuilder center = new StringBuilder();

		if (pageParams.getSearchType() != null) {
			if (pageParams.getSearchType().equals("1")) { // 模糊搜索
				tbsRoleModel.getPageUtil().setLike(true);
			}
			listTbsRoleModel = tbsRoleService.selectByModel(tbsRoleModel);
			center.append("{\"total\":\"" + tbsRoleModel.getPageUtil().getRowCount() + "\",\"rows\":" + str);
		} else {
			if (pageParams.getGridName() == null) {
				listTbsRoleModel = tbsRoleService.selectByModel(tbsRoleModel);
				center.append("{\"total\":\"" + tbsRoleModel.getPageUtil().getRowCount() + "\",\"rows\":");
				suffix = "}";
			}
		}

		if (pageParams.getGridName() == null) { // datagrid
			center.append(JSON.toJSONString(listTbsRoleModel));
		}
		center.append(suffix);
		return center.toString();
	}

	// /////////////////////////////////在下面添加自定义的方法///////////////////////////////////
	@Resource(name = "roleMenuService")
	RoleMenuServiceImpl<RoleMenuModel> tbsRoleMenuService;
	@Resource(name = "roleUserService")
	RoleUserServiceImpl<RoleUserModel> tbsRoleUserService;

	@RequestMapping("add.htm")
	public void add(HttpServletResponse response, RoleModel tbsRoleModel, Integer... roleAuthTree) {
		RoleMenuModel tbsRoleMenuModel = new RoleMenuModel();
		try {
			tbsRoleService.insert(tbsRoleModel);// 入库
			if (null != roleAuthTree) {
				for (int i = 0; i < roleAuthTree.length; i++) {
					MenuModel model = new MenuModel();
					tbsRoleMenuModel.setRoleId(tbsRoleModel.getId());
					tbsRoleMenuModel.setMenuIdFun(roleAuthTree[i]);
					model.setId(roleAuthTree[i]);
					MenuModel tbsMenuModel = tbsMenuService.selectByEntity(model);
					if (tbsMenuModel.getIsMenu() != 0) {
						model.setId(tbsMenuModel.getParentId());
						tbsMenuModel = tbsMenuService.selectByEntity(model);
						tbsRoleMenuModel.setMenuId(tbsMenuModel.getId());
					} else {
						tbsRoleMenuModel.setMenuId(roleAuthTree[i]);
					}
					tbsRoleMenuService.insert(tbsRoleMenuModel);
				}
			}
			this.toJsonMsg(response, 0, null);
		} catch (Exception e) {
			this.toJsonMsg(response, 1, null);
			log.error("", e);
		}
	}

	@RequestMapping("save.htm")
	public void save(HttpServletResponse response, RoleModel tbsRoleModel, Integer... roleAuthTree) {
		if (tbsRoleModel.getId() != null) {
			RoleMenuModel tbsRoleMenuModel = new RoleMenuModel();
			tbsRoleMenuModel.setRoleId(tbsRoleModel.getId());
			try {
				tbsRoleMenuService.deleteByEntity(tbsRoleMenuModel);
				if (null != roleAuthTree) {
					for (int i = 0; i < roleAuthTree.length; i++) {
						MenuModel model = new MenuModel();
						tbsRoleMenuModel.setRoleId(tbsRoleModel.getId());
						tbsRoleMenuModel.setMenuIdFun(roleAuthTree[i]);
						model.setId(roleAuthTree[i]);
						MenuModel tbsMenuModel = tbsMenuService.selectByEntity(model);
						if (tbsMenuModel.getIsMenu() != 0) {
							model.setId(tbsMenuModel.getParentId());
							tbsMenuModel = tbsMenuService.selectByEntity(model);
							tbsRoleMenuModel.setMenuId(tbsMenuModel.getId());
						} else {
							tbsRoleMenuModel.setMenuId(roleAuthTree[i]);
						}
						tbsRoleMenuService.insert(tbsRoleMenuModel);
					}
				}
				tbsRoleService.updateByPrimaryKey(tbsRoleModel);
				this.toJsonMsg(response, 0, null);
			} catch (Exception e) {
				this.toJsonMsg(response, 1, null);
				log.error("", e);
			}
		}
	}

	@RequestMapping("del.htm")
	public void del(Integer[] ids, HttpServletResponse response) {
		log.debug("del-ids:" + Arrays.toString(ids));
		try {
			if (ids != null) {
				tbsRoleService.deleteByPrimaryKeys(ids);
				RoleMenuModel trmb = new RoleMenuModel();
				RoleUserModel trub = new RoleUserModel();
				for (int i = 0; i < ids.length; i++) {
					trmb.setRoleId(ids[i]);
					tbsRoleMenuService.deleteByEntity(trmb);
					trub.setRoleId(ids[i]);
					tbsRoleUserService.deleteByEntity(trub);
				}
				this.toJsonMsg(response, 0, null);
			}
		} catch (Exception e) {
			this.toJsonMsg(response, 1, null);
			log.error("", e);
		}
	}

}
