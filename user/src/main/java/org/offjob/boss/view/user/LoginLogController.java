package org.offjob.boss.view.user;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.offjob.boss.utils.Contants.TimeContants;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import org.offjob.boss.domain.user.model.LoginLogModel;
import org.offjob.boss.domain.user.model.MenuModel;
import org.offjob.boss.domain.user.service.LoginLogServiceImpl;
import org.offjob.boss.domain.user.service.MenuServiceImpl;
import org.offjob.boss.utils.NumberUtils;
import org.offjob.boss.utils.PageParams;
import org.offjob.boss.view.BaseController;

@Controller
@RequestMapping("/admin/LoginLog")
public class LoginLogController extends BaseController {

	@Resource(name = "menuService")
	MenuServiceImpl<MenuModel> tbsMenuService;

	// 服务类
	@Resource(name = "loginLogService")
	private LoginLogServiceImpl<LoginLogModel> tbsLoginLogService;

	@RequestMapping("index.htm")
	public ModelAndView index(String id, ModelMap modelMap, HttpServletRequest request) {
		return new ModelAndView("admin/LoginLog/index", modelMap);
	}

	@RequestMapping("charts.htm")
	public String charts() {
		return "admin/LoginLog/charts";
	}

	@RequestMapping("chartsJson.htm")
	public void chartsJson(HttpServletResponse response, String startTime, String endTime, String chartsName,
			Integer type) {
		String resultJson = "[]";
		String startTimeFormat = null;
		String endTimeFormat = null;
		String groupTimeFormat = null;
		String year = "%Y";
		String month = "%m";
		String day = "%d";
		String hour = "%H";
		String minute = "%i";
		String second = "%s";
		Map<String, String> paramMap = new HashMap<String, String>();
		if (null == chartsName) {
			chartsName = "line";
		}
		if (null == type) {
			type = 4;
		}
		switch (type) {
		case 1: // 年
			groupTimeFormat = year;
			break;
		case 2:// 月
			startTimeFormat = year + "-00";
			endTimeFormat = year + "-12";
			groupTimeFormat = year + "-" + month;
			break;
		case 3:// 日
			startTimeFormat = year + "-" + month + "-01";
			endTimeFormat = year + "-" + month + "-31";
			groupTimeFormat = year + "-" + month + "-" + day;
			break;
		case 4:// 时
			startTimeFormat = year + "-" + month + "-" + day + " "+ TimeContants.HOUR_START_STR;
			endTimeFormat = year + "-" + month + "-" + day + " "+TimeContants.HOUR_END_STR;
			groupTimeFormat = year + "-" + month + "-" + day + " " + hour;
			break;
		case 5:// 分
			startTimeFormat = year + "-" + month + "-" + day + " " + hour + ":"+TimeContants.MINUTE_START_STR;
			endTimeFormat = year + "-" + month + "-" + day + " " + hour + ":"+TimeContants.MINUTE_END_STR;
			groupTimeFormat = year + "-" + month + "-" + day + " " + hour + ":" + minute;
			break;
		case 6:// 秒
			startTimeFormat = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":"+TimeContants.SECOND_START_STR;
			endTimeFormat = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":"+TimeContants.SECOND_END_STR;
			groupTimeFormat = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
			break;
		default:
			break;
		}
		paramMap.put("startTimeFormat", startTimeFormat);
		paramMap.put("endTimeFormat", endTimeFormat);
		paramMap.put("groupTimeFormat", groupTimeFormat);
		this.toJsonPrint(response, resultJson);
	}

	@RequestMapping("searchDlg.htm")
	public String searchDlg() {
		return "admin/LoginLog/searchDlg";
	}

	@RequestMapping("data.htm")
	@ResponseBody
	public String data(PageParams pageParams, LoginLogModel tbsLoginLogModel) throws Exception {

		tbsLoginLogModel.getPageUtil().setPageId(NumberUtils.parseInt(pageParams.getPage(), 1)); // 当前页
		tbsLoginLogModel.getPageUtil().setPageSize(NumberUtils.parseInt(pageParams.getRows(), 10));// 显示X条

		String str = "";
		String suffix = "}";
		if (pageParams.getGridName() != null) {
			str = "[";
			suffix = "]}";
		}
		List<LoginLogModel> listTbsLoginLogModel = null;
		StringBuilder center = new StringBuilder();

		if (pageParams.getSearchType() != null) {
			if (pageParams.getSearchType().equals("1")) { // 模糊搜索
				tbsLoginLogModel.getPageUtil().setLike(true);
				listTbsLoginLogModel = tbsLoginLogService.selectByModelPaging(tbsLoginLogModel);
				center.append("{\"total\":\"" + tbsLoginLogModel.getPageUtil().getRowCount() + "\",\"rows\":" + str);
			}
		} else {
			if (pageParams.getGridName() == null) {
				listTbsLoginLogModel = tbsLoginLogService.selectByModelPaging(tbsLoginLogModel);
				center.append("{\"total\":\"" + tbsLoginLogModel.getPageUtil().getRowCount() + "\",\"rows\":");
				suffix = "}";
			} 
		}

		if (pageParams.getGridName() == null) { // datagrid
			center.append(JSON.toJSONString(listTbsLoginLogModel));
		}
		center.append(suffix);
		return center.toString();
	}

}
