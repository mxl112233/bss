package org.offjob.boss.domain.user.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import org.offjob.boss.domain.service.BaseServiceImpl;
import org.offjob.boss.domain.user.dao.LoginLogMapper;


@Service("loginLogService")
public class LoginLogServiceImpl<T> extends BaseServiceImpl<T> {
	@Resource(name = "loginLogMapper")
	private LoginLogMapper<T> mapper;

	@Override
	public LoginLogMapper<T> getMapper() {
		return mapper;
	}
}
