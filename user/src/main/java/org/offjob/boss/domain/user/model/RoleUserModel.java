package org.offjob.boss.domain.user.model;

import org.offjob.boss.domain.model.BaseModel;

public class RoleUserModel extends BaseModel {

	private Integer id; // 主键
	private Integer userId; // 用户主键
	private Integer roleId; // 角色主键
	private java.sql.Timestamp createTime; // 时间

	public java.sql.Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(java.sql.Timestamp createTime) {
		this.createTime = createTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	@Override
	public String toString() {
		return com.alibaba.fastjson.JSON.toJSONString(this);
	}

}
