package org.offjob.boss.domain.user.model;

import org.offjob.boss.domain.model.BaseModel;

/**
 * 
 * <br>
 * <b>功能：</b>映射类<br>
 * <b>作者：</b>mxl<br>
 * <b>日期：</b> Sat Jul 26 09:54:25 CST 2014 <br>
 * 
 * @return id
 */
public class ConfigModel extends BaseModel {

	private String configKey;
	private String configValue;
	private String configDesc;
	private String module;
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getConfigKey() {
		return configKey;
	}

	public void setConfigKey(String configKey) {
		this.configKey = configKey;
	}

	public String getConfigValue() {
		return configValue;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	public String getConfigDesc() {
		return configDesc;
	}

	public void setConfigDesc(String configDesc) {
		this.configDesc = configDesc;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String toString() {
		return com.alibaba.fastjson.JSON.toJSONString(this);
	}

}