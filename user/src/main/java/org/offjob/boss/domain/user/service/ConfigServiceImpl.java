package org.offjob.boss.domain.user.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import org.offjob.boss.domain.service.BaseServiceImpl;
import org.offjob.boss.domain.user.dao.ConfigMapper;

@Service("configService")
public class ConfigServiceImpl<T> extends BaseServiceImpl<T>{
	
	@Resource(name = "configMapper")
    private ConfigMapper<T> mapper;
    
    public ConfigMapper<T> getMapper() {
		return mapper;
	}
}
