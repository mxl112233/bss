package org.offjob.boss.domain.user.model;

import org.offjob.boss.domain.model.BaseModel;

/**
 * 
 * <br>
 * <b>功能：</b>映射类<br>
 * <b>作者：</b>mxl<br>
 * <b>日期：</b> Fri Sep 20 15:28:53 CST 2013 <br>
 * 
 * @return id
 */
public class DeptModel extends BaseModel{

       private Integer id;  
       private String name;  
       private String deptDesc;  
       private Integer parentId;  
   	
   	public Integer getId(){
	   return id;
	}
   
	public void setId(Integer id){
	   this.id=id;
	}
	
   	public String getName(){
	   return name;
	}
   
	public void setName(String name){
	   this.name=name;
	}
	
   	public String getDeptDesc(){
	   return deptDesc;
	}
   
	public void setDeptDesc(String deptDesc){
	   this.deptDesc=deptDesc;
	}
	
   	public Integer getParentId(){
	   return parentId;
	}
   
	public void setParentId(Integer parentId){
	   this.parentId=parentId;
	}
	
       @Override
	public String toString(){
	  return com.alibaba.fastjson.JSON.toJSONString(this);
	  // return ${SQL.toString}
    }
	
}