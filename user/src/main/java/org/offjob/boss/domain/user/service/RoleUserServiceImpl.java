package org.offjob.boss.domain.user.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import org.offjob.boss.domain.service.BaseServiceImpl;
import org.offjob.boss.domain.user.dao.RoleUserMapper;

@Service("roleUserService")
public class RoleUserServiceImpl<T> extends BaseServiceImpl<T> {
	@Resource(name = "roleUserMapper")
	private RoleUserMapper<T> mapper;

	@Override
	public RoleUserMapper<T> getMapper() {
		return mapper;
	}

}
