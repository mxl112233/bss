package org.offjob.boss.domain.user.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import org.offjob.boss.domain.service.BaseServiceImpl;
import org.offjob.boss.domain.user.dao.DeptMapper;

@Service("deptService")
public class DeptServiceImpl<T> extends BaseServiceImpl<T>{
	
	@Resource(name = "deptMapper")
    private DeptMapper<T> mapper;
    
    @Override
	public DeptMapper<T> getMapper() {
		return mapper;
	}
    
    /**
     * tree
     * @param map
     * @return
     * @throws java.sql.SQLException
     */
    public java.util.List<java.util.Map<String, Object>> selectByDeptOther(
			java.util.Map<String, Object> map) throws java.sql.SQLException {
		return mapper.selectByDeptOther(map);
	}
}
