package org.offjob.boss.domain.user.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import org.offjob.boss.domain.service.BaseServiceImpl;
import org.offjob.boss.domain.user.dao.MenuMapper;

@Service("menuService")
public class MenuServiceImpl<T> extends BaseServiceImpl<T> {
	@Resource(name = "menuMapper")
	private MenuMapper<T> mapper;

	@Override
	public MenuMapper<T> getMapper() {
		return mapper;
	}

	public List<Map<String, Object>> selectByMenuIsMenu(
			Map<String, Object> map) throws SQLException {
		return mapper.selectByMenuIsMenu(map);
	}

	public List<Map<String, Object>> selectByMenuOther(
			Map<String, Object> map) throws SQLException {
		return mapper.selectByMenuOther(map);
	}

	public List<T> selectByButtons(Map<String, Object> map) throws SQLException {
		return mapper.selectByButtons(map);
	}

}
