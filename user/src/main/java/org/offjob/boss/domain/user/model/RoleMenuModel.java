package org.offjob.boss.domain.user.model;

import org.offjob.boss.domain.model.BaseModel;

public class RoleMenuModel extends BaseModel {

	private Integer id; // 主键
	private Integer roleId; // 角色主键
	private Integer menuIdFun; // 功能主键
	private Integer menuId; // 菜单主键
	private java.sql.Timestamp createTime; // 时间

	@Override
	public String toString() {
		return com.alibaba.fastjson.JSON.toJSONString(this);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getMenuIdFun() {
		return menuIdFun;
	}

	public void setMenuIdFun(Integer menuIdFun) {
		this.menuIdFun = menuIdFun;
	}

	public Integer getMenuId() {
		return menuId;
	}

	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}

	public java.sql.Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(java.sql.Timestamp createTime) {
		this.createTime = createTime;
	}

}
