package org.offjob.boss.domain.user.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import org.offjob.boss.domain.service.BaseServiceImpl;
import org.offjob.boss.domain.user.dao.RoleMapper;

@Service("roleService")
public class RoleServiceImpl<T> extends BaseServiceImpl<T> {

	@Resource(name = "roleMapper")
	private RoleMapper<T> mapper;

	@Override
	public RoleMapper<T> getMapper() {
		return mapper;
	}

}
