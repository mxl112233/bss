package org.offjob.boss.domain.user.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import org.offjob.boss.domain.service.BaseServiceImpl;
import org.offjob.boss.domain.user.dao.RoleMenuMapper;

@Service("roleMenuService")
public class RoleMenuServiceImpl<T> extends BaseServiceImpl<T> {
	@Resource(name = "roleMenuMapper")
	private RoleMenuMapper<T> mapper;

	@Override
	public RoleMenuMapper<T> getMapper() {
		return mapper;
	}

}
