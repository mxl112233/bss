package org.offjob.boss.domain.user.dao;

import org.offjob.boss.domain.dao.BaseMapper;
/**
 * 
 * <br>
 * <b>功能：</b>定义在这里由Mapper映射文件来实现 私有的<br>
 * <b>作者：</b>mxl<br>
 * <b>日期：</b> Sat Jul 26 09:54:25 CST 2014 <br>
 * <b>版权所有：<b>版权所有(C) 2013<br>
 */
public interface ConfigMapper<T> extends BaseMapper<T> {
}
