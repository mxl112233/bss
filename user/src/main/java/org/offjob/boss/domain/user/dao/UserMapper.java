package org.offjob.boss.domain.user.dao;

import org.offjob.boss.domain.dao.BaseMapper;

/**
 * 
 * <br>
 * <b>功能：</b>定义在这里由Mapper映射文件来实现 私有的<br>
 * <b>作者：</b>mxl<br>
 * <b>日期：</b> 2013-5-24 <br>
 * <b>版权所有：<b>版权所有<br>
 */
public interface UserMapper<T> extends BaseMapper<T> {
	java.util.List<java.util.Map<String, Object>> selectByRoleUrls(
			java.util.Map<?, ?> map) throws Exception;

}
