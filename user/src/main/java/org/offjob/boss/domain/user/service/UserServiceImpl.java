package org.offjob.boss.domain.user.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import org.offjob.boss.domain.service.BaseServiceImpl;
import org.offjob.boss.domain.user.dao.UserMapper;

@Service("userService")
public class UserServiceImpl<T> extends BaseServiceImpl<T>{
	
	@Resource(name="userMapper")
	private UserMapper<T> mapper;

	@Override
	public UserMapper<T> getMapper() {
		return mapper;
	}

	public java.util.List<java.util.Map<String, Object>> selectByRoleUrls(
			java.util.Map<?, ?> map) throws Exception {
		return mapper.selectByRoleUrls(map);
	}
}
