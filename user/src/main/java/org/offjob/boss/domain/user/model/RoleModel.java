package org.offjob.boss.domain.user.model;

import org.offjob.boss.domain.model.BaseModel;

public class RoleModel extends BaseModel {

	private Integer id; // 主键
	private String name; // 角色
	private String text; // 权限名称
	private java.sql.Timestamp createTime; // 创建时间

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 
	 * <br>
	 * <b>功能：</b>角色<br>
	 * <b>作者：</b>湘林<br>
	 * <b>日期：</b> 2013-4-26 <br>
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * <br>
	 * <b>功能：</b>角色<br>
	 * <b>作者：</b>湘林<br>
	 * <b>日期：</b> 2013-4-26 <br>
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * <br>
	 * <b>功能：</b>权限名称<br>
	 * <b>作者：</b>湘林<br>
	 * <b>日期：</b> 2013-4-26 <br>
	 * 
	 * @return text
	 */
	public String getText() {
		return text;
	}

	/**
	 * 
	 * <br>
	 * <b>功能：</b>权限名称<br>
	 * <b>作者：</b>湘林<br>
	 * <b>日期：</b> 2013-4-26 <br>
	 * 
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * 
	 * <br>
	 * <b>功能：</b>创建时间<br>
	 * <b>作者：</b>湘林<br>
	 * <b>日期：</b> 2013-4-26 <br>
	 * 
	 * @return createTime
	 */
	public java.sql.Timestamp getCreateTime() {
		return createTime;
	}

	/**
	 * 
	 * <br>
	 * <b>功能：</b>创建时间<br>
	 * <b>作者：</b>湘林<br>
	 * <b>日期：</b> 2013-4-26 <br>
	 * 
	 * @param createTime
	 */
	public void setCreateTime(java.sql.Timestamp createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return com.alibaba.fastjson.JSON.toJSONString(this);
	}

}
