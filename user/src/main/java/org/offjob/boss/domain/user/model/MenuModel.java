package org.offjob.boss.domain.user.model;

import org.offjob.boss.domain.model.BaseModel;

import java.io.Serializable;

public class MenuModel extends BaseModel implements Serializable {
	private static final long serialVersionUID = -1689106348051690347L;

	private Integer id; // 唯一
	private Integer parentId; // 父节点
	private String name; // 名称
	private Integer isMenu; // 菜单类型
	private Integer type; // 系统类型
	private Integer sortNumber; // 排序
	private String url; // 地址
	private String button; // 按钮
	private Integer status; // 状态
	private java.sql.Timestamp createTime; // 时间

	private java.util.List<MenuModel> listTbsMenuModel;

	public String getButton() {
		return button;
	}

	public java.util.List<MenuModel> getListTbsMenuModel() {
		return listTbsMenuModel;
	}

	public void setListTbsMenuModel(java.util.List<MenuModel> listTbsMenuModel) {
		this.listTbsMenuModel = listTbsMenuModel;
	}

	public void setButton(String button) {
		this.button = button;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getIsMenu() {
		return isMenu;
	}

	public void setIsMenu(Integer isMenu) {
		this.isMenu = isMenu;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getSortNumber() {
		return sortNumber;
	}

	public void setSortNumber(Integer sortNumber) {
		this.sortNumber = sortNumber;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public java.sql.Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(java.sql.Timestamp createTime) {
		this.createTime = createTime;
	}

}
