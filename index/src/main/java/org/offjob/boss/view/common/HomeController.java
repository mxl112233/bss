package org.offjob.boss.view.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import org.offjob.boss.domain.user.model.DeptModel;
import org.offjob.boss.domain.user.service.DeptServiceImpl;
import org.offjob.boss.view.BaseController;

@Controller
@RequestMapping("/Home/")
public class HomeController extends BaseController {
	private final static Logger log = Logger.getLogger(HomeController.class);

	@Resource(name = "deptService")
	private DeptServiceImpl<DeptModel> deptService;

	@RequestMapping("index.htm")
	public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView("common/noticeIndex");
	}

	@RequestMapping("moreNotice.htm")
	public ModelAndView moreNotice(HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView("common/moreNotice");
	}


	@RequestMapping("dataList.htm")
	public ModelAndView dataList(HttpServletRequest request) throws Exception {
		return new ModelAndView("common/shownoticeView").addObject("noticeList", new ArrayList<Object>());
	}

	@RequestMapping("comboxTree.htm")
	@ResponseBody
	public String comboxTree(String id) throws Exception {
		List<Map<String, Object>> lm;
		Map<String, Object> map = new HashMap<String, Object>();
		if (null == id) {
			map.put("andCondition", "parent_id IS NULL");
			lm = deptService.selectByDeptOther(map);
		} else {
			map.put("parentId", id);
			lm = deptService.selectByDeptOther(map);
		}
		map.clear();
		if (lm != null && lm.size() > 0) {
			StringBuffer sb = new StringBuffer();
			sb.append("[");
			for (int i = 0; i < lm.size(); i++) {
				sb.append("{");
				map = lm.get(i);
				Long child = (Long) map.get("child");
				Integer pkid = (Integer) map.get("id");
				if (child > 0) {
					sb.append("\"state\":\"closed\",");
				}
				sb.append("\"id\":" + "\"" + pkid + "\",");
				sb.append("\"text\":" + "\"" + map.get("name") + "\"");
				sb.append("},");
			}
			sb = sb.delete(sb.length() - 1, sb.length());
			sb.append("]");
			log.debug("json:" + sb.toString());
			return sb.toString();
		}
		return "[]";
	}
}
