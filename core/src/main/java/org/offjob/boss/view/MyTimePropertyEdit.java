package org.offjob.boss.view;

import java.beans.PropertyEditorSupport;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;

/**
 * <br>
 * <b>功能：</b>类功能描述<br>
 * <b>作者：</b>mxl<br>
 * <b>日期：</b> 2012-6-19 <br>
 * <b>版权所有：<b>版权所有(C) <br>
 * <b>更新者：</b><br>
 * <b>日期：</b> <br>
 * <b>更新内容：</b><br>
 */
public class MyTimePropertyEdit extends PropertyEditorSupport {
	private final static Logger log = Logger.getLogger(MyTimePropertyEdit.class);

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		try {
			if (text.length() == 8) {
				setValue(new Time(new SimpleDateFormat("HH:mm:ss").parse(text).getTime()));
			}
		} catch (ParseException e) {
			log.error("", e);
		}
	}

}