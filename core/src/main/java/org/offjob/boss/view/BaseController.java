package org.offjob.boss.view;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.alibaba.fastjson.JSON;
import org.offjob.boss.utils.BrowserUtils;
import org.offjob.boss.utils.JsonUtil;

public class BaseController extends MultiActionController {
	protected final Logger log = Logger.getLogger(BaseController.class);

	@InitBinder
	// // 必须有一个参数WebDataBinder 日期类型装换 --如果需要为util的data类型
	// controller需要重写此方法---400请求错误也是类型转换的问题
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Timestamp.class, new MyTimestampPropertyEdit()); // 使用自定义属性编辑器
		binder.registerCustomEditor(Date.class, new MyDatePropertyEdit()); // 使用自定义属性编辑器
		binder.registerCustomEditor(Time.class, new MyTimePropertyEdit()); // 使用自定义属性编辑器
	}

	public void writer(HttpServletResponse response, String str) {
		PrintWriter out = null;
		try {
			// 设置页面不缓存
			response.setHeader("Pragma", "No-cache");
			response.setHeader("Cache-Control", "no-cache");
			response.setCharacterEncoding("UTF-8");
			out = response.getWriter();
			out.print(str);
			out.flush();
		} catch (IOException e) {
			log.error("writer error........", e);
		} finally {
			IOUtils.closeQuietly(out);
		}
	}

	public void toJsonMsg(HttpServletResponse response, AjaxJson ajaxJson) {
		if (ajaxJson.isSuccess()) {
			this.toJsonMsg(response, 0, null);
		} else {
			log.warn("------->>>>>>toJsonMsg error msg:" + ajaxJson.getMsg());
			this.toJsonMsg(response, 1, ajaxJson.getMsg());
		}
	}

	public void downLoad(HttpServletResponse response, String fileName, ByteArrayOutputStream os) throws Exception {
		OutputStream out = null;
		try {
			// 创建输出exl
			response.reset();
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition",
					"attachment; filename=" + new String(fileName.getBytes("gb2312"), "ISO8859-1") + ".xlsx");
			response.setContentLength(os.size());
			out = response.getOutputStream();
			os.writeTo(out);
			out.flush();
		} finally {
			IOUtils.closeQuietly(os);
			IOUtils.closeQuietly(out);
		}
	}

	public void toJsonMsg(HttpServletResponse response, int type, String msg) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("state", type);
		if (type == 0) {
			map.put("success", true);
			msg = StringUtils.isEmpty(msg) ? "成功" : msg;
		} else {
			map.put("success", false);
			msg = StringUtils.isEmpty(msg) ? "失败" : msg;
		}
		map.put("msg", msg);
		this.toJsonPrint(response, JSON.toJSONString(map));
	}

	public void toJsonPrint(HttpServletResponse response, String str) {
		response.setContentType("text/html;charset=UTF-8");
		this.writer(response, str);
	}

	public void toJson(Object t, HttpServletRequest request, HttpServletResponse response) {
		Assert.isNull(t, "object is null");
		// 不支持跨域 ---以下解决火狐和ie下乱码问题
		if (request != null && BrowserUtils.isIE(request)) {
			response.setContentType("application/json;charset=utf-8");
		}
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		PrintWriter pw = null;
		try {
			pw = response.getWriter();
			if (t instanceof String) {
				pw.write(t.toString());
			} else {
				pw.write(JsonUtil.beanToJSON(t));
			}
			pw.flush();
		} catch (Exception e) {
			log.error("", e);
		} finally {
			IOUtils.closeQuietly(pw);
		}
	}
}
