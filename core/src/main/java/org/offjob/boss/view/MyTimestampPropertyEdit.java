package org.offjob.boss.view;

import java.beans.PropertyEditorSupport;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;

/**
 * <br>
 * <b>功能：</b>类功能描述<br>
 * <b>作者：</b>mxl<br>
 * <b>日期：</b> 2012-6-19 <br>
 * <b>版权所有：<b>版权所有(C) <br>
 * <b>更新者：</b><br>
 * <b>日期：</b> <br>
 * <b>更新内容：</b><br>
 */
public class MyTimestampPropertyEdit extends PropertyEditorSupport {
	private final static Logger log = Logger
			.getLogger(MyTimestampPropertyEdit.class);

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		try {
			if (text.length() == 19) {
				setValue(new Timestamp(new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss").parse(text).getTime()));
			} else if (text.length() == 10) {
				setValue(new Timestamp(new SimpleDateFormat(
						"yyyy-MM-dd").parse(text).getTime()));
			}
		} catch (ParseException e) {
			log.error("", e);
		}
	}

}