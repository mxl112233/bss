package org.offjob.boss.domain.service;

import java.util.List;
import java.util.Map;

import org.offjob.boss.domain.dao.BaseMapper;
import org.offjob.boss.domain.model.BaseModel;
import org.offjob.boss.domain.model.PageUtil;

public class BaseServiceImpl<T> {

	private BaseMapper<T> mapper;

	public BaseMapper<T> getMapper() {
		return mapper;
	}

	/**
	 * 
	 * <br>
	 * <b>功能：</b>主键查询<br>
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public T selectByPrimaryKey(Object key) throws Exception {
		return getMapper().selectByPrimaryKey(key);
	}

	/**
	 * 
	 * <br>
	 * 
	 * @param t
	 * @throws Exception
	 */
	public Integer updateByPrimaryKey(T t) throws Exception {
		return getMapper().updateByPrimaryKey(t);
	}

	/**
	 * 
	 * <br>
	 * <b>功能：</b>主键删除<br>
	 * 
	 * @param keys
	 * @throws Exception
	 */
	public Integer deleteByPrimaryKeys(Object... keys) throws Exception {
		int i = 0;
		for (Object key : keys) {
			i += getMapper().deleteByPrimaryKey(key);
		}
		return i;
	}

	public Integer deleteByEntity(T entity) throws Exception {
		return getMapper().deleteByEntity(entity);
	}

	/**
	 * 
	 * <br>
	 * <b>功能：</b>插入<br>
	 * 
	 * @param t
	 * @throws Exception
	 */
	public Integer insert(T t) throws Exception {
		return getMapper().insert(t);
	}

	/**
	 * 
	 * <br>
	 * <b>功能：</b>模型不分页<br>
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public List<T> selectByModel(BaseModel model) throws Exception {
		cleanPage(model.getPageUtil());
		return getMapper().selectByModel(model);
	}

	private void cleanPage(PageUtil page) {
		// page.setAndCondition(null);
		// page.setOrderByCondition(null);
		// page.setQueryCondition(null);
		page.setPaging(false);
	}

	public T selectByEntity(BaseModel model) throws Exception {
		cleanPage(model.getPageUtil());
		List<T> list = getMapper().selectByModel(model);
		return list != null && !list.isEmpty() ? list.get(0) : null;
	}

	/**
	 * 
	 * <br>
	 * <b>功能：</b>模型分页<br>
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public List<T> selectByModelPaging(BaseModel model) throws Exception {
		model.getPageUtil().setRowCount(getMapper().selectByModelCount(model));
		return getMapper().selectByModel(model);
	}

	public Integer selectByModelCount(BaseModel model) throws Exception {
		return getMapper().selectByModelCount(model);
	}

	/**
	 * 
	 * <br>
	 * <b>功能：</b>查询分页<br>
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public List<T> selectByMapPaging(Map<?, ?> map) throws Exception {
		int rowCount = getMapper().selectByMapCount(map);
		PageUtil pageUtil = (PageUtil) map.get("pageUtil");
		if (null == pageUtil) {
			throw new IllegalArgumentException("pageUtil not init...");
		}
		pageUtil.setRowCount(rowCount);
		// 可以优化 如果结果为0直接返回空List
		return getMapper().selectByMap(map);
	}

	public List<T> selectByMap(Map<?, ?> map) throws Exception {
		return getMapper().selectByMap(map);
	}

	public List<Map<String, Object>> queryByMap(Map<?, ?> map) throws Exception {
		return getMapper().queryByMap(map);
	}
	//
	// public T selectByEntity(Map<?, ?> map) throws Exception {
	// model.setPageUtil(null);
	// List<T> list = getMapper().selectByModel(model);
	// return list != null && !list.isEmpty() ? list.get(0) : null;
	// }

	/**
	 * 
	 * <br>
	 * <b>功能：</b>删除一条记录<br>
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public Integer deleteByPrimaryKey(Object key) throws Exception {
		return getMapper().deleteByPrimaryKey(key);
	}
}
