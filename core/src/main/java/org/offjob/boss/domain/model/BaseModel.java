package org.offjob.boss.domain.model;

public class BaseModel {

	private PageUtil pageUtil =new PageUtil();

	public PageUtil getPageUtil() {
		return pageUtil;
	}

	public void setPageUtil(PageUtil pageUtil) {
		this.pageUtil = pageUtil;
	}

}
