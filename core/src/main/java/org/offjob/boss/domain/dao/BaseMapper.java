package org.offjob.boss.domain.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import org.offjob.boss.domain.model.BaseModel;
/**
 * 
 * <br>
 * <b>功能：</b>定义在这里是公用的 由Mapper配置文件实现<br>
 */
public interface BaseMapper<T> {
	/***************** CRUD操作 ********************/
	T selectByPrimaryKey(Object key) throws Exception;

	Integer updateByPrimaryKey(T t) throws Exception;

	Integer deleteByPrimaryKey(Object key) throws Exception;

	Integer insert(T t) throws Exception;

	Integer deleteByEntity(T entity) throws Exception;

	List<T> selectBySql(@Param(value = "sql") String sql) throws Exception;

	Integer updateBySql(@Param(value = "sql") String sql) throws Exception;

	Integer deleteBySql(@Param(value = "sql") String sql) throws Exception;

	/*********************** 分页查询操作 ************************/
	Integer selectByModelCount(BaseModel model) throws Exception;

	List<T> selectByModel(BaseModel model) throws Exception;

	Integer selectByMapCount(Map<?, ?> map) throws Exception;

	List<T> selectByMap(Map<?, ?> map) throws Exception;

	/*********************** 查询不分页 *************************/
	List<Map<String, Object>> queryByMap(Map<?, ?> map) throws Exception;
}
