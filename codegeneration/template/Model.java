package ${PACKAGEPATHROOT}.model;

import org.offjob.boss.domain.model.BaseModel;

public class ${TABLENAME}Model extends BaseModel{

   #foreach($prop in $columnList)  
    private ${prop.fieldType} ${prop.fieldName};  
   #end  
	
   #foreach($prop in $columnList)
	public ${prop.fieldType} get${prop.fieldNameUpper}(){
	   return ${prop.fieldName};
	}
   
	public void set${prop.fieldNameUpper}(${prop.fieldType} ${prop.fieldName}){
	   this.${prop.fieldName}=${prop.fieldName};
	}
	
   #end	
    public String toString(){
	  return com.alibaba.fastjson.JSON.toJSONString(this);
    }
	
}