package ${PACKAGEPATHROOT}.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import ${PACKAGEPATHROOT}.dao.${TABLENAME}Mapper;
import org.offjob.boss.domain.service.BaseServiceImpl;

@Service("${TABLENAMES}Service")
public class ${TABLENAME}ServiceImpl<T> extends BaseServiceImpl<T>{
	
	@Resource(name = "${TABLENAMES}Mapper")
    private ${TABLENAME}Mapper<T> mapper;
    
    public ${TABLENAME}Mapper<T> getMapper() {
		return mapper;
	}
}
