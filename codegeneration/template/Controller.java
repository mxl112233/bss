package ${PACKAGEPATHC};

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.offjob.boss.utils.NumberUtils;
import org.offjob.boss.utils.PageParams;
import org.offjob.boss.view.BaseController;
import ${PACKAGEPATHROOT}.model.${TABLENAME}Model;
import ${PACKAGEPATHROOT}.service.${TABLENAME}ServiceImpl;



@Controller
#if(${ROLE}=='admin')
@RequestMapping("/admin/${MODULENAME}/${TABLENAME}")
#else  
@RequestMapping("/${MODULENAME}/${TABLENAME}")
#end	
public class ${TABLENAME}Controller extends BaseController {
	private final static Logger log = Logger
			.getLogger(${TABLENAME}Controller.class);
	
	@Resource(name="${TABLENAMES}Service")
	private ${TABLENAME}ServiceImpl<${TABLENAME}Model> ${TABLENAMES}Service;

	@RequestMapping("index.htm")
	public ModelAndView index(String id, ModelMap modelMap,
			HttpServletRequest request) {
		#if(${ROLE}=='admin')
		return new ModelAndView("view/admin/${MODULENAME}/${TABLENAME}/index", modelMap);
		#else  
		return new ModelAndView("view/common/${MODULENAME}/${TABLENAME}/index", modelMap);
		#end
	}

	@RequestMapping("baseDlg.htm")
	public String baseDlg() {
		#if(${ROLE}=='admin')
		return "view/admin/${MODULENAME}/${TABLENAME}/baseDlg";
		#else  
		return "view/common/${MODULENAME}/${TABLENAME}/baseDlg";
		#end
	}

	@RequestMapping("data.htm")
	@ResponseBody
	public String data(PageParams pageParams, ${TABLENAME}Model ${TABLENAMES}Model)
			throws Exception {
		log.info("pageParams:" + pageParams + "|tbs${TABLENAME}Model:"
				+ ${TABLENAMES}Model);
		${TABLENAMES}Model.getPageUtil().setPageId(NumberUtils.parseInt(pageParams.getPage(), 1)); // 当前页
		${TABLENAMES}Model.getPageUtil().setPageSize(NumberUtils.parseInt(pageParams.getRows(), 10));// 显示X条

		StringBuilder center = new StringBuilder();
		if ("1".equalsIgnoreCase(pageParams.getSearchType())) {
			${TABLENAMES}Model.getPageUtil().setLike(false);
		}
		List<${TABLENAME}Model> list${TABLENAME}Model = ${TABLENAMES}Service.selectByModelPaging(${TABLENAMES}Model);
		center.append("{\"total\":\""+ ${TABLENAMES}Model.getPageUtil().getRowCount()+ "\",\"rows\":");
		if (list${TABLENAME}Model == null || list${TABLENAME}Model.isEmpty()) {
			center.append("[]");
		} else {
			center.append(JSON.toJSONString(list${TABLENAME}Model,SerializerFeature.WriteDateUseDateFormat));
		}
		center.append("}");
		return center.toString();
	}

	@RequestMapping("add.htm")
	public void add(${TABLENAME}Model ${TABLENAMES}Model, HttpServletResponse response) {
		log.info("${TABLENAMES}Model:" + ${TABLENAMES}Model.toString());
		try {
			if (${TABLENAMES}Service.insert(${TABLENAMES}Model) > 0) {
				this.toJsonMsg(response, 0, null);
				return;
			}
		} catch (Exception e) {
			log.error("${TABLENAMES}Service.insert fail.......",e);
		}
		this.toJsonMsg(response, 1, null);
	}

	@RequestMapping("save.htm")
	public void save(${TABLENAME}Model ${TABLENAMES}Model, HttpServletResponse response) {
		try {
			if (${TABLENAMES}Service.updateByPrimaryKey(${TABLENAMES}Model) > 0) {
				this.toJsonMsg(response, 0, null);
				return;
			}
		} catch (Exception e) {
			log.error("${TABLENAMES}Service.updateByPrimaryKey fail.......",e);
			this.toJsonMsg(response, 1, null);
		}
	}

	@RequestMapping("del.htm")
	#if(${TABLEPKTYPE}==1)
	public void del(Integer[] ids, HttpServletResponse response) {
    #else  
	public void del(String[] ids, HttpServletResponse response) {
	#end
		log.info("del-ids:" + Arrays.toString(ids));
		try {
			if (null != ids && ids.length > 0&&${TABLENAMES}Service.deleteByPrimaryKeys(ids) > 0) {
				this.toJsonMsg(response, 0, null);
			}
		} catch (Exception e) {
			this.toJsonMsg(response, 1, null);
			log.error("del fail.....",e);
		}
	}
}
