package ${PACKAGEPATHROOT}.dao;

import org.offjob.boss.domain.dao.BaseMapper;

public interface ${TABLENAME}Mapper<T> extends BaseMapper<T> {
}
