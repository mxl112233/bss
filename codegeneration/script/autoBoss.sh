#!/bin/bash

nowDate=`date +%Y-%m-%d`

pathProj=/home/oracle/tomcat/webapps
warPathProj=/home/oracle/war
warSrvNew="$pathProj/ROOT.war"

warname=`ls -t $warPathProj | head -1`

warSrvOrig=$warPathProj/$warname

echo 'get new war name='$warSrvOrig

echo 'enter to stop tomcat...'

pid=`ps aux|grep "java"|grep "/home/oracle/tomcat"|awk '{printf $2}'`

kill -9 $pid

echo 'stop tomcat finished...pid='$pid

echo 'enter delete tomcat webapps project....'

rm -rf $pathProj/ROOT

rm -rf $pathProj/ROOT.war

echo 'delete project suc.......'

cp $warSrvOrig $warSrvNew

echo 'copy web finish...war='$warSrvOrig

echo 'restart tomcat7 bossWeb...'

pid=`ps aux|grep "java"|grep "/home/oracle/tomcat"|awk '{printf $2}'`

kill -9 $pid

sleep 1

/home/oracle/tomcat/bin/startup.sh

echo 'show logs...'

tail -100f /home/oracle/tomcat/logs/catalina.$nowDate.log
