# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/.local/bin:$HOME/bin

export PATH

nowDate=`date +%Y-%m-%d`

restartBoss() {
        echo 'restart tomcat7 bossWeb...'
        pid=`ps aux|grep "java"|grep "/home/oracle/tomcat"|awk '{printf $2}'`
        echo 'tomcat7 pid='$pid
        kill -9 $pid
        sleep 2
        /home/oracle/tomcat/bin/startup.sh
        echo 'show logs...'
        tail -100f /home/oracle/tomcat/logs/catalina.$nowDate.log
}

alias rB='restartBoss'

stopBoss(){
        echo 'shutdown tomcat7 bossWeb...'
        pid=`ps aux|grep "java"|grep "/home/oracle/tomcat"|awk '{printf $2}'`
        kill -9 $pid
        echo 'shutdown tomcat7 finished...'
}

alias sB='stopBoss'

showBossLog(){
       tail -100f /home/oracle/tomcat/logs/catalina.$nowDate.log
}

alias tB='showBossLog'

showBsLog(){
       tail -100f /home/oracle/log/app-intra.log
}

alias blog='showBsLog'

showLog(){
       view /home/oracle/log/app-intra.log
}

alias slog='showLog'

alias deploy='/home/oracle/autoBoss.sh'
