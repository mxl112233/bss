/*
Navicat MySQL Data Transfer

Source Server         : 120.26.108.220
Source Server Version : 50505
Source Host           : 120.26.108.220:3306
Source Database       : boss

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-03-21 15:23:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `t_city`
-- ----------------------------
DROP TABLE IF EXISTS `t_city`;
CREATE TABLE `t_city` (
  `city_no` int(11) NOT NULL COMMENT '城市编号',
  `province_no` int(11) DEFAULT NULL COMMENT '省份编号',
  `city_name` varchar(255) NOT NULL COMMENT '城市名称',
  `zip_code` varchar(255) NOT NULL COMMENT '邮编',
  PRIMARY KEY (`city_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_city
-- ----------------------------
INSERT INTO `t_city` VALUES ('1', '11', '北京市', '100000');
INSERT INTO `t_city` VALUES ('2', '12', '天津市', '100000');
INSERT INTO `t_city` VALUES ('3', '13', '石家庄市', '050000');
INSERT INTO `t_city` VALUES ('4', '13', '唐山市', '063000');
INSERT INTO `t_city` VALUES ('5', '13', '秦皇岛市', '066000');
INSERT INTO `t_city` VALUES ('6', '13', '邯郸市', '056000');
INSERT INTO `t_city` VALUES ('7', '13', '邢台市', '054000');
INSERT INTO `t_city` VALUES ('8', '13', '保定市', '071000');
INSERT INTO `t_city` VALUES ('9', '13', '张家口市', '075000');
INSERT INTO `t_city` VALUES ('10', '13', '承德市', '067000');
INSERT INTO `t_city` VALUES ('11', '13', '沧州市', '061000');
INSERT INTO `t_city` VALUES ('12', '13', '廊坊市', '065000');
INSERT INTO `t_city` VALUES ('13', '13', '衡水市', '053000');
INSERT INTO `t_city` VALUES ('14', '14', '太原市', '030000');
INSERT INTO `t_city` VALUES ('15', '14', '大同市', '037000');
INSERT INTO `t_city` VALUES ('16', '14', '阳泉市', '045000');
INSERT INTO `t_city` VALUES ('17', '14', '长治市', '046000');
INSERT INTO `t_city` VALUES ('18', '14', '晋城市', '048000');
INSERT INTO `t_city` VALUES ('19', '14', '朔州市', '036000');
INSERT INTO `t_city` VALUES ('20', '14', '晋中市', '030600');
INSERT INTO `t_city` VALUES ('21', '14', '运城市', '044000');
INSERT INTO `t_city` VALUES ('22', '14', '忻州市', '034000');
INSERT INTO `t_city` VALUES ('23', '14', '临汾市', '041000');
INSERT INTO `t_city` VALUES ('24', '14', '吕梁市', '030500');
INSERT INTO `t_city` VALUES ('25', '15', '呼和浩特市', '010000');
INSERT INTO `t_city` VALUES ('26', '15', '包头市', '014000');
INSERT INTO `t_city` VALUES ('27', '15', '乌海市', '016000');
INSERT INTO `t_city` VALUES ('28', '15', '赤峰市', '024000');
INSERT INTO `t_city` VALUES ('29', '15', '通辽市', '028000');
INSERT INTO `t_city` VALUES ('30', '15', '鄂尔多斯市', '010300');
INSERT INTO `t_city` VALUES ('31', '15', '呼伦贝尔市', '021000');
INSERT INTO `t_city` VALUES ('32', '15', '巴彦淖尔市', '014400');
INSERT INTO `t_city` VALUES ('33', '15', '乌兰察布市', '011800');
INSERT INTO `t_city` VALUES ('34', '15', '兴安盟', '137500');
INSERT INTO `t_city` VALUES ('35', '15', '锡林郭勒盟', '011100');
INSERT INTO `t_city` VALUES ('36', '15', '阿拉善盟', '016000');
INSERT INTO `t_city` VALUES ('37', '21', '沈阳市', '110000');
INSERT INTO `t_city` VALUES ('38', '21', '大连市', '116000');
INSERT INTO `t_city` VALUES ('39', '21', '鞍山市', '114000');
INSERT INTO `t_city` VALUES ('40', '21', '抚顺市', '113000');
INSERT INTO `t_city` VALUES ('41', '21', '本溪市', '117000');
INSERT INTO `t_city` VALUES ('42', '21', '丹东市', '118000');
INSERT INTO `t_city` VALUES ('43', '21', '锦州市', '121000');
INSERT INTO `t_city` VALUES ('44', '21', '营口市', '115000');
INSERT INTO `t_city` VALUES ('45', '21', '阜新市', '123000');
INSERT INTO `t_city` VALUES ('46', '21', '辽阳市', '111000');
INSERT INTO `t_city` VALUES ('47', '21', '盘锦市', '124000');
INSERT INTO `t_city` VALUES ('48', '21', '铁岭市', '112000');
INSERT INTO `t_city` VALUES ('49', '21', '朝阳市', '122000');
INSERT INTO `t_city` VALUES ('50', '21', '葫芦岛市', '125000');
INSERT INTO `t_city` VALUES ('51', '22', '长春市', '130000');
INSERT INTO `t_city` VALUES ('52', '22', '吉林市', '132000');
INSERT INTO `t_city` VALUES ('53', '22', '四平市', '136000');
INSERT INTO `t_city` VALUES ('54', '22', '辽源市', '136200');
INSERT INTO `t_city` VALUES ('55', '22', '通化市', '134000');
INSERT INTO `t_city` VALUES ('56', '22', '白山市', '134300');
INSERT INTO `t_city` VALUES ('57', '22', '松原市', '131100');
INSERT INTO `t_city` VALUES ('58', '22', '白城市', '137000');
INSERT INTO `t_city` VALUES ('59', '22', '延边朝鲜族自治州', '133000');
INSERT INTO `t_city` VALUES ('60', '23', '哈尔滨市', '150000');
INSERT INTO `t_city` VALUES ('61', '23', '齐齐哈尔市', '161000');
INSERT INTO `t_city` VALUES ('62', '23', '鸡西市', '158100');
INSERT INTO `t_city` VALUES ('63', '23', '鹤岗市', '154100');
INSERT INTO `t_city` VALUES ('64', '23', '双鸭山市', '155100');
INSERT INTO `t_city` VALUES ('65', '23', '大庆市', '163000');
INSERT INTO `t_city` VALUES ('66', '23', '伊春市', '152300');
INSERT INTO `t_city` VALUES ('67', '23', '佳木斯市', '154000');
INSERT INTO `t_city` VALUES ('68', '23', '七台河市', '154600');
INSERT INTO `t_city` VALUES ('69', '23', '牡丹江市', '157000');
INSERT INTO `t_city` VALUES ('70', '23', '黑河市', '164300');
INSERT INTO `t_city` VALUES ('71', '23', '绥化市', '152000');
INSERT INTO `t_city` VALUES ('72', '23', '大兴安岭地区', '165000');
INSERT INTO `t_city` VALUES ('73', '31', '上海市', '200000');
INSERT INTO `t_city` VALUES ('74', '32', '南京市', '210000');
INSERT INTO `t_city` VALUES ('75', '32', '无锡市', '214000');
INSERT INTO `t_city` VALUES ('76', '32', '徐州市', '221000');
INSERT INTO `t_city` VALUES ('77', '32', '常州市', '213000');
INSERT INTO `t_city` VALUES ('78', '32', '苏州市', '215000');
INSERT INTO `t_city` VALUES ('79', '32', '南通市', '226000');
INSERT INTO `t_city` VALUES ('80', '32', '连云港市', '222000');
INSERT INTO `t_city` VALUES ('81', '32', '淮安市', '223200');
INSERT INTO `t_city` VALUES ('82', '32', '盐城市', '224000');
INSERT INTO `t_city` VALUES ('83', '32', '扬州市', '225000');
INSERT INTO `t_city` VALUES ('84', '32', '镇江市', '212000');
INSERT INTO `t_city` VALUES ('85', '32', '泰州市', '225300');
INSERT INTO `t_city` VALUES ('86', '32', '宿迁市', '223800');
INSERT INTO `t_city` VALUES ('87', '33', '杭州市', '310000');
INSERT INTO `t_city` VALUES ('88', '33', '宁波市', '315000');
INSERT INTO `t_city` VALUES ('89', '33', '温州市', '325000');
INSERT INTO `t_city` VALUES ('90', '33', '嘉兴市', '314000');
INSERT INTO `t_city` VALUES ('91', '33', '湖州市', '313000');
INSERT INTO `t_city` VALUES ('92', '33', '绍兴市', '312000');
INSERT INTO `t_city` VALUES ('93', '33', '金华市', '321000');
INSERT INTO `t_city` VALUES ('94', '33', '衢州市', '324000');
INSERT INTO `t_city` VALUES ('95', '33', '舟山市', '316000');
INSERT INTO `t_city` VALUES ('96', '33', '台州市', '318000');
INSERT INTO `t_city` VALUES ('97', '33', '丽水市', '323000');
INSERT INTO `t_city` VALUES ('98', '34', '合肥市', '230000');
INSERT INTO `t_city` VALUES ('99', '34', '芜湖市', '241000');
INSERT INTO `t_city` VALUES ('100', '34', '蚌埠市', '233000');
INSERT INTO `t_city` VALUES ('101', '34', '淮南市', '232000');
INSERT INTO `t_city` VALUES ('102', '34', '马鞍山市', '243000');
INSERT INTO `t_city` VALUES ('103', '34', '淮北市', '235000');
INSERT INTO `t_city` VALUES ('104', '34', '铜陵市', '244000');
INSERT INTO `t_city` VALUES ('105', '34', '安庆市', '246000');
INSERT INTO `t_city` VALUES ('106', '34', '黄山市', '242700');
INSERT INTO `t_city` VALUES ('107', '34', '滁州市', '239000');
INSERT INTO `t_city` VALUES ('108', '34', '阜阳市', '236100');
INSERT INTO `t_city` VALUES ('109', '34', '宿州市', '234100');
INSERT INTO `t_city` VALUES ('110', '34', '巢湖市', '238000');
INSERT INTO `t_city` VALUES ('111', '34', '六安市', '237000');
INSERT INTO `t_city` VALUES ('112', '34', '亳州市', '236800');
INSERT INTO `t_city` VALUES ('113', '34', '池州市', '247100');
INSERT INTO `t_city` VALUES ('114', '34', '宣城市', '366000');
INSERT INTO `t_city` VALUES ('115', '35', '福州市', '350000');
INSERT INTO `t_city` VALUES ('116', '35', '厦门市', '361000');
INSERT INTO `t_city` VALUES ('117', '35', '莆田市', '351100');
INSERT INTO `t_city` VALUES ('118', '35', '三明市', '365000');
INSERT INTO `t_city` VALUES ('119', '35', '泉州市', '362000');
INSERT INTO `t_city` VALUES ('120', '35', '漳州市', '363000');
INSERT INTO `t_city` VALUES ('121', '35', '南平市', '353000');
INSERT INTO `t_city` VALUES ('122', '35', '龙岩市', '364000');
INSERT INTO `t_city` VALUES ('123', '35', '宁德市', '352100');
INSERT INTO `t_city` VALUES ('124', '36', '南昌市', '330000');
INSERT INTO `t_city` VALUES ('125', '36', '景德镇市', '333000');
INSERT INTO `t_city` VALUES ('126', '36', '萍乡市', '337000');
INSERT INTO `t_city` VALUES ('127', '36', '九江市', '332000');
INSERT INTO `t_city` VALUES ('128', '36', '新余市', '338000');
INSERT INTO `t_city` VALUES ('129', '36', '鹰潭市', '335000');
INSERT INTO `t_city` VALUES ('130', '36', '赣州市', '341000');
INSERT INTO `t_city` VALUES ('131', '36', '吉安市', '343000');
INSERT INTO `t_city` VALUES ('132', '36', '宜春市', '336000');
INSERT INTO `t_city` VALUES ('133', '36', '抚州市', '332900');
INSERT INTO `t_city` VALUES ('134', '36', '上饶市', '334000');
INSERT INTO `t_city` VALUES ('135', '37', '济南市', '250000');
INSERT INTO `t_city` VALUES ('136', '37', '青岛市', '266000');
INSERT INTO `t_city` VALUES ('137', '37', '淄博市', '255000');
INSERT INTO `t_city` VALUES ('138', '37', '枣庄市', '277100');
INSERT INTO `t_city` VALUES ('139', '37', '东营市', '257000');
INSERT INTO `t_city` VALUES ('140', '37', '烟台市', '264000');
INSERT INTO `t_city` VALUES ('141', '37', '潍坊市', '261000');
INSERT INTO `t_city` VALUES ('142', '37', '济宁市', '272100');
INSERT INTO `t_city` VALUES ('143', '37', '泰安市', '271000');
INSERT INTO `t_city` VALUES ('144', '37', '威海市', '265700');
INSERT INTO `t_city` VALUES ('145', '37', '日照市', '276800');
INSERT INTO `t_city` VALUES ('146', '37', '莱芜市', '271100');
INSERT INTO `t_city` VALUES ('147', '37', '临沂市', '276000');
INSERT INTO `t_city` VALUES ('148', '37', '德州市', '253000');
INSERT INTO `t_city` VALUES ('149', '37', '聊城市', '252000');
INSERT INTO `t_city` VALUES ('150', '37', '滨州市', '256600');
INSERT INTO `t_city` VALUES ('151', '37', '荷泽市', '255000');
INSERT INTO `t_city` VALUES ('152', '41', '郑州市', '450000');
INSERT INTO `t_city` VALUES ('153', '41', '开封市', '475000');
INSERT INTO `t_city` VALUES ('154', '41', '洛阳市', '471000');
INSERT INTO `t_city` VALUES ('155', '41', '平顶山市', '467000');
INSERT INTO `t_city` VALUES ('156', '41', '安阳市', '454900');
INSERT INTO `t_city` VALUES ('157', '41', '鹤壁市', '456600');
INSERT INTO `t_city` VALUES ('158', '41', '新乡市', '453000');
INSERT INTO `t_city` VALUES ('159', '41', '焦作市', '454100');
INSERT INTO `t_city` VALUES ('160', '41', '濮阳市', '457000');
INSERT INTO `t_city` VALUES ('161', '41', '许昌市', '461000');
INSERT INTO `t_city` VALUES ('162', '41', '漯河市', '462000');
INSERT INTO `t_city` VALUES ('163', '41', '三门峡市', '472000');
INSERT INTO `t_city` VALUES ('164', '41', '南阳市', '473000');
INSERT INTO `t_city` VALUES ('165', '41', '商丘市', '476000');
INSERT INTO `t_city` VALUES ('166', '41', '信阳市', '464000');
INSERT INTO `t_city` VALUES ('167', '41', '周口市', '466000');
INSERT INTO `t_city` VALUES ('168', '41', '驻马店市', '463000');
INSERT INTO `t_city` VALUES ('169', '42', '武汉市', '430000');
INSERT INTO `t_city` VALUES ('170', '42', '黄石市', '435000');
INSERT INTO `t_city` VALUES ('171', '42', '十堰市', '442000');
INSERT INTO `t_city` VALUES ('172', '42', '宜昌市', '443000');
INSERT INTO `t_city` VALUES ('173', '42', '襄樊市', '441000');
INSERT INTO `t_city` VALUES ('174', '42', '鄂州市', '436000');
INSERT INTO `t_city` VALUES ('175', '42', '荆门市', '448000');
INSERT INTO `t_city` VALUES ('176', '42', '孝感市', '432100');
INSERT INTO `t_city` VALUES ('177', '42', '荆州市', '434000');
INSERT INTO `t_city` VALUES ('178', '42', '黄冈市', '438000');
INSERT INTO `t_city` VALUES ('179', '42', '咸宁市', '437000');
INSERT INTO `t_city` VALUES ('180', '42', '随州市', '441300');
INSERT INTO `t_city` VALUES ('181', '42', '恩施土家族苗族自治州', '445000');
INSERT INTO `t_city` VALUES ('182', '42', '神农架', '442400');
INSERT INTO `t_city` VALUES ('183', '43', '长沙市', '410000');
INSERT INTO `t_city` VALUES ('184', '43', '株洲市', '412000');
INSERT INTO `t_city` VALUES ('185', '43', '湘潭市', '411100');
INSERT INTO `t_city` VALUES ('186', '43', '衡阳市', '421000');
INSERT INTO `t_city` VALUES ('187', '43', '邵阳市', '422000');
INSERT INTO `t_city` VALUES ('188', '43', '岳阳市', '414000');
INSERT INTO `t_city` VALUES ('189', '43', '常德市', '415000');
INSERT INTO `t_city` VALUES ('190', '43', '张家界市', '427000');
INSERT INTO `t_city` VALUES ('191', '43', '益阳市', '413000');
INSERT INTO `t_city` VALUES ('192', '43', '郴州市', '423000');
INSERT INTO `t_city` VALUES ('193', '43', '永州市', '425000');
INSERT INTO `t_city` VALUES ('194', '43', '怀化市', '418000');
INSERT INTO `t_city` VALUES ('195', '43', '娄底市', '417000');
INSERT INTO `t_city` VALUES ('196', '43', '湘西土家族苗族自治州', '416000');
INSERT INTO `t_city` VALUES ('197', '44', '广州市', '510000');
INSERT INTO `t_city` VALUES ('198', '44', '韶关市', '521000');
INSERT INTO `t_city` VALUES ('199', '44', '深圳市', '518000');
INSERT INTO `t_city` VALUES ('200', '44', '珠海市', '519000');
INSERT INTO `t_city` VALUES ('201', '44', '汕头市', '515000');
INSERT INTO `t_city` VALUES ('202', '44', '佛山市', '528000');
INSERT INTO `t_city` VALUES ('203', '44', '江门市', '529000');
INSERT INTO `t_city` VALUES ('204', '44', '湛江市', '524000');
INSERT INTO `t_city` VALUES ('205', '44', '茂名市', '525000');
INSERT INTO `t_city` VALUES ('206', '44', '肇庆市', '526000');
INSERT INTO `t_city` VALUES ('207', '44', '惠州市', '516000');
INSERT INTO `t_city` VALUES ('208', '44', '梅州市', '514000');
INSERT INTO `t_city` VALUES ('209', '44', '汕尾市', '516600');
INSERT INTO `t_city` VALUES ('210', '44', '河源市', '517000');
INSERT INTO `t_city` VALUES ('211', '44', '阳江市', '529500');
INSERT INTO `t_city` VALUES ('212', '44', '清远市', '511500');
INSERT INTO `t_city` VALUES ('213', '44', '东莞市', '511700');
INSERT INTO `t_city` VALUES ('214', '44', '中山市', '528400');
INSERT INTO `t_city` VALUES ('215', '44', '潮州市', '515600');
INSERT INTO `t_city` VALUES ('216', '44', '揭阳市', '522000');
INSERT INTO `t_city` VALUES ('217', '44', '云浮市', '527300');
INSERT INTO `t_city` VALUES ('218', '45', '南宁市', '530000');
INSERT INTO `t_city` VALUES ('219', '45', '柳州市', '545000');
INSERT INTO `t_city` VALUES ('220', '45', '桂林市', '541000');
INSERT INTO `t_city` VALUES ('221', '45', '梧州市', '543000');
INSERT INTO `t_city` VALUES ('222', '45', '北海市', '536000');
INSERT INTO `t_city` VALUES ('223', '45', '防城港市', '538000');
INSERT INTO `t_city` VALUES ('224', '45', '钦州市', '535000');
INSERT INTO `t_city` VALUES ('225', '45', '贵港市', '537100');
INSERT INTO `t_city` VALUES ('226', '45', '玉林市', '537000');
INSERT INTO `t_city` VALUES ('227', '45', '百色市', '533000');
INSERT INTO `t_city` VALUES ('228', '45', '贺州市', '542800');
INSERT INTO `t_city` VALUES ('229', '45', '河池市', '547000');
INSERT INTO `t_city` VALUES ('230', '45', '来宾市', '546100');
INSERT INTO `t_city` VALUES ('231', '45', '崇左市', '532200');
INSERT INTO `t_city` VALUES ('232', '46', '海口市', '570000');
INSERT INTO `t_city` VALUES ('233', '46', '三亚市', '572000');
INSERT INTO `t_city` VALUES ('234', '55', '重庆市', '400000');
INSERT INTO `t_city` VALUES ('235', '51', '成都市', '610000');
INSERT INTO `t_city` VALUES ('236', '51', '自贡市', '643000');
INSERT INTO `t_city` VALUES ('237', '51', '攀枝花市', '617000');
INSERT INTO `t_city` VALUES ('238', '51', '泸州市', '646100');
INSERT INTO `t_city` VALUES ('239', '51', '德阳市', '618000');
INSERT INTO `t_city` VALUES ('240', '51', '绵阳市', '621000');
INSERT INTO `t_city` VALUES ('241', '51', '广元市', '628000');
INSERT INTO `t_city` VALUES ('242', '51', '遂宁市', '629000');
INSERT INTO `t_city` VALUES ('243', '51', '内江市', '641000');
INSERT INTO `t_city` VALUES ('244', '51', '乐山市', '614000');
INSERT INTO `t_city` VALUES ('245', '51', '南充市', '637000');
INSERT INTO `t_city` VALUES ('246', '51', '眉山市', '612100');
INSERT INTO `t_city` VALUES ('247', '51', '宜宾市', '644000');
INSERT INTO `t_city` VALUES ('248', '51', '广安市', '638000');
INSERT INTO `t_city` VALUES ('249', '51', '达州市', '635000');
INSERT INTO `t_city` VALUES ('250', '51', '雅安市', '625000');
INSERT INTO `t_city` VALUES ('251', '51', '巴中市', '635500');
INSERT INTO `t_city` VALUES ('252', '51', '资阳市', '641300');
INSERT INTO `t_city` VALUES ('253', '51', '阿坝藏族羌族自治州', '624600');
INSERT INTO `t_city` VALUES ('254', '51', '甘孜藏族自治州', '626000');
INSERT INTO `t_city` VALUES ('255', '51', '凉山彝族自治州', '615000');
INSERT INTO `t_city` VALUES ('256', '52', '贵阳市', '55000');
INSERT INTO `t_city` VALUES ('257', '52', '六盘水市', '553000');
INSERT INTO `t_city` VALUES ('258', '52', '遵义市', '563000');
INSERT INTO `t_city` VALUES ('259', '52', '安顺市', '561000');
INSERT INTO `t_city` VALUES ('260', '52', '铜仁地区', '554300');
INSERT INTO `t_city` VALUES ('261', '52', '黔西南布依族苗族自治州', '551500');
INSERT INTO `t_city` VALUES ('262', '52', '毕节地区', '551700');
INSERT INTO `t_city` VALUES ('263', '52', '黔东南苗族侗族自治州', '551500');
INSERT INTO `t_city` VALUES ('264', '52', '黔南布依族苗族自治州', '550100');
INSERT INTO `t_city` VALUES ('265', '53', '昆明市', '650000');
INSERT INTO `t_city` VALUES ('266', '53', '曲靖市', '655000');
INSERT INTO `t_city` VALUES ('267', '53', '玉溪市', '653100');
INSERT INTO `t_city` VALUES ('268', '53', '保山市', '678000');
INSERT INTO `t_city` VALUES ('269', '53', '昭通市', '657000');
INSERT INTO `t_city` VALUES ('270', '53', '丽江市', '674100');
INSERT INTO `t_city` VALUES ('271', '53', '思茅市', '665000');
INSERT INTO `t_city` VALUES ('272', '53', '临沧市', '677000');
INSERT INTO `t_city` VALUES ('273', '53', '楚雄彝族自治州', '675000');
INSERT INTO `t_city` VALUES ('274', '53', '红河哈尼族彝族自治州', '654400');
INSERT INTO `t_city` VALUES ('275', '53', '文山壮族苗族自治州', '663000');
INSERT INTO `t_city` VALUES ('276', '53', '西双版纳傣族自治州', '666200');
INSERT INTO `t_city` VALUES ('277', '53', '大理白族自治州', '671000');
INSERT INTO `t_city` VALUES ('278', '53', '德宏傣族景颇族自治州', '678400');
INSERT INTO `t_city` VALUES ('279', '53', '怒江傈僳族自治州', '671400');
INSERT INTO `t_city` VALUES ('280', '53', '迪庆藏族自治州', '674400');
INSERT INTO `t_city` VALUES ('281', '54', '拉萨市', '850000');
INSERT INTO `t_city` VALUES ('282', '54', '昌都地区', '854000');
INSERT INTO `t_city` VALUES ('283', '54', '山南地区', '856000');
INSERT INTO `t_city` VALUES ('284', '54', '日喀则地区', '857000');
INSERT INTO `t_city` VALUES ('285', '54', '那曲地区', '852000');
INSERT INTO `t_city` VALUES ('286', '54', '阿里地区', '859100');
INSERT INTO `t_city` VALUES ('287', '54', '林芝地区', '860100');
INSERT INTO `t_city` VALUES ('288', '61', '西安市', '710000');
INSERT INTO `t_city` VALUES ('289', '61', '铜川市', '727000');
INSERT INTO `t_city` VALUES ('290', '61', '宝鸡市', '721000');
INSERT INTO `t_city` VALUES ('291', '61', '咸阳市', '712000');
INSERT INTO `t_city` VALUES ('292', '61', '渭南市', '714000');
INSERT INTO `t_city` VALUES ('293', '61', '延安市', '716000');
INSERT INTO `t_city` VALUES ('294', '61', '汉中市', '723000');
INSERT INTO `t_city` VALUES ('295', '61', '榆林市', '719000');
INSERT INTO `t_city` VALUES ('296', '61', '安康市', '725000');
INSERT INTO `t_city` VALUES ('297', '61', '商洛市', '711500');
INSERT INTO `t_city` VALUES ('298', '56', '兰州市', '730000');
INSERT INTO `t_city` VALUES ('299', '62', '嘉峪关市', '735100');
INSERT INTO `t_city` VALUES ('300', '62', '金昌市', '737100');
INSERT INTO `t_city` VALUES ('301', '62', '白银市', '730900');
INSERT INTO `t_city` VALUES ('302', '62', '天水市', '741000');
INSERT INTO `t_city` VALUES ('303', '62', '武威市', '733000');
INSERT INTO `t_city` VALUES ('304', '62', '张掖市', '734000');
INSERT INTO `t_city` VALUES ('305', '62', '平凉市', '744000');
INSERT INTO `t_city` VALUES ('306', '62', '酒泉市', '735000');
INSERT INTO `t_city` VALUES ('307', '62', '庆阳市', '744500');
INSERT INTO `t_city` VALUES ('308', '62', '定西市', '743000');
INSERT INTO `t_city` VALUES ('309', '62', '陇南市', '742100');
INSERT INTO `t_city` VALUES ('310', '62', '临夏回族自治州', '731100');
INSERT INTO `t_city` VALUES ('311', '62', '甘南藏族自治州', '747000');
INSERT INTO `t_city` VALUES ('312', '63', '西宁市', '810000');
INSERT INTO `t_city` VALUES ('313', '63', '海东地区', '810600');
INSERT INTO `t_city` VALUES ('314', '63', '海北藏族自治州', '810300');
INSERT INTO `t_city` VALUES ('315', '63', '黄南藏族自治州', '811300');
INSERT INTO `t_city` VALUES ('316', '63', '海南藏族自治州', '813000');
INSERT INTO `t_city` VALUES ('317', '63', '果洛藏族自治州', '814000');
INSERT INTO `t_city` VALUES ('318', '63', '玉树藏族自治州', '815000');
INSERT INTO `t_city` VALUES ('319', '63', '海西蒙古族藏族自治州', '817000');
INSERT INTO `t_city` VALUES ('320', '64', '银川市', '750000');
INSERT INTO `t_city` VALUES ('321', '64', '石嘴山市', '753000');
INSERT INTO `t_city` VALUES ('322', '64', '吴忠市', '751100');
INSERT INTO `t_city` VALUES ('323', '64', '固原市', '756000');
INSERT INTO `t_city` VALUES ('324', '64', '中卫市', '751700');
INSERT INTO `t_city` VALUES ('325', '65', '乌鲁木齐市', '830000');
INSERT INTO `t_city` VALUES ('326', '65', '克拉玛依市', '834000');
INSERT INTO `t_city` VALUES ('327', '65', '吐鲁番地区', '838000');
INSERT INTO `t_city` VALUES ('328', '65', '哈密地区', '839000');
INSERT INTO `t_city` VALUES ('329', '65', '昌吉回族自治州', '831100');
INSERT INTO `t_city` VALUES ('330', '65', '博尔塔拉蒙古自治州', '833400');
INSERT INTO `t_city` VALUES ('331', '65', '巴音郭楞蒙古自治州', '841000');
INSERT INTO `t_city` VALUES ('332', '65', '阿克苏地区', '843000');
INSERT INTO `t_city` VALUES ('333', '65', '克孜勒苏柯尔克孜自治州', '835600');
INSERT INTO `t_city` VALUES ('334', '65', '喀什地区', '844000');
INSERT INTO `t_city` VALUES ('335', '65', '和田地区', '848000');
INSERT INTO `t_city` VALUES ('336', '65', '伊犁哈萨克自治州', '833200');
INSERT INTO `t_city` VALUES ('337', '65', '塔城地区', '834700');
INSERT INTO `t_city` VALUES ('338', '65', '阿勒泰地区', '836500');
INSERT INTO `t_city` VALUES ('339', '65', '石河子市', '832000');
INSERT INTO `t_city` VALUES ('340', '65', '阿拉尔市', '843300');
INSERT INTO `t_city` VALUES ('341', '65', '图木舒克市', '843900');
INSERT INTO `t_city` VALUES ('342', '65', '五家渠市', '831300');
INSERT INTO `t_city` VALUES ('343', '81', '香港特别行政区', '000000');
INSERT INTO `t_city` VALUES ('344', '82', '澳门特别行政区', '000000');
INSERT INTO `t_city` VALUES ('345', '71', '台湾省', '000000');

-- ----------------------------
-- Table structure for `t_config`
-- ----------------------------
DROP TABLE IF EXISTS `t_config`;
CREATE TABLE `t_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `config_key` varchar(255) NOT NULL COMMENT '配置参数Key',
  `config_value` varchar(2000) NOT NULL COMMENT '配置参数值',
  `config_desc` varchar(255) DEFAULT NULL COMMENT '配置参数描述',
  `module` varchar(255) DEFAULT NULL COMMENT '模块',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_config
-- ----------------------------
INSERT INTO `t_config` VALUES ('1', 'websiteDesc', '业务运营平台', '业务运营平台', '');
INSERT INTO `t_config` VALUES ('2', 'websiteKey', 'oms', '网站关键字', '');
INSERT INTO `t_config` VALUES ('3', 'websiteLogo', 'logo1.png', '网站LOGO', '');
INSERT INTO `t_config` VALUES ('4', 'websiteName', '24kpay运营管理系统', '网站标题', '');
INSERT INTO `t_config` VALUES ('5', 'websitePublish', ' ', '网站版权', '');
INSERT INTO `t_config` VALUES ('6', 'websiteStyle', 'default', '网站风格', '');
INSERT INTO `t_config` VALUES ('7', 'websiteTitle', '运营管理系统', '网站首页简称', '');

-- ----------------------------
-- Table structure for `t_dept`
-- ----------------------------
DROP TABLE IF EXISTS `t_dept`;
CREATE TABLE `t_dept` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门编号',
  `name` varchar(255) DEFAULT '' COMMENT '部门名称',
  `dept_desc` varchar(255) DEFAULT '' COMMENT '部门描述',
  `parent_id` int(11) DEFAULT NULL COMMENT '父部门编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_dept
-- ----------------------------
INSERT INTO `t_dept` VALUES ('1', '总公司', '总公司', null);

-- ----------------------------
-- Table structure for `t_loginlog`
-- ----------------------------
DROP TABLE IF EXISTS `t_loginlog`;
CREATE TABLE `t_loginlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `url` varchar(100) DEFAULT NULL COMMENT 'url',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '时间',
  `ip` varchar(50) DEFAULT NULL COMMENT 'IP',
  `user_agent` varchar(300) DEFAULT NULL COMMENT '设备',
  `status` int(1) DEFAULT '1' COMMENT '状态',
  `msg` varchar(50) DEFAULT NULL COMMENT '消息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=519 DEFAULT CHARSET=utf8 COMMENT='登录日志';

-- ----------------------------
-- Records of t_loginlog
-- ----------------------------

-- ----------------------------
-- Table structure for `t_menu`
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `parent_id` int(20) DEFAULT NULL COMMENT '父节点',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `is_menu` int(1) DEFAULT '0' COMMENT '菜单类型',
  `menu_type` int(1) DEFAULT '0' COMMENT '加载方式',
  `sort_number` int(4) DEFAULT '0' COMMENT '排序',
  `url` varchar(5000) DEFAULT NULL COMMENT '地址',
  `button` varchar(500) DEFAULT NULL COMMENT '按钮',
  `status` int(2) DEFAULT '0' COMMENT '状态',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=utf8 COMMENT='菜单导航';

-- ----------------------------
-- Records of t_menu
-- ----------------------------
INSERT INTO `t_menu` VALUES ('1', null, '系统管理', '0', '0', '100', null, null, '0', '2013-08-01 10:39:54');
INSERT INTO `t_menu` VALUES ('2', null, '监控管理', '0', '0', '12', null, null, '0', '2013-08-01 10:39:54');
INSERT INTO `t_menu` VALUES ('100', '1', '角色管理', '0', '0', '0', 'admin/Role/index.htm', null, '0', '2013-08-01 10:39:59');
INSERT INTO `t_menu` VALUES ('101', '1', '菜单管理', '0', '0', '0', 'admin/Menu/index.htm', null, '0', '2013-08-01 10:39:59');
INSERT INTO `t_menu` VALUES ('103', '1', '用户管理', '0', '0', '0', 'admin/User/index.htm', null, '0', '2013-08-01 10:40:00');
INSERT INTO `t_menu` VALUES ('104', '1', '部门管理', '0', '0', '0', 'admin/Dept/index.htm', null, '0', '2013-09-19 20:02:38');
INSERT INTO `t_menu` VALUES ('106', '1', '系统配置', '0', '0', '0', 'admin/Config/index.htm', '', '0', '2014-07-26 09:15:29');
INSERT INTO `t_menu` VALUES ('108', '2', '登录日志', '0', '0', '0', 'admin/LoginLog/index.htm', null, '0', '2013-08-01 10:39:59');
INSERT INTO `t_menu` VALUES ('111', '103', '添加', '1', '0', '1', 'admin/User/add.htm,admin/User/baseDlg.htm', '<a href=\"javascript:void(0)\" onclick=\"javascript:tbsUserGridAddAndEdit(\'添加 \',\'admin/TbsUser/add.html\',0)\" class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:\'icon-add\'\">添加</a>', '0', '2013-08-01 10:40:00');
INSERT INTO `t_menu` VALUES ('112', '103', '编辑', '1', '0', '2', 'admin/User/edit.htm,admin/User/baseDlg.htm,admin/User/save.htm', '<a href=\"javascript:void(0)\" onclick=\"javascript:tbsUserGridAddAndEdit(\'修改\',\'admin/TbsUser/save.html\',1)\" class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:\'icon-edit\'\">编辑 </a>', '0', '2013-08-01 10:40:00');
INSERT INTO `t_menu` VALUES ('113', '103', '删除', '1', '0', '3', 'admin/User/del.htm', '<a href=\"javascript:void(0)\" onclick=\"javascript:tbsUserGridDel()\" class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:\'icon-remove\'\">删除</a>', '0', '2013-08-01 10:40:00');
INSERT INTO `t_menu` VALUES ('114', '103', '查看', '1', '0', '0', 'admin/User/data.htm', null, '0', '2014-01-17 14:15:57');
INSERT INTO `t_menu` VALUES ('115', '104', '编辑', '1', '0', '1', 'admin/Dept/edit.htm,admin/Dept/baseDlg.htm,admin/Dept/save.htm', null, '0', '2014-01-17 14:35:31');
INSERT INTO `t_menu` VALUES ('116', '104', '查看', '1', '0', '0', 'admin/Dept/data.htm', null, '0', '2014-01-17 14:15:05');
INSERT INTO `t_menu` VALUES ('117', '104', '添加', '1', '0', '1', 'admin/Dept/add.htm,admin/Dept/baseDlg.htm', null, '0', '2014-01-17 14:35:31');
INSERT INTO `t_menu` VALUES ('118', '100', '添加', '1', '0', '1', 'admin/Role/add.htm,admin/Role/baseDlg.htm', '<a href=\"javascript:void(0)\" onclick=\"javascript:tbsRoleGridAddAndEdit(\'添加 \',\'admin/TbsRole/add.html\',0)\" class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:\'icon-add\'\">添加</a>', '0', '2013-08-01 10:39:59');
INSERT INTO `t_menu` VALUES ('119', '100', '编辑', '1', '0', '1', 'admin/Role/edit.htm,admin/Role/baseDlg.htm,admin/Role/save.htm', '<a href=\"javascript:void(0)\" onclick=\"javascript:tbsRoleGridAddAndEdit(\'添加 \',\'admin/TbsRole/add.html\',0)\" class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:\'icon-add\'\">添加</a>', '0', '2013-08-01 10:39:59');
INSERT INTO `t_menu` VALUES ('120', '100', '删除', '1', '0', '3', 'admin/Role/del.htm', '<a href=\"javascript:void(0)\" onclick=\"javascript:tbsRoleGridDel()\" class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:\'icon-remove\'\">删除</a>', '0', '2013-08-01 10:39:59');
INSERT INTO `t_menu` VALUES ('121', '100', '查看', '1', '0', '0', 'admin/Role/data.htm', null, '0', '2013-08-01 10:39:59');
INSERT INTO `t_menu` VALUES ('122', '101', '查看', '1', '0', '0', 'admin/Menu/data.htm', null, '0', '2013-08-01 10:39:59');
INSERT INTO `t_menu` VALUES ('123', '101', '编辑', '1', '0', '2', 'admin/Menu/edit.htm,admin/Menu/baseDlg.htm,admin/Menu/save.htm', '<a href=\"javascript:void(0)\" onclick=\"javascript:t_menuGridAddAndEdit(\'修改\',\'admin/TbsMenu/save.html\',1)\" class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:\'icon-edit\'\">编辑 </a>', '0', '2013-08-01 10:39:59');
INSERT INTO `t_menu` VALUES ('124', '101', '添加', '1', '0', '1', 'admin/Menu/add.htm,admin/Menu/baseDlg.htm', '<a href=\"javascript:void(0)\" onclick=\"javascript:t_menuGridAddAndEdit(\'添加 \',\'admin/TbsMenu/add.html\',0)\" class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:\'icon-add\'\">添加</a>', '0', '2013-08-01 10:39:59');
INSERT INTO `t_menu` VALUES ('125', '101', '删除', '1', '0', '3', 'admin/Menu/del.htm', '<a href=\"javascript:void(0)\" onclick=\"javascript:tbsMenuGridDel()\" class=\"easyui-linkbutton\" data-options=\"plain:true,iconCls:\'icon-remove\'\">删除</a>', '0', '2013-08-01 10:39:59');
INSERT INTO `t_menu` VALUES ('127', '108', '查看', '1', '0', '0', 'admin/LoginLog/data.htm', null, '0', '2014-01-17 14:15:57');
INSERT INTO `t_menu` VALUES ('132', '108', '查看', '0', '0', '1', 'admin/LoginLog/chartsJson.htm', null, '0', '2014-01-15 18:30:52');
INSERT INTO `t_menu` VALUES ('137', '106', '编辑', '1', '0', '0', 'admin/Config/save.htm,admin/Config/baseDlg.htm', '', '0', '2014-07-26 09:15:29');
INSERT INTO `t_menu` VALUES ('138', '106', '添加', '1', '0', '0', 'admin/Config/add.htm,admin/Config/baseDlg.htm', '', '0', '2014-07-26 09:15:29');
INSERT INTO `t_menu` VALUES ('139', '106', '查看', '1', '0', '0', 'admin/Config/data.htm', '', '0', '2014-07-26 09:15:29');

-- ----------------------------
-- Table structure for `t_province`
-- ----------------------------
DROP TABLE IF EXISTS `t_province`;
CREATE TABLE `t_province` (
  `province_no` int(11) NOT NULL COMMENT '省份编号',
  `province_name` varchar(255) NOT NULL COMMENT '省份名称',
  PRIMARY KEY (`province_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='省份';

-- ----------------------------
-- Records of t_province
-- ----------------------------
INSERT INTO `t_province` VALUES ('11', '北京市');
INSERT INTO `t_province` VALUES ('12', '天津市');
INSERT INTO `t_province` VALUES ('13', '河北省');
INSERT INTO `t_province` VALUES ('14', '山西省');
INSERT INTO `t_province` VALUES ('15', '内蒙古自治区');
INSERT INTO `t_province` VALUES ('21', '辽宁省');
INSERT INTO `t_province` VALUES ('22', '吉林省');
INSERT INTO `t_province` VALUES ('23', '黑龙江省');
INSERT INTO `t_province` VALUES ('31', '上海市');
INSERT INTO `t_province` VALUES ('32', '江苏省');
INSERT INTO `t_province` VALUES ('33', '浙江省');
INSERT INTO `t_province` VALUES ('34', '安徽省');
INSERT INTO `t_province` VALUES ('35', '福建省');
INSERT INTO `t_province` VALUES ('36', '江西省');
INSERT INTO `t_province` VALUES ('37', '山东省');
INSERT INTO `t_province` VALUES ('41', '河南省');
INSERT INTO `t_province` VALUES ('42', '湖北省');
INSERT INTO `t_province` VALUES ('43', '湖南省');
INSERT INTO `t_province` VALUES ('44', '广东省');
INSERT INTO `t_province` VALUES ('45', '广西壮族自治区');
INSERT INTO `t_province` VALUES ('46', '海南省');
INSERT INTO `t_province` VALUES ('51', '四川省');
INSERT INTO `t_province` VALUES ('52', '贵州省');
INSERT INTO `t_province` VALUES ('53', '云南省');
INSERT INTO `t_province` VALUES ('54', '西藏自治区');
INSERT INTO `t_province` VALUES ('55', '重庆市');
INSERT INTO `t_province` VALUES ('61', '陕西省');
INSERT INTO `t_province` VALUES ('62', '甘肃省');
INSERT INTO `t_province` VALUES ('63', '青海省');
INSERT INTO `t_province` VALUES ('64', '宁夏回族自治区');
INSERT INTO `t_province` VALUES ('65', '新疆维吾尔自治区');
INSERT INTO `t_province` VALUES ('71', '台湾省');
INSERT INTO `t_province` VALUES ('81', '澳门特别行政区');
INSERT INTO `t_province` VALUES ('82', '香港特别行政区');

-- ----------------------------
-- Table structure for `t_role`
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(20) DEFAULT NULL COMMENT '角色',
  `text` varchar(500) DEFAULT NULL COMMENT '所有权限',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='权限角色';

-- ----------------------------
-- Records of t_role
-- ----------------------------

-- ----------------------------
-- Table structure for `t_rolemenu`
-- ----------------------------
DROP TABLE IF EXISTS `t_rolemenu`;
CREATE TABLE `t_rolemenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` int(11) DEFAULT NULL COMMENT '角色主键',
  `menu_idfun` int(11) DEFAULT NULL COMMENT '功能主键',
  `menu_id` int(11) DEFAULT NULL COMMENT '菜单主键',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='角色与资源关联表';

-- ----------------------------
-- Records of t_rolemenu
-- ----------------------------

-- ----------------------------
-- Table structure for `t_roleuser`
-- ----------------------------
DROP TABLE IF EXISTS `t_roleuser`;
CREATE TABLE `t_roleuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) DEFAULT NULL COMMENT '用户主键',
  `role_id` int(11) DEFAULT NULL COMMENT '角色主键',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户与角色关联表';

-- ----------------------------
-- Records of t_roleuser
-- ----------------------------

-- ----------------------------
-- Table structure for `t_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_name` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `real_name` varchar(65) DEFAULT NULL COMMENT '姓名',
  `sex` char(1) DEFAULT NULL COMMENT '性别',
  `mobile_no` varchar(18) DEFAULT NULL COMMENT '手机',
  `telephone` varchar(18) DEFAULT NULL COMMENT '座机',
  `mail` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `work_place` varchar(30) DEFAULT NULL COMMENT '工作地点',
  `postid` varchar(50) DEFAULT NULL COMMENT '职位',
  `deptid` varchar(50) DEFAULT NULL COMMENT '部门',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '入职时间',
  `ip` varchar(50) DEFAULT NULL COMMENT 'ip',
  `count` int(2) DEFAULT '0' COMMENT '次数',
  `isLock` int(1) DEFAULT '0' COMMENT '锁定',
  `lock_time` timestamp NULL DEFAULT NULL COMMENT '锁定时间',
  `fail_count` int(1) DEFAULT '0' COMMENT '失败次数',
  `state` int(1) DEFAULT NULL COMMENT '员工状态',
  `is_admin` int(1) DEFAULT '1' COMMENT '权限类型',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_user_username` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=20002 DEFAULT CHARSET=utf8 COMMENT='系统用户';

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('20000', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '超级管理员', '1', '13912345678', '02512345678', 'hao@126.com', '江苏省南京市', '', '', '2013-08-01 10:39:55', '127.0.0.1', '0', '0', '2017-01-04 07:44:44', '1', '1', '0');
