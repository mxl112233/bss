package org.boss.tools;

import com.mysql.jdbc.StringUtils;
import org.apache.commons.io.FileUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 代码生成工具类-----根据创建表的语句生成对应的增删改查（多表关联，后续增加生成规则）
 * 
 * @author xianglin.mo
 * 
 */
public class VelocityUtil {

	private static final Map<String, String> SQLTYPE = new HashMap<String, String>();

	static {
		SQLTYPE.put("date", "java.sql.Date,2");
		SQLTYPE.put("timestamp", "java.sql.Timestamp,2");
		SQLTYPE.put("time", "java.sql.Time,2");
		SQLTYPE.put("char", "String,0");
		SQLTYPE.put("varchar", "String,0");
		SQLTYPE.put("bigint", "Long,1");
		SQLTYPE.put("int", "Integer,1");
		SQLTYPE.put("tinyint", "Integer,1");
		SQLTYPE.put("long", "Long,1");
		SQLTYPE.put("number", "java.math.BigDecimal,1");
		SQLTYPE.put("decimal", "java.math.BigDecimal,1");
		SQLTYPE.put("text", "String,0");
	}

	public static void exportFixedVelocity(Param param, String tableComent, String role,
			List<Map<String, Object>> clumListMap, Integer menuId) throws Exception {

		// 设置初始化数据
		VelocityContext context = new VelocityContext();
		setValue(clumListMap, context, param.getTableName(), tableComent);
		context.put("DATE", new Date());
		context.put("ROLE", role);
		context.put("batchInsert", param.isBatchInsert() ? 1 : 0);
		context.put("queryByMap", param.isQueryByMap() ? 1 : 0);
		context.put("PAGE", "#include");
		String packageName = "org.offjob.boss.domain." + param.getModuleName();
		context.put("PACKAGEPATHROOT", packageName);
		context.put("MODULENAME", param.getModuleName());

		//前端代码生成-----------------------------------------------------------------------------------------start
		String frontPath = param.getProjectPath() +"\\"+param.getModuleName()+"-"+param.getUiName()+ "\\src\\main\\";
		String htmlPath = "";
		if ("admin".equalsIgnoreCase(role)) {
			htmlPath = frontPath + "webapp\\ftl\\view\\admin\\"+param.getModuleName()+"\\";
		} else {
			htmlPath = frontPath + "webapp\\ftl\\view\\common\\"+param.getModuleName()+"\\";
		}

		String frontTemplatePath = VelocityUtil.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		frontTemplatePath = frontTemplatePath.replace("/target/classes/", "/template/"+param.getUiName()+"/");
		VelocityEngine frontVelocityEngine = new VelocityEngine();
		Properties frontProperties = new Properties();
		frontProperties.setProperty(Velocity.FILE_RESOURCE_LOADER_PATH, frontTemplatePath);
		frontVelocityEngine.init(frontProperties);
		File frontDir = new File(frontTemplatePath);
		File[] frontFiles = frontDir.listFiles();


		if (frontFiles.length > 0) {
			String filePath = "";
			for (int i = 0; i < frontFiles.length; i++) {
				if (frontFiles[i].isDirectory()) {
					continue;
				}
				String FileName = frontFiles[i].getName();
				if (FileName.endsWith(".html")) {
					filePath = htmlPath + context.get("TABLENAME").toString();
				} else {
					System.err.println("File not support front file:" + FileName);
					continue;
				}
				filePath = filePath.replace(".", "\\");
				System.err.println(filePath);
				Template t = frontVelocityEngine.getTemplate(frontFiles[i].getName(), "UTF-8");
				buildTemplate(context, filePath, FileName, t);
			}
		}
		//前端代码生成---------------------------------------------------------------------end
		//后端代码生成---------------------------------------------------------------------start
		String backEndPath = param.getProjectPath() +"\\"+param.getModuleName()+ "\\src\\main\\";
		String backEndTemplatePath = VelocityUtil.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		backEndTemplatePath = backEndTemplatePath.replace("/target/classes/", "/template/");
		VelocityEngine backEndVelocityEngine = new VelocityEngine();
		Properties backEndProperties = new Properties();
		backEndProperties.setProperty(Velocity.FILE_RESOURCE_LOADER_PATH, backEndTemplatePath);
		backEndVelocityEngine.init(backEndProperties);
		File backEndDir = new File(backEndTemplatePath);
		File[] backEndFiles = backEndDir.listFiles();
		if (backEndFiles.length > 0) {
			String filePath = "";
			for (int i = 0; i < backEndFiles.length; i++) {
				if (backEndFiles[i].isDirectory()) {
					continue;
				}
				String FileName = backEndFiles[i].getName();
				if ("Mapper.java".endsWith(FileName)) {
					filePath = backEndPath + "java\\" + packageName + ".dao";
				} else if ("Mapper.xml".endsWith(FileName)) {
					filePath = backEndPath + "resources.ormapping.ibatis." + param.getModuleName();
				} else if ("ServiceImpl.java".endsWith(FileName)) {
					filePath = backEndPath + "java\\" + packageName + ".service";
				} else if ("Model.java".endsWith(FileName)) {
					filePath = backEndPath + "java\\" + packageName + ".model";
				} else if ("ControllerAdmin.java".endsWith(FileName) || "Controller.java".endsWith(FileName)) {
					filePath = backEndPath + "java\\" + "org.offjob.boss.view." + param.getModuleName();
					context.put("PACKAGEPATHC", "org.offjob.boss.view." + param.getModuleName());
				}else {
					System.err.println("File not support backend file::" + FileName);
					continue;
				}
				filePath = filePath.replace(".", "\\");
				System.err.println(packageName);
				System.err.println(filePath);
				Template t = backEndVelocityEngine.getTemplate(backEndFiles[i].getName(), "UTF-8");
				FileName = context.get("TABLENAME").toString() + FileName;
				buildTemplate(context, filePath, FileName, t);
			}
		}
		//生成权限数据
		geSql(context.get("MODULENAME").toString(),context.get("TABLENAME").toString(), tableComent, menuId, role);
	}

	private static void geSql(String moduleName,String tableName, String tableComent, Integer menuId, String role) {
		String sqlPath = VelocityUtil.class.getProtectionDomain().getCodeSource().getLocation().getPath();

		sqlPath = sqlPath.replace("/target/classes/", "/sql/"+moduleName+"/"+ tableName);
		// tableName
		StringBuilder s = new StringBuilder();
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

		Integer uuid = menuId;
		tableComent = StringUtils.isEmptyOrWhitespaceOnly(tableComent) ? tableName + "管理" : tableComent;

		tableName = "admin".equalsIgnoreCase(role) ? "admin/" +moduleName+"/"+ tableName : moduleName+"/"+tableName;
		s.append("INSERT INTO `t_menu` VALUES (").append(uuid).append(",").append("'t_menu表中菜单ID',")
				.append("'" + tableComent + "',").append("'0',").append("'0',").append("'0',")
				.append("'" + tableName + "/index.htm',").append("'',").append("'0',").append("'" + date + "'")
				.append(");\n");

		s.append("INSERT INTO `t_menu` VALUES (").append("null,").append(uuid).append(",").append("'添加',")
				.append("'1',").append("'0',").append("'0',")
				.append("'" + tableName + "/add.htm," + tableName + "/baseDlg.htm',").append("'',").append("'0',")
				.append("'" + date + "'").append(");\n");
		s.append("INSERT INTO `t_menu` VALUES (").append("null,").append(uuid).append(",").append("'编辑',")
				.append("'1',").append("'0',").append("'0',")
				.append("'" + tableName + "/save.htm," + tableName + "/baseDlg.htm',").append("'',").append("'0',")
				.append("'" + date + "'").append(");\n");
		s.append("INSERT INTO `t_menu` VALUES (").append("null,").append(uuid).append(",").append("'删除',")
				.append("'1',").append("'0',").append("'0',").append("'" + tableName + "/del.htm',").append("'',")
				.append("'0',").append("'" + date + "'").append(");\n");
		s.append("INSERT INTO `t_menu` VALUES (").append("null,").append(uuid).append(",").append("'查看',")
				.append("'1',").append("'0',").append("'0',").append("'" + tableName + "/data.htm',").append("'',")
				.append("'0',").append("'" + date + "'").append(");\n");

		try {
			FileUtils.writeStringToFile(new File(sqlPath + ".sql"), s.toString(), "GBK");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void setValue(List<Map<String, Object>> clumListMap, VelocityContext context, String tableName,
			String tableComent) {
		List<TableDescModel> columnList = new ArrayList<TableDescModel>();
		TableDescModel model = null;
		context.put("TABLENAMEMAP", tableName);
		tableName = tableName.replaceFirst(".*_", "");
		String classIns = lineToHump(tableName);
		// 首字母大写
		String className = classIns.substring(0, 1).toUpperCase() + classIns.substring(1);

		context.put("TABLECOMENT", tableComent);
		context.put("TABLENAME", className);
		context.put("TABLENAMES", classIns);
		context.put("TABLENAMEUPPER", className.trim().toUpperCase());

		for (Map<String, Object> clum : clumListMap) {
			model = new TableDescModel();
			String name = clum.get("COLUMN_NAME").toString();
			model.setColumnName(name.toUpperCase());

			name = lineToHump(name);
			model.setFieldName(name);
			model.setFieldNameUpper(name.substring(0, 1).toUpperCase() + name.substring(1));

			if ("NO".equalsIgnoreCase(clum.get("IS_NULLABLE").toString())) {
				model.setCheck(true);
			}
			model.setComment((String) clum.get("COLUMN_COMMENT"));

			String type = clum.get("DATA_TYPE").toString();
			type = getFieldType(type);
			String[] vString = type.split(",");
			model.setFieldType(vString[0]);
			model.setFieldTypeLower(type.startsWith("java.") ? vString[0] : vString[0].toLowerCase());
			model.setType(Integer.parseInt(vString[1]));

			if (clum.get("COLUMN_KEY") != null && clum.get("COLUMN_KEY").toString().toUpperCase().startsWith("PRI")) {
				context.put("TABLEPKU", clum.get("COLUMN_NAME").toString().toUpperCase());
				model.setPk(true);
				context.put("TABLEPK", model.getFieldName());
				context.put("TABLEPKTYPE", model.getType());
			}
			columnList.add(model);
		}
		context.put("columnList", columnList);
	}

	private static Pattern linePattern = Pattern.compile("_(\\w)");

	/** 下划线转驼峰 */
	private static String lineToHump(String str) {
		str = str.toLowerCase();
		Matcher matcher = linePattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

	private static String getFieldType(String type) {
		for (Entry<String, String> e : SQLTYPE.entrySet()) {
			if (type.toLowerCase().indexOf(e.getKey()) >= 0) {
				return e.getValue();
			}
		}
		return null;
	}

	private static void buildTemplate(VelocityContext context, String saveDir, String fileName, Template t)
			throws Exception {
		File newsDir = new File(saveDir);
		if (!newsDir.exists()) {
			newsDir.mkdirs();
		}
		// try {
		Writer out = new OutputStreamWriter(new FileOutputStream(newsDir.getPath() + File.separator + fileName),
				"UTF-8");
		t.merge(context, out);
		out.flush();
		out.close();
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
	}

	public static void generateCode(Param param) {
		String paths[] = { "context-service.xml" };
		ApplicationContext ctx = new ClassPathXmlApplicationContext(paths);
		JdbcTemplate jdbcTemplate = (JdbcTemplate) ctx.getBean("jdbcTemplate");
		List<Map<String, Object>> clumListMap = jdbcTemplate.queryForList(
				"select COLUMN_NAME,DATA_TYPE,COLUMN_KEY,COLUMN_COMMENT,IS_NULLABLE from information_schema.COLUMNS where table_name =?",
				new Object[] { param.getTableName() });
		Map<String, Object> tb = jdbcTemplate.queryForMap(
				"SELECT table_name,table_comment FROM information_schema.`TABLES` where table_name=?",
				new Object[] { param.getTableName() });
		// 生成模板
		try {
			exportFixedVelocity(param, (String) tb.get("table_comment"), param.isPermission() ? "admin" : "common",
					clumListMap, jdbcTemplate.queryForObject("select Max(id) from t_menu", Integer.class) + 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 输出权限sql
	}

}
