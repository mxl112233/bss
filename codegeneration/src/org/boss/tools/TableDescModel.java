package org.boss.tools;

public class TableDescModel {
	private String fieldTypeLower;
	private String fieldType;
	private String fieldName;
	private String fieldNameUpper; // 第一个字母大写
	private String columnName;
	private boolean isPk;
	private String comment;
	private boolean check;
	//前段页面类型 0 字符串 1 数字 2 时间类型 3 下拉选项
	private int type;

	
	public boolean getCheck() {
		return check;
	}

	public void setCheck(boolean check) {
		this.check = check;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public boolean isPk() {
		return isPk;
	}

	public void setPk(boolean isPk) {
		this.isPk = isPk;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldNameUpper() {
		return fieldNameUpper;
	}

	public void setFieldNameUpper(String fieldNameUpper) {
		this.fieldNameUpper = fieldNameUpper;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getFieldTypeLower() {
		return fieldTypeLower;
	}

	public void setFieldTypeLower(String fieldTypeLower) {
		this.fieldTypeLower = fieldTypeLower;
	}

}
