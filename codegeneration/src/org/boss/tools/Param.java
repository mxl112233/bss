package org.boss.tools;

public class Param {
	private String tableName;
	private String moduleName;
	private String projectPath;
	private boolean permission;
	private boolean batchInsert;
	private boolean queryByMap;
	private String uiName;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getProjectPath() {
		return projectPath;
	}

	public void setProjectPath(String projectPath) {
		this.projectPath = projectPath;
	}

	public boolean isPermission() {
		return permission;
	}

	public void setPermission(boolean permission) {
		this.permission = permission;
	}

	public boolean isBatchInsert() {
		return batchInsert;
	}

	public void setBatchInsert(boolean batchInsert) {
		this.batchInsert = batchInsert;
	}

	public boolean isQueryByMap() {
		return queryByMap;
	}

	public void setQueryByMap(boolean queryByMap) {
		this.queryByMap = queryByMap;
	}

	public String getUiName() {
		return uiName;
	}

	public void setUiName(String uiName) {
		this.uiName = uiName;
	}
}
