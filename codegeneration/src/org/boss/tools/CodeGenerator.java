package org.boss.tools;

import java.sql.Time;

public class CodeGenerator {
    public static void main(String[] args) {
        System.err.println("单表操作 JAR结构后端代码生成开始................");

        // 参数说明依次：表名,模块名,生成代码的工程根目录,是否需要权限控制 ---后面扩展 批量扩展insert map查询 其他页面类型
        Param param = new Param();
//		 param.setBatchInsert(TRUE);
        param.setQueryByMap(true);
        param.setPermission(true);
        param.setTableName("k_type");
        param.setModuleName("knowledge");
        param.setProjectPath("E:\\code\\idea\\boss");
        param.setUiName("easyui");
        VelocityUtil.generateCode(param);
        System.err.println("");
        System.err.println("单表操作后端代码自动生成结束................");
        System.err.println(Time.valueOf("09:15:00"));
    }
}
