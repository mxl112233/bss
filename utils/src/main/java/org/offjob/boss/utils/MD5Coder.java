package org.offjob.boss.utils;

import org.apache.commons.codec.digest.DigestUtils;

public abstract class MD5Coder {
	/**
	 * MD5消息摘要
	 * 
	 * @param data
	 *            待处理的消息 明文
	 * @return byte[] 消息摘要 密文
	 * @throws Exception
	 */
	public static byte[] encodeMD5(String data) throws Exception {
		// 执行消息
		return DigestUtils.md5(data);
	}

	public static String encodeMD5Hex(String data) {
		// 执行消息摘要
		return DigestUtils.md5Hex(data);
	}
}