package org.offjob.boss.utils.validate;

/**
 * 断言扩展
 * 
 * @ClassName: Assert
 * @Description: TODO
 * @author xianglin.mo
 * @date 2015-8-24 下午08:19:45
 * @version [1.0, 2015-8-24]
 * @since version 1.0
 */
public class Assert {

	public static void hasText(Long text, String message) {
		if (text == null || text == 0)
			throw new IllegalArgumentException(message);
		else
			return;
	}

	public static void hasText(Integer text, String message) {
		if (text == null || text == 0)
			throw new IllegalArgumentException(message);
		else
			return;
	}

	public static void hasText(String text, String message) {
		org.springframework.util.Assert.hasText(text, message);
	}

	public static void hasText(String text) {
		org.springframework.util.Assert
				.hasText(
						text,
						"[Assertion failed] - this String argument must have text; it must not be null, empty, or blank");
	}

	public static void notNull(Object obj, String message) {
		if (obj == null) {
			throw new IllegalArgumentException(message);
		}
	}
}
