/**
 * 文件名：TimeContants.java
 *
 * 版本信息：
 * 日期：Apr 23, 2013
 * Copyright © 2013 , All Rights Reserved
 *
 */
package org.offjob.boss.utils.Contants;


/** 
 * 类名称：TimeContants
 * 类描述：字符串编码静态变量定义
 * 创建人：sq
 * 创建时间：Aug 6, 2013 10:29:04 AM
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0 
 */
public class StringCharsetContants {
	

	public static final String CHARSET_UTF8 = "UTF-8";
	public static final String CHARSET_GBK = "UTF-8";
	public static final String CHARSET_GB2312 = "UTF-8";
	public static final String CHARSET_ISO88591 = "ISO8859_1";

}