/**
 * 文件名：TimeContants.java
 *
 * 版本信息：
 * 日期：Apr 23, 2013
 * Copyright © 2013 , All Rights Reserved
 *
 */
package org.offjob.boss.utils.Contants;


/** 
 * 类名称：TimeContants
 * 类描述：时间类的静态变量定义
 * 创建人：sq
 * 创建时间：Aug 6, 2013 10:29:04 AM
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0 
 */
public class TimeContants {
	

	public static final String HOUR_START_STR = "00";
	public static final String HOUR_END_STR = "24";
	public static final String MINUTE_START_STR = "00";
	public static final String MINUTE_END_STR = "59";
	public static final String SECOND_START_STR = "00";
	public static final String SECOND_END_STR = "59";

}