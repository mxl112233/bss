package org.offjob.boss.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
	private DateUtils() {

	}

	private static SimpleDateFormat formatter = new SimpleDateFormat(
			"yyyy-MM-dd");
	
	private static SimpleDateFormat fullformatter = new SimpleDateFormat(
	"yyyy-MM-dd HH:mm:ss");

//	public static String getMsgSentDateString(Date sentDate) {
//		String dateString = "";
//		DateFormat hhmmSDF = new SimpleDateFormat("HH:mm");
//		DateFormat mmddSDF = new SimpleDateFormat("MM月dd日");
//		DateFormat yyyySDF = new SimpleDateFormat("yyyy-MM-dd");
//		DateFormat fullSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//		DateTime sentDateTime = new DateTime(fullSDF.format(sentDate));
//		DateTime todayDateTime = new DateTime(fullSDF.format(new Date()));
//		// 如果是在今天之内
//		if (sentDateTime.isSameDayAs(todayDateTime)) {
//			dateString = hhmmSDF.format(sentDate);
//		} else if (sentDateTime.numDaysFrom(todayDateTime) <= 90) {
//			// 如果在三个月内，就显示月和日
//			dateString = mmddSDF.format(sentDate);
//		} else {
//			// 否则就显示yyyy-MM-dd
//			dateString = yyyySDF.format(sentDate);
//		}
//		return dateString;
//	}

	public static String getSentDateFullString(Date sentDate) {
		DateFormat fullSDF = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
		return fullSDF.format(sentDate);
	}

	public static String getSentDateWeekString(Date sentDate) {
		final String dayNames[] = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五",
				"星期六" };
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(sentDate);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		if (dayOfWeek < 0) {
			dayOfWeek = 0;
		}
		return dayNames[dayOfWeek];
	}

	/**
	 * 字符串的日期格式的计算
	 */
	public static int daysBetween(String startTime, String endTime)
			throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(sdf.parse(startTime));
		long time1 = cal.getTimeInMillis();
		cal.setTime(sdf.parse(endTime));
		long time2 = cal.getTimeInMillis();
		long between_days = (time2 - time1) / (1000 * 3600 * 24);

		return Integer.parseInt(String.valueOf(between_days));
	}

	/**
	 * 计算两个日期之间相差的天数
	 * 
	 * @param smdate
	 *            较小的时间
	 * @param bdate
	 *            较大的时间
	 * @return 相差天数
	 * @throws ParseException
	 */
	public static int daysBetween(Date smdate, Date bdate)
			throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		smdate = sdf.parse(sdf.format(smdate));
		bdate = sdf.parse(sdf.format(bdate));
		Calendar cal = Calendar.getInstance();
		cal.setTime(smdate);
		long time1 = cal.getTimeInMillis();
		cal.setTime(bdate);
		long time2 = cal.getTimeInMillis();
		long between_days = (time2 - time1) / (1000 * 3600 * 24);

		return Integer.parseInt(String.valueOf(between_days));
	}

	public static String getCunDate(boolean isAfter, int day) {
		Date d = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		if (isAfter) {
			cal.add(Calendar.DATE, day);
		} else {
			cal.add(Calendar.DATE, -day);
		}
		String dateString = formatter.format(cal.getTime());
		return dateString;
	}
	
	public static String getCunDate() {
		Date d = new Date();
		String dateString = fullformatter.format(d);
		return dateString;
	}
	
	public static String getCunDate(boolean isAfter, int day,String date) {
		Calendar cal = Calendar.getInstance();
		try {
			cal.setTime(formatter.parse(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (isAfter) {
			cal.add(Calendar.DATE, day);
		} else {
			cal.add(Calendar.DATE, -day);
		}
		String dateString = formatter.format(cal.getTime());
		return dateString;
	}

	public static String getCunMothOneDay() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, 0);
		c.set(Calendar.DAY_OF_MONTH, 1);// 设置为1号,当前日期既为本月第一天
		String first = formatter.format(c.getTime());
		return first;
	}
	
	public static String getCunWeekOneDay() {
		Calendar c = Calendar.getInstance();
		c.setFirstDayOfWeek(Calendar.MONDAY);// 设置为1号,当前日期既为本月第一天
		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		String first = formatter.format(c.getTime());
		System.err.println(first);
		return first;
	}
	
	public static void main(String[] args) {
		
		System.err.println(getCunDate(true,1,"2014-07-01"));
	}
}
