package org.offjob.boss.utils;

import org.offjob.boss.utils.Contants.StringCharsetContants;

import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

/**
 * DES加密、解密类.
 * 
 * @author Winters He
 * 
 */
public class DESUtil {
	private final static String DES = "DES";
	
	public final static String KEY = "b25zZWFu";//onsean

	/**
	 * 加密
	 * 
	 * @param src
	 *            数据源
	 * @param key
	 *            密钥，长度必须是8的倍数
	 * @return 返回加密后的数据
	 * @throws Exception
	 */
	public static byte[] encrypt(byte[] src, byte[] key) throws Exception {
		// DES算法要求有一个可信任的随机数源
		SecureRandom sr = new SecureRandom();
		// 从原始密匙数据创建DESKeySpec对象
		DESKeySpec dks = new DESKeySpec(key);
		// 创建一个密匙工厂，然后用它把DESKeySpec转换成
		// 一个SecretKey对象
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES);
		SecretKey securekey = keyFactory.generateSecret(dks);
		// Cipher对象实际完成加密操作
		Cipher cipher = Cipher.getInstance(DES);
		// 用密匙初始化Cipher对象
		cipher.init(Cipher.ENCRYPT_MODE, securekey, sr);
		// 现在，获取数据并加密
		// 正式执行加密操作
		return cipher.doFinal(src);
	}

	/**
	 * 解密
	 * 
	 * @param src
	 *            数据源
	 * @param key
	 *            密钥，长度必须是8的倍数
	 * @return 返回解密后的原始数据
	 * @throws Exception
	 */
	public static byte[] decrypt(byte[] src, byte[] key) throws Exception {
		// DES算法要求有一个可信任的随机数源
		SecureRandom sr = new SecureRandom();
		// 从原始密匙数据创建一个DESKeySpec对象
		DESKeySpec dks = new DESKeySpec(key);
		// 创建一个密匙工厂，然后用它把DESKeySpec对象转换成
		// 一个SecretKey对象
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES);
		SecretKey securekey = keyFactory.generateSecret(dks);
		// Cipher对象实际完成解密操作
		Cipher cipher = Cipher.getInstance(DES);
		// 用密匙初始化Cipher对象
		cipher.init(Cipher.DECRYPT_MODE, securekey, sr);
		// 现在，获取数据并解密
		// 正式执行解密操作
		return cipher.doFinal(src);
	}

	  /**
	   * 已知密钥的情况下加密
	   */
	  public static String encode(String str, String key) throws Exception {
      SecureRandom sr = new SecureRandom();
	    byte[] rawKey = new sun.misc.BASE64Decoder().decodeBuffer(key);//Base64.decode(key);

	    DESKeySpec dks = new DESKeySpec(rawKey);
	    SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
	    SecretKey secretKey = keyFactory.generateSecret(dks);

	    Cipher cipher = Cipher.getInstance("DES");
	    cipher.init(Cipher.ENCRYPT_MODE, secretKey, sr);

	    byte data[] = str.getBytes("UTF8");
	    byte encryptedData[] = cipher.doFinal(data);
		String sRes = new sun.misc.BASE64Encoder().encodeBuffer(encryptedData);
		sRes = sRes.replaceAll("\r","");
		sRes = sRes.replaceAll("\n","");
			return sRes;

  }
	
	/**
	 * 密码解密
	 * 
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public final static String decrypt(String data, String key) {
		try {
			return new String(decrypt(hex2byte(data.getBytes(StringCharsetContants.CHARSET_UTF8)),
					key.getBytes(StringCharsetContants.CHARSET_UTF8)), StringCharsetContants.CHARSET_UTF8);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 密码加密
	 * 
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public final static String encrypt(String data, String key) {
		try {
			return byte2hex(encrypt(data.getBytes(StringCharsetContants.CHARSET_UTF8), key
					.getBytes(StringCharsetContants.CHARSET_UTF8)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * 二行制转字符串
	 * 
	 * @param b
	 * 
	 * @return
	 */

	public static String byte2hex(byte[] b) {

		String hs = "";

		String stmp = "";

		for (int n = 0; n < b.length; n++) {

			stmp = java.lang.Integer.toHexString(b[n] & 0XFF);

			if (stmp.length() == 1)

				hs = hs + "0" + stmp;

			else

				hs = hs + stmp;

		}

		return hs.toUpperCase();

	}

	public static byte[] hex2byte(byte[] b) {

		if (b.length % 2 != 0)

			throw new IllegalArgumentException("Length is not even");

		byte[] b2 = new byte[b.length / 2];

		for (int n = 0; n < b.length; n += 2) {

			String item = null;
			try {
				item = new String(b, n, 2, StringCharsetContants.CHARSET_UTF8);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

			b2[n / 2] = (byte) Integer.parseInt(item, 16);

		}

		return b2;
	}
	public static void main(String[] args) throws Exception {
//	System.err.println(encode("50#09718090922371234#091486606360775678","xdrckmpyjkor="));
//	System.err.println(new String(encrypt("50#09718090922371234#091486606360775678".getBytes(),"x7VhhchMp0Y=".getBytes())));
		System.err.println(encrypt("root", "onsean=="));
		System.err.println(decrypt("D09732B560465B0A", "onsean=="));
	}
}