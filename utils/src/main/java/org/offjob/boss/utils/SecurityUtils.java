package org.offjob.boss.utils;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class SecurityUtils {

	public static String sign(Map<String, Object> params, String acct_id,
			String apikey) throws Exception {
		TreeMap<String, Object> paramsMap = new TreeMap<String, Object>();
		paramsMap.putAll(params);
		Iterator<String> iter = paramsMap.keySet().iterator();
		StringBuffer paramsUrl = new StringBuffer();
		while (iter.hasNext()) {
			String key = iter.next();
			paramsUrl.append(key);
			paramsUrl.append(paramsMap.get(key));
		}
		paramsUrl.append(acct_id);
		paramsUrl.append(apikey);
		return new MD5()
				.get16MD5ofStr(
						new String(paramsUrl.toString().getBytes("ISO8859-1"),
								"utf-8")).toUpperCase();
	}

	/**
	 * 敏感字段需要先加密后放进params中
	 * 
	 * @Title: sign
	 * @param params
	 * @return
	 * @throws Exception
	 * @return String 返回类型
	 */
	public static String signMd5(Map<String, String> params, String appKey,
			String appSecret) throws Exception {
		return signMd5(params, appKey, appSecret, "ISO8859-1");
	}

	public static String signMd5(Map<String, String> params, String appKey,
			String appSecret, String charsetName) throws Exception {
		TreeMap<String, String> paramsMap = new TreeMap<String, String>();
		paramsMap.putAll(params);
		Iterator<String> iter = paramsMap.keySet().iterator();
		StringBuffer paramsUrl = new StringBuffer();
		paramsUrl.append(appSecret);
		paramsUrl.append("appKey").append(appKey);
		while (iter.hasNext()) {
			String key = iter.next();
			paramsUrl.append(key);
			paramsUrl.append(paramsMap.get(key));
		}
		paramsUrl.append(appSecret);
		return new MD5()
				.get16MD5ofStr(
						new String(paramsUrl.toString().getBytes(charsetName),
								"utf-8")).toUpperCase();
	}

	public static boolean sign(Map<String, String> params, String appKey,
			String appSecret, String ogisign) throws Exception {
		return signMd5(params, appKey, appSecret).equalsIgnoreCase(ogisign);
	}

	public static String getRandomString(int length) {
		String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		Random random = new Random();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++) {
			int number = random.nextInt(62);
			sb.append(str.charAt(number));
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		try {
			// for (int i = 0; i < 100; i++) {
			// System.err.println(getRandomString(32));
			// }
			System.err.println(new MD5().get16MD5ofStr(
					new String(getRandomString(32).getBytes("ISO8859-1"),
							"utf-8")).toUpperCase());

			System.err.println(new MD5()
			.get16MD5ofStr(new String("2D0BA8DD6E9449C6E053602FA8C0D816appKeyffan_10078959817billAccount18700085400merchantName1234562D0BA8DD6E9449C6E053602FA8C0D816".getBytes("utf-8"), "utf-8")));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
