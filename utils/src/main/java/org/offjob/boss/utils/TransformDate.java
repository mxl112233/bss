package org.offjob.boss.utils;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class TransformDate {

	/**
	 * 直接将当前时间只按日期(时间为0)作为mysql时间戳字段的条件 最终返回时间类型java.sql.Date
	 */
	public void transformCurDate() {
		/*try {
			Date timePara = new Date(new java.util.Date().getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}*/
	}

	/**
	 * 将java的当前时间转成指定格式(yyyy-MM-0100:00:00")作为mysql时间戳字段的条件
	 * 最终返回时间类型java.sql.Date
	 */
	public void transformCurYearMon() {
		/*SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
		String time = format.format(new java.util.Date()).concat("-0100:00:00");
		try {
			Date timePara = new Date(format.parse(time).getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}*/
	}

	public static Date getDate(String dateTime) throws Exception {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date = formatter.parse(dateTime.substring(0, 10));
		return new Date(date.getTime());
	}

	public static java.sql.Time getTime(String time) throws Exception {
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		java.util.Date date = formatter.parse(time.substring(11, time.length()));
		return new java.sql.Time(date.getTime());
	}

	public static void main(String[] args) {
		try {
			// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd
			// hh:mm:ss");
			// Timestamp date = java.sql.Timestamp.valueOf("2012-12-12
			// 01:12:11");
			// System.out.println(date);
			// SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			// String time = format.format(new Date());
			// Date date1 = format.parse(time.concat(" 00:00:00"));
			// System.out.println(date1);
			String time = "2011-10-01 18:10:10";
			System.err.println(getDate(time));
			System.err.println(getTime(time));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
