package org.offjob.boss.utils.validate;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
Bean Validation 中内置的 constraint       
@Null   被注释的元素必须为 null       
@NotNull    被注释的元素必须不为 null       
@AssertTrue     被注释的元素必须为 true       
@AssertFalse    被注释的元素必须为 false       
@Min(value)     被注释的元素必须是一个数字，其值必须大于等于指定的最小值       
@Max(value)     被注释的元素必须是一个数字，其值必须小于等于指定的最大值       
@DecimalMin(value)  被注释的元素必须是一个数字，其值必须大于等于指定的最小值       
@DecimalMax(value)  被注释的元素必须是一个数字，其值必须小于等于指定的最大值       
@Size(max=, min=)   被注释的元素的大小必须在指定的范围内       
@Digits (integer, fraction)     被注释的元素必须是一个数字，其值必须在可接受的范围内       
@Past   被注释的元素必须是一个过去的日期       
@Future     被注释的元素必须是一个将来的日期       
@Pattern(regex=,flag=)  被注释的元素必须符合指定的正则表达式       
Hibernate Validator 附加的 constraint       
@NotBlank(message =)   验证字符串非null，且长度必须大于0       
@Email  被注释的元素必须是电子邮箱地址       
@Length(min=,max=)  被注释的字符串的大小必须在指定的范围内       
@NotEmpty   被注释的字符串的必须非空       
@Range(min=,max=,message=)  被注释的元素必须在合适的范围内   
**/
public class ValidatorUtil {
	private static ValidatorFactory factory = Validation
			.buildDefaultValidatorFactory();

	public static Validator getDefaultValidator() {
		return factory.getValidator();
	}
	
	public static <T> StringBuffer validate(T bean) {
		StringBuffer errorMsg = new StringBuffer();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<T>> violations = validator.validate(bean);
		if (violations.size() != 0) {
			for (ConstraintViolation<T> violation : violations) {
				errorMsg.append(violation.getMessage()).append(";");
			}
		}
		return errorMsg;
	}
}
