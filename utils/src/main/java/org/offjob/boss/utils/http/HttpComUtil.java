package org.offjob.boss.utils.http;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.offjob.boss.utils.Contants.IPContants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.offjob.boss.utils.JsonUtil;

public class HttpComUtil {
	private final static Logger log = LoggerFactory.getLogger(HttpComUtil.class);

	public static String postHttpRequest(Object requestObject, String serviceId, String url) throws Exception {
		return postHttpRequest(requestObject, serviceId, url, Integer.valueOf(60000));
	}

	public static String postHttpRequest(Object requestObject, String serviceId, String url, Integer timeOut)
			throws Exception {
		CloseableHttpResponse response = null;
		String responseString = null;
		try {
			String reqeustString = "[]";
			if (requestObject != null) {
				reqeustString = JsonUtil.beanToJSON(requestObject);
			}
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(url);
			httpPost.addHeader("x-service-id", serviceId);
			httpPost.addHeader("x-client-ip", getLocalIpAddr());
			// httpPost.addHeader("x-app-code", appCode);
			RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(timeOut.intValue())
					.setConnectTimeout(timeOut.intValue()).setSocketTimeout(timeOut.intValue()).build();
			httpPost.setConfig(requestConfig);
			httpPost.setEntity(new StringEntity(reqeustString, ContentType.APPLICATION_JSON));
			response = httpclient.execute(httpPost);
			int resStatu = response.getStatusLine().getStatusCode();
			if (resStatu == 200 && "00".equals(response.getHeaders("x-service-code")[0].getValue())) {
				responseString = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
			} else {
				String error = new StringBuilder().append(url).append(": resStatu is ").append(resStatu).append(";")
						.append(Arrays.toString(response.getHeaders("x-service-id"))).append(";")
						.append(Arrays.toString(response.getHeaders("x-service-code"))).append(";")
						.append(Arrays.toString(response.getHeaders("x-service-message"))).toString();
				log.error(error);
				throw new Exception(error);
			}
		} catch (Exception e) {
			log.error("Error", e);
			throw e;
		} finally {
			if (response != null)
				try {
					EntityUtils.consume(response.getEntity());
					response.close();
				} catch (Exception e) {
					log.error("Error", e);
				}
		}
		return responseString;
	}

	public static String getLocalIpAddr() {
		String ip = null;
		try {
			ip = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			log.error("UnknownHostException fail......", e);
		}
		return ip != null && ip.length() > 20 ? ip.substring(0, 20) : ip;
	}

	/**
	 * 获取登录用户的IP地址
	 * 
	 * @param request 请求
	 * @return ip
	 */
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader(IPContants.REQUEST_HEAD_KEY_X_FORWARDED_FOR);
		if (ip == null || ip.length() == 0 || IPContants.UNKNOW_IP.equalsIgnoreCase(ip)) {
			ip = request.getHeader(IPContants.REQUEST_HEAD_KEY_PROXY_CLIENT_IP);
			if (ip == null || ip.length() == 0 ||IPContants.UNKNOW_IP.equalsIgnoreCase(ip)) {
				ip = request.getHeader(IPContants.REQUEST_HEAD_KEY_WL_PROXY_CLIENT_IP);

			}
			if (ip == null || ip.length() == 0 || IPContants.UNKNOW_IP.equalsIgnoreCase(ip)) {
				ip = request.getHeader(IPContants.REQUEST_HEAD_KEY_HTTP_CLIENT_IP);

			}
			if (ip == null || ip.length() == 0 || IPContants.UNKNOW_IP.equalsIgnoreCase(ip)) {
				ip = request.getHeader(IPContants.REQUEST_HEAD_KEY_HTTP_X_FORWARDED_FOR);

			}
			if (ip == null || ip.length() == 0 || IPContants.UNKNOW_IP.equalsIgnoreCase(ip)) {
				ip = request.getRemoteAddr();
			}
		} else if (ip.length() > 15) {
			String[] ips = ip.split(",");
			for (String strIp:ips) {
				if (!IPContants.UNKNOW_IP.equalsIgnoreCase(strIp)) {
					ip = strIp;
					break;
				}
			}
		}
		if (ip.equals(IPContants.LOCAL_IP)) {
			ip = IPContants.LOCAL_IP_ZH;
		}
		return ip.length() > 20 ? ip.substring(0, 20) : ip;
	}

}
