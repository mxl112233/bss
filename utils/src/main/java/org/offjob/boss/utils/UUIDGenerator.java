package org.offjob.boss.utils;

import java.net.InetAddress;
import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 
 * @author 张代浩
 * 
 */
public class UUIDGenerator {

	// private final static Logger logger = LoggerFactory
	// .getLogger(UUIDGenerator.class);

	/**
	 * 产生一个32位的UUID
	 * 
	 * @return
	 */

	public static String generate() {
		return new StringBuilder(32).append(format(getIP()))
				.append(format(getJVM())).append(format(getHiTime()))
				.append(format(getLoTime())).append(format(getCount()))
				.toString();

	}

	private static final int IP;

	private static String IP3;

	static {
		IP3 = "999";
		int ipadd;
		try {
			IP3 = InetAddress.getLocalHost().getHostAddress();
			if (IP3.indexOf(".") > 0) {
				IP3 = IP3.substring(IP3.lastIndexOf(".") + 1, IP3.length());
				IP3 = IP3.length() == 1 ? ("00" + IP3)
						: IP3.length() == 2 ? ("0" + IP3) : IP3;
			}
			ipadd = toInt(InetAddress.getLocalHost().getAddress());
		} catch (Exception e) {
			ipadd = 0;
		}
		IP = ipadd;
	}

	private static short counter = (short) 0;

	private static final int JVM = (int) (System.currentTimeMillis() >>> 8);

	private final static String format(int intval) {
		String formatted = Integer.toHexString(intval);
		StringBuilder buf = new StringBuilder("00000000");
		buf.replace(8 - formatted.length(), 8, formatted);
		return buf.toString();
	}

	private final static String format(short shortval) {
		String formatted = Integer.toHexString(shortval);
		StringBuilder buf = new StringBuilder("0000");
		buf.replace(4 - formatted.length(), 4, formatted);
		return buf.toString();
	}

	private final static int getJVM() {
		return JVM;
	}

	private final static short getCount() {
		synchronized (UUIDGenerator.class) {
			if (counter < 0)
				counter = 0;
			return counter++;
		}
	}

	/**
	 * Unique in a local network
	 */
	private final static int getIP() {
		return IP;
	}

	/**
	 * Unique down to millisecond
	 */
	private final static short getHiTime() {
		return (short) (System.currentTimeMillis() >>> 32);
	}

	private final static int getLoTime() {
		return (int) System.currentTimeMillis();
	}

	private final static int toInt(byte[] bytes) {
		int result = 0;
		for (int i = 0; i < 4; i++) {
			result = (result << 8) - Byte.MIN_VALUE + (int) bytes[i];
		}
		return result;
	}

	/** The FieldPosition. */
	private static final FieldPosition HELPER_POSITION = new FieldPosition(0);

	/** This Format for format the data to special format. */
	private final static Format dateFormat = new SimpleDateFormat(
			"yyMMddHHmmss");

	/** This Format for format the number to special format. */
	private final static NumberFormat numberFormat = new DecimalFormat("0000");

	/** This int is the sequence number ,the default value is 0. */
	private static int seq = 0;

	private static final int MAX = 9999;
	private static Lock lock = new ReentrantLock();

	/**
	 * 时间格式生成序列
	 * 
	 * @return String
	 */
	public static synchronized String generateSequenceNo() {
		Calendar rightNow = Calendar.getInstance();
		StringBuffer sb = new StringBuffer();
		dateFormat.format(rightNow.getTime(), sb, HELPER_POSITION);
		numberFormat.format(seq, sb, HELPER_POSITION);
		if (seq == MAX) {
			seq = 0;
		} else {
			seq++;
		}
		return sb.toString();
	}

	/**
	 * 时间格式+三位机器吗+生成序列
	 * 
	 * @return String
	 */
	public static String getNextSeq() {
		Calendar rightNow = Calendar.getInstance();
		StringBuffer sb = new StringBuffer();
		dateFormat.format(rightNow.getTime(), sb, HELPER_POSITION);
		sb.append(IP3);
		try {
			lock.lock();
			if (seq == MAX) {
				seq = 0;
			} else {
				seq++;
			}
		} finally {
			lock.unlock();
		}
		numberFormat.format(seq, sb, HELPER_POSITION);
		return sb.toString();
	}

	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			// System.err.println(UUIDGenerator.generate());
			System.err.println(UUIDGenerator.getNextSeq());
		}
		System.out.println(Long.MAX_VALUE);
	}

}
