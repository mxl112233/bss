package org.offjob.boss.utils.Contants;


/** 
 * 类名称：IPContants
 * 类描述：IP类的静态变量定义
 * 创建人：sq
 * 创建时间：Aug 6, 2013 10:29:04 AM
 * 修改人：
 * 修改时间：
 * 修改备注：
 * @version 1.0 
 */
public class IPContants {
	public static final String LOCAL_IP = "0"+":"+"0"+":"+"0"+":"+"0"+":"+"0"+":"+"0"+":"+"0"+":"+"1";
	public static final String LOCAL_IP_ZH = "本地";
	public static final String UNKNOW_IP = "unknown";
	public static final String REQUEST_HEAD_KEY_X_FORWARDED_FOR = "x-forwarded-for";
	public static final String REQUEST_HEAD_KEY_PROXY_CLIENT_IP = "Proxy-Client-IP";
	public static final String REQUEST_HEAD_KEY_WL_PROXY_CLIENT_IP = "WL-Proxy-Client-IP";
	public static final String REQUEST_HEAD_KEY_HTTP_CLIENT_IP = "HTTP_CLIENT_IP";
	public static final String REQUEST_HEAD_KEY_HTTP_X_FORWARDED_FOR = "HTTP_X_FORWARDED_FOR";

}