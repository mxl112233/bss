package org.offjob.boss.task;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 事件类，基于Spring注解方式 ---不依赖quartz。jar  基于jdk的task做的
 */
@Component
public class JobTest {
	public JobTest() {
		System.out.println("MyJob创建成功");
	}

	@Scheduled(cron = "0/1 * *  * * ? ") // 每隔1秒隔行一次
	public void run() {
		System.out.println("Hello MyJob  " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ").format(new Date()));
	}

}
