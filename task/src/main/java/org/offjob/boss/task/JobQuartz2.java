package org.offjob.boss.task;

import org.offjob.boss.job.QuartzJob;

/**
 * 基于quartz。jar实现 需要配置xml 可实现多节点 必须依赖数据库
 * 
 * @author Administrator
 *
 */
@QuartzJob(name = "job1", cron = "0/3 * * * * ?")
public class JobQuartz2 {

	public void run() {
		// 需要做的事情
		System.out.println("执行targetObject中的targetMethod方法，开始！");
		System.out.println("执行targetObject中的targetMethod方法，结束！");
	}

}
