package org.offjob.boss.task;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import org.offjob.boss.job.QuartzJob;

/**
 * spring启动时记载loadCronTriggers
 * 
 * @author Administrator
 *
 */
@Component
public class QuartJobSchedulingListener extends SchedulerFactoryBean {
	private static final Logger logger = Logger.getLogger(QuartJobSchedulingListener.class);
	private ApplicationContext applicationContext;

	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	public void registerJobsAndTriggers() throws SchedulerException {
		try {
			Map<String, Object> quartzJobBeans = applicationContext.getBeansWithAnnotation(QuartzJob.class);
			Set<String> beanNames = quartzJobBeans.keySet();
			for (String beanName : beanNames) {
				Object targetObject = quartzJobBeans.get(beanName);
				QuartzJob job = AnnotationUtils.findAnnotation(targetObject.getClass(), QuartzJob.class);
				registerJobs(targetObject, job.method(), job.name(), job.cron());
			}
			// 获取所有bean name
			// String[] beanNames =
			// applicationContext.getBeanNamesForType(Object.class);
			// for (String beanName : beanNames) {
			// Class<?> targetClass = applicationContext.getType(beanName);
			// // 循环判断是否标记了MyTriggerType注解
			// if (targetClass.isAnnotationPresent(QuartzJob.class)) {
			// Object targetObject = applicationContext.getBean(beanName);
			// QuartzJob job = targetClass.getAnnotation(QuartzJob.class);
			// registerJobs(targetObject, job.method(), job.name(), job.cron());
			// }
			// }
		} catch (Exception e) {
			logger.error("----->>>>>registerJobsAndTriggers fail.......", e);
		}
	}

	/**
	 * 注册定时器
	 * 
	 * @param targetObject
	 * @param targetMethod
	 * @param beanName
	 * @param cronExpression
	 * @throws Exception
	 */
	private void registerJobs(Object targetObject, String targetMethod, String beanName, String cronExpression)
			throws Exception {
		// 声明包装业务类
		MethodInvokingJobDetailFactoryBean jobDetailFactoryBean = new MethodInvokingJobDetailFactoryBean();
		jobDetailFactoryBean.setTargetObject(targetObject);
		jobDetailFactoryBean.setTargetMethod(targetMethod);
		jobDetailFactoryBean.setBeanName(beanName + "_" + targetMethod + "_Task");
		jobDetailFactoryBean.setName(beanName + "_" + targetMethod + "_Task");
		jobDetailFactoryBean.setConcurrent(false);
		jobDetailFactoryBean.afterPropertiesSet();

		// 获取JobDetail
		JobDetail jobDetail = jobDetailFactoryBean.getObject();

		// 声明定时器
		CronTriggerFactoryBean cronTriggerBean = new CronTriggerFactoryBean();
		cronTriggerBean.setJobDetail(jobDetail);
		cronTriggerBean.setCronExpression(cronExpression);
		cronTriggerBean.setName(beanName + "_" + targetMethod + "_Trigger");
		cronTriggerBean.setBeanName(beanName + "_" + targetMethod + "_Trigger");
		cronTriggerBean.afterPropertiesSet();

		CronTrigger trigger = cronTriggerBean.getObject();
		// 将定时器注册到factroy
		List<Trigger> triggerList = new ArrayList<Trigger>();
		triggerList.add(trigger);
		Trigger[] triggers = (Trigger[]) triggerList.toArray(new Trigger[triggerList.size()]);
		setTriggers(triggers);
		System.err.println("------>>registerJob info success:" + JSON.toJSONString(cronTriggerBean));
		super.registerJobsAndTriggers();
	}

}
