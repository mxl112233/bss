﻿function ww4(date) {
	var y = date.getFullYear();
	var m = date.getMonth() + 1;
	return y + '-' + (m < 10 ? ('0' + m) : m);

}
function w4(s) {
	var reg = /[\u4e00-\u9fa5]/ // 利用正则表达式分隔
	var ss = (s.split(reg));
	var y = parseInt(ss[0], 10);
	var m = parseInt(ss[1], 10);
	var d = parseInt(ss[2], 10);
	var h = parseInt(ss[3], 10);
	if (!isNaN(y) && !isNaN(m) && !isNaN(d) && !isNaN(h)) {
		return new Date(y, m - 1, d, h);
	} else {
		return new Date();
	}
}
function expData(url, fm) {
	$('#' + fm).form('submit', {
		url : url,
		dataType : 'json',
		onSubmit : function() {
			var isValid = $(this).form('validate');
			if (!isValid) {
				topCenter('提醒', '必填项不能为空!');
				$.messager.progress('close')
			}
			return isValid
		},
		success : function(result) {
			$.messager.progress('close');
			var result = eval('(' + result + ')');
			if (result.success) {
				topCenter('提醒', result.msg)
			} else {
				topCenter('失败', result.msg)
			}
		}
	});
}
function addAddress(title, input, key, url) {
	$('<div/>').dialog(
			{
				href : url,
				modal : true,
				iconCls : 'icon-save',
				title : title,
				top : '15%',
				left : '30%',
				width : 600,
				height : 400,
				resizable : true,
				buttons : [
						{
							text : '确定',
							handler : function() {
								var rows = $('#companyAddressByDeptGrid')
										.datagrid('getSelections');
								if (rows.length > 0) {
									var ids = '';
									for (var i = 0; i < rows.length; i++) {
										if (key == 'mail') {
											ids += rows[i].mail + ';';
										} else if (key == 'username') {
											ids += rows[i].username + ';';
										} else if (key == 'phone') {
											ids += rows[i].phone + ';';
										} else {
											ids += rows[i].realname + ';';
										}
									}
									;
									ids = ids.substring(0, ids.length - 1);
									$('#' + input).val(ids);
									$(this).closest('.window-body').dialog(
											'destroy');
								} else {
									topCenter('提醒', '请选择一行记录!');
								}
							}
						},
						{
							text : '取消',
							handler : function() {
								$(this).closest('.window-body').dialog(
										'destroy');
							}
						} ],
				onClose : function() {
					$(this).dialog('destroy');
				}
			});
}
function topCenter(title, content) {
	$.messager.show({
		title : title,
		msg : content,
		showType : 'slide',
		style : {
			right : '',
			top : document.body.scrollTop + document.documentElement.scrollTop,
			bottom : ''
		}
	});
}
function addGrid(url, id, dlgUrl, uploadFlg) {
	commGrid('新增', url, 0, id + 'Grid', id + 'Fm', id + 'Dlg', dlgUrl,
			uploadFlg, '', '', '');
}
function addGridwidth(title, url, id, dlgUrl, uploadFlg, heightValue,
		widthValue) {
	commGridwidth(title, url, 0, id + 'Grid', id + 'Fm', id + 'Dlg', dlgUrl,
			uploadFlg, '', '', '', heightValue, widthValue);
}
function addtreeGrid(url, id, dlgUrl, treeId, treeUrl) {
	commGrid('新增', url, 0, id + 'Grid', id + 'Fm', id + 'Dlg', dlgUrl, false,
			'tree', treeId, treeUrl);
}
function editGrid(url, id, dlgUrl, uploadFlg) {
	commGrid('编辑', url, 1, id + 'Grid', id + 'Fm', id + 'Dlg', dlgUrl,
			uploadFlg, '', '', '');
}
function editGridwidth(title, url, id, dlgUrl, uploadFlg, heightValue,
		widthValue) {
	var gd = $('#' + id + 'Grid');
	var rows = gd.datagrid('getSelections');
	if (rows.length != 1) {
		topCenter('提醒', '请选择一行记录!');
		return;
	}
	commDialogWidth(title, url, 1, gd, id + 'Fm', id + 'Dlg', dlgUrl,
			uploadFlg, '', '', '', heightValue, widthValue);
}
function detailGridwidth(title, url, id, dlgUrl, uploadFlg, heightValue,
		widthValue) {
	var gd = $('#' + id + 'Grid');
	var rows = gd.datagrid('getSelections');
	if (rows.length != 1) {
		topCenter('提醒', '请选择一行记录!');
		return;
	}
	detailDialogWidth(title, url, 1, gd, id + 'Fm', id + 'Dlg', dlgUrl,
			uploadFlg, '', '', '', heightValue, widthValue);
}
function edittreeGrid(url, id, dlgUrl, treeId, treeUrl) {
	commGrid('编辑', url, 1, id + 'Grid', id + 'Fm', id + 'Dlg', dlgUrl, false,
			'tree', treeId, treeUrl);
}
function edit2andGrid(url, id, dlgUrl, tabsUrl, uploadFlg) {
	commGrid('编辑', url, 1, id + 'Grid', id + 'Fm', id + 'Dlg', dlgUrl,
			uploadFlg, '', '', '');
}
function commGrid(title, url, type, grid, fm, dlg, dlgUrl, isupload, treeType,
		treeId, treeurl) {
	var gd = $('#' + grid);
	if (type == 1) {
		var rows = gd.datagrid('getSelections');
		if (rows.length != 1) {
			topCenter('提醒', '请选择一行记录!');
			return;
		}
	}
	commDialog(title, url, type, gd, fm, dlg, dlgUrl, isupload, treeType,
			treeId, treeurl, '600')
}
function commGridwidth(title, url, type, grid, fm, dlg, dlgUrl, isupload,
		treeType, treeId, treeurl, heightValue, widthValue) {
	var gd = $('#' + grid);
	if (type == 1) {
		var rows = gd.datagrid('getSelections');
		if (rows.length != 1) {
			topCenter('提醒', '请选择一行记录!');
			return;
		}
	}
	commDialogWidth(title, url, type, gd, fm, dlg, dlgUrl, isupload, treeType,
			treeId, treeurl, heightValue, widthValue)
}
function systemSetDialog(url, id, dlgUrl) {
	commDialog('系统全局设置', url, 1, '', id + 'Fm', id + 'Dlg', dlgUrl, true, '',
			'', '', '600')
}
function commDialog(title, url, type, gd, fm, dlg, dlgUrl, isupload, treeType,
		treeId, treeurl, widthValue) {
	var flg = false;
	$('<div/>').dialog({
		href : dlgUrl,
		modal : true,
		title : title,
		top : '15%',
		left : '30%',
		width : widthValue,
		resizable : true,
		buttons : [ {
			text : '确定',
			handler : function() {
				if ($('#' + fm).length > 0) {
					$.messager.progress();
					$('#' + fm).form('submit', {
						url : url,
						iframe : isupload,
						contentType : 'application/json; charset=utf-8',
						dataType : 'json',
						onSubmit : function() {
							var isValid = $(this).form('validate');
							if (!isValid) {
								topCenter('提醒', '必填项不能为空!');
								$.messager.progress('close');
							} else {
								flg = true;
							}
							return isValid;
						},
						success : function(data) {
							$.messager.progress('close');
							var result = eval('(' + data + ')');
							if (result.success) {
								$('#' + dlg).dialog('close');
								if (gd != '') {
									if (treeType == 'tree') {
										gd.treegrid('reload');
									} else {
										gd.datagrid('reload');
										gd.datagrid('uncheckAll');
									}
								} else {
									topCenter('提醒', result.msg);
								}
							} else {
								topCenter('提醒', result.msg);
							}
						}
					});
				}
				if (flg) {
					$(this).closest('.window-body').dialog('destroy');
				}
			}
		}, {
			text : '取消',
			handler : function() {
				$(this).closest('.window-body').dialog('destroy');
			}
		} ],
		onClose : function() {
			$(this).dialog('destroy');
		},
		onLoad : function() {
			if (gd != '') {
				if (type == 0) {
				} else {
					$('#' + fm).form('clear');
					var rows = gd.datagrid('getSelections');
					$('#' + fm).form('load', rows[0]);
				}
				if (treeType == 'tree') {
					var comboxtree = $('#' + treeId);
					comboxtree.before('<span style=\"padding-left: 2px;\"/>');
					comboxtree.combotree({
						width : 155,
						multiple : false,
						url : treeurl
					});
				}
			}
		}
	});
}
function detailDialogWidth(title, url, type, gd, fm, dlg, dlgUrl, isupload,
		treeType, treeId, treeurl, heightValue, widthValue) {
	var flg = false;
	$('<div/>').dialog({
		href : dlgUrl,
		modal : true,
		title : title,
		top : '15%',
		left : '10%',
		height : heightValue,
		width : widthValue,
		resizable : true,
		buttons : [ {
			text : '关闭',
			handler : function() {
				$(this).closest('.window-body').dialog('destroy');
			}
		} ],
		onClose : function() {
			$(this).dialog('destroy');
		},
		onLoad : function() {
			if (gd != '') {
				if (type == 0) {
				} else {
					$('#' + fm).form('clear');
					var rows = gd.datagrid('getSelections');
					$('#' + fm).form('load', rows[0]);
				}
				if (treeType == 'tree') {
					var comboxtree = $('#' + treeId);
					comboxtree.before('<span style=\"padding-left: 2px;\"/>');
					comboxtree.combotree({
						width : 155,
						multiple : false,
						url : treeurl
					});
				}
			}
		}
	});
}
function commDialogWidth(title, url, type, gd, fm, dlg, dlgUrl, isupload,
		treeType, treeId, treeurl, heightValue, widthValue) {
	var flg = false;
	$('<div/>').dialog({
		href : dlgUrl,
		modal : true,
		title : title,
		top : '15%',
		left : '30%',
		height : heightValue,
		width : widthValue,
		resizable : true,
		buttons : [ {
			text : '确定',
			handler : function() {
				if ($('#' + fm).length > 0) {
					$.messager.progress();
					$('#' + fm).form('submit', {
						url : url,
						iframe : isupload,
						contentType : 'application/json; charset=utf-8',
						dataType : 'json',
						onSubmit : function() {
							var isValid = $(this).form('validate');
							if (!isValid) {
								topCenter('提醒', '必填项不能为空!');
								$.messager.progress('close');
							} else {
								flg = true;
							}
							return isValid;
						},
						success : function(data) {
							$.messager.progress('close');
							var result = eval('(' + data + ')');
							if (result.success) {
								$('#' + dlg).dialog('close');
								if (gd != '') {
									if (treeType == 'tree') {
										gd.treegrid('reload');
									} else {
										gd.datagrid('reload');
										gd.datagrid('uncheckAll');
									}
								} else {
									topCenter('提醒', result.msg);
								}
							} else {
								topCenter('提醒', result.msg);
							}
						}
					});
				}
				if (flg) {
					$(this).closest('.window-body').dialog('destroy');
				}
			}
		}, {
			text : '取消',
			handler : function() {
				$(this).closest('.window-body').dialog('destroy');
			}
		} ],
		onClose : function() {
			$(this).dialog('destroy');
		},
		onLoad : function() {
			if (gd != '') {
				if (type == 0) {
				} else {
					$('#' + fm).form('clear');
					var rows = gd.datagrid('getSelections');
					$('#' + fm).form('load', rows[0]);
				}
				if (treeType == 'tree') {
					var comboxtree = $('#' + treeId);
					comboxtree.before('<span style=\"padding-left: 2px;\"/>');
					comboxtree.combotree({
						width : 155,
						multiple : false,
						url : treeurl
					});
				}
			}
		}
	});
}
function formSubmit(url, fm, msg) {
	$.messager.progress();
	$('#' + fm).form('submit', {
		url : url,
		dataType : 'json',
		onSubmit : function() {
			var isValid = $(this).form('validate');
			if (!isValid) {
				topCenter('提醒', '必填项不能为空!');
				$.messager.progress('close');
			}
			return isValid;
		},
		success : function(result) {
			$.messager.progress('close');
			var result = eval('(' + result + ')');
			if (result.success) {
				topCenter('提醒', result.msg);
			} else {
				topCenter('失败', result.msg);
			}
		}
	});
}
function formSearch(gridType, fm, grid, type) {
	var gd = $('#' + grid);
	gd.datagrid('options').pageNumber = 1;
	var isValid = $('#' + fm).form('validate');
	if (!isValid) {
		topCenter('提醒', '必填项不能为空!');
		return;
	}
	var obj = $('#' + fm).serializeJson();
	obj['searchType'] = type;
	if (gridType == 'tree') {
		gd.treegrid({
			queryParams : obj
		});
	} else {
		gd.datagrid({
			queryParams : obj
		});
		gd.datagrid('uncheckAll');
	}
}
function formMethod(fm, method) {
	$('#' + fm).form(method);
}
function gridReload(gridType, grid, url) {
	var gd = $('#' + grid);
	if (gridType == 'tree') {
		gd.treegrid('options').pageNumber = 1;
		gd.treegrid('options').url = url;
		gd.treegrid('reload');
	} else {
		gd.datagrid('options').pageNumber = 1;
		gd.datagrid('reload', {});
		gd.datagrid('uncheckAll');
	}
}
function gridDelete(gridType, grid, url) {
	delcomm(gridType, grid, url, 'id');
}
function delcomm(gridType, grid, url, key) {
	var gd = $('#' + grid);
	var rows = gd.datagrid('getSelections');
	if (rows.length > 0) {
		$.messager.confirm('Confirm', '确定要删除选择的数据吗?', function(r) {
			if (r) {
				var ids = '';
				for (var i = 0; i < rows.length; i++) {
					ids += 'ids=' + rows[i][key] + '&';
				}
				ids = ids.substring(0, ids.length - 1);
				$.get(url + "?" + ids, function(result) {
					if (result.success) {
						if (gridType == 'tree') {
							gd.treegrid('reload');
							gd.treegrid('clearSelections');
						} else {
							gd.datagrid('reload');
							gd.datagrid('clearSelections');
						}
					} else {
						topCenter('失败', result.msg);
					}
				}, 'json');
			}
		});
	} else {
		$.messager.alert('消息', '请选择要删除的数据!', 'info');
	}
}
function gridDeleteByKey(gridType, grid, url, key) {
	delcomm(gridType, grid, url, key);
}
function updateInfoComm(gridType, grid, url, key) {
	var gd = $('#' + grid);
	var rows = gd.datagrid('getSelections');
	if (rows.length == 1) {
		$.messager.confirm('Confirm', '确认要重置密匙吗?', function(r) {
			if (r) {
				var ids = '';
				for (var i = 0; i < rows.length; i++) {
					ids += key += '=' + rows[i][key] + '&';
				}
				ids = ids.substring(0, ids.length - 1);
				$.get(url + "?" + ids, function(result) {
					if (result.success) {
						if (gridType == 'tree') {
							gd.treegrid('reload');
							gd.treegrid('clearSelections');
						} else {
							gd.datagrid('reload');
							gd.datagrid('clearSelections');
						}
					} else {
						topCenter('失败', result.msg);
					}
				}, 'json');
			}
		});
	} else {
		$.messager.alert('消息', '请选择一行记录!', 'info');
	}
}
function gridUpateInfoByKey(gridType, grid, url, key) {
	updateInfoComm(gridType, grid, url, key);
}
function gridExportAndImport(url, type, grid) {
	if (type == 0) {
		window.location = url;
		return;
	}
	if (type == 1) {
		$('<div/>').dialog({
			href : url,
			modal : true,
			title : '导入',
			top : '30%',
			left : '40%',
			width : 320,
			height : 270,
			resizable : false,
			buttons : [ {
				text : '确定',
				handler : function() {
					$(this).closest('.window-body').dialog('destroy');
					$('#' + grid).datagrid('reload');
				}
			}, {
				text : '取消',
				handler : function() {
					$(this).closest('.window-body').dialog('destroy');
				}
			} ]
		});
		return;
	}
}
function searchARemove(curr) {
	if ($(curr).closest('table').find('tr').size() > 2) {
		$(curr).closest('tr').remove();
	} else {
		alert('该行不允许删除');
	}
}
function searchA(gridType, grid, fm, url) {
	var gd = $('#' + grid);
	$('<div/>').dialog({
		href : url,
		modal : true,
		title : '高级查询',
		top : 120,
		width : 480,
		buttons : [ {
			text : '增加一行',
			handler : function() {
				var currObj = $(this).closest('.panel').find('table');
				currObj.find('tr:last').clone().appendTo(currObj);
			}
		}, {
			text : '确定',
			handler : function() {
				if (gridType == 'tree') {
					gd.treegrid('options').pageNumber = 1;
					gd.treegrid('load', serializeObjectEx($('#' + fm)));
				} else {
					gd.datagrid('options').pageNumber = 1;
					gd.datagrid('load', serializeObjectEx($('#' + fm)));
				}
			}
		}, {
			text : '取消',
			handler : function() {
				$(this).closest('.window-body').dialog('destroy');
			}
		} ],
		onClose : function() {
			$(this).dialog('destroy');
		}
	});
}
(function($) {
	$.fn.serializeJson = function() {
		var serializeObj = {};
		var array = this.serializeArray();
		var str = this.serialize();
		$(array).each(
				function() {
					if (serializeObj[this.name]) {
						if ($.isArray(serializeObj[this.name])) {
							serializeObj[this.name].push(this.value);
						} else {
							serializeObj[this.name] = [
									serializeObj[this.name], this.value ];
						}
					} else {
						serializeObj[this.name] = this.value;
					}
				});
		return serializeObj;
	};
})(jQuery);
$(function() {
	var themeDiv = $("#styleMenu div div").click(
			function() {
				var themeName = $.trim($(this).html());
				if ("黑色" == themeName) {
					themeName = "black";
				} else if ("灰色" == themeName) {
					themeName = "bootstrap";
				} else {
					themeName = "default";
				}
				var $easyuiTheme = $('#easyuiTheme');
				var url = $easyuiTheme.attr('href');
				var href = url.substring(0, url.indexOf('themes')) + 'themes/'
						+ themeName + '/ui.css';
				$easyuiTheme.attr('href', href);
				$.cookie('easyuiThemeName', themeName, {
					expires : 7
				});
			});
});
serializeObject = function(form) {
	var o = {};
	$.each(form.serializeArray(), function(index) {
		if (o[this['name']]) {
			o[this['name']] = o[this['name']] + "," + this['value'];
		} else {
			o[this['name']] = this['value'];
		}
	});
	return o;
};
serializeObjectEx = function(form) {
	var o = {};
	$.each(form.serializeArray(), function(index) {
		if (o[this['name']]) {
			o[this['name']] = o[this['name']] + ","
					+ (this['value'] == '' ? ' ' : this['value']);
		} else {
			o[this['name']] = this['value'] == '' ? ' ' : this['value'];
		}
	});
	return o;
};
$.ajaxSetup({
	type : 'POST',
	error : function(XMLHttpRequest, textStatus, errorThrown) {
		$.messager.progress('close');
		$.messager.alert('错误', XMLHttpRequest.responseText);
	}
});
if ($.fn.panel) {
	$.fn.panel.defaults.loadingMessage = '加载中....';
}
if ($.fn.pagination) {
	$.fn.pagination.defaults.pageNumber = 1;
	$.fn.pagination.defaults.pageSize = 50;
	$.fn.pagination.defaults.checkOnSelect = !1;
	$.fn.pagination.defaults.selectOnCheck = !1;
	$.fn.pagination.defaults.beforePageText = '第';
	$.fn.pagination.defaults.afterPageText = '共{pages}页';
	$.fn.pagination.defaults.displayMsg = '显示{from}到{to},共{total}条';
}
if ($.fn.datagrid) {
	$.fn.datagrid.defaults.loadMsg = '正在处理，请稍待。。。';
	$.fn.datagrid.defaults.pagination = true;
	$.fn.datagrid.defaults.rownumbers = true;
	$.fn.datagrid.defaults.singleSelect = true;
	$.fn.datagrid.defaults.idField = 'id';
	$.fn.datagrid.defaults.pageSize = 50;
	$.fn.datagrid.defaults.pageList = [ 50, 100, 200 ];
	$.fn.datagrid.defaults.nowrap = true;
	$.fn.datagrid.defaults.striped = true;
	$.fn.datagrid.defaults.fitColumns = !1;
	$.fn.datagrid.defaults.pagePosition = 'bottom';
	$.fn.datagrid.defaults.fit = true;
}
if ($.fn.treegrid && $.fn.datagrid) {
	$.fn.treegrid.defaults.loadMsg = $.fn.datagrid.defaults.loadMsg;
	$.fn.treegrid.defaults.rownumbers = true;
	$.fn.treegrid.defaults.idField = 'id';
	$.fn.treegrid.defaults.treeField = 'name';
	$.fn.treegrid.defaults.fitColumns = !1;
	$.fn.treegrid.defaults.pagePosition = 'bottom';
	$.fn.treegrid.defaults.fit = true;
}
if ($.messager) {
	$.messager.defaults.ok = '确定';
	$.messager.defaults.cancel = '取消';
	$.messager.defaults.text = "加载中....";
}
if ($.fn.validatebox) {
	$.fn.validatebox.defaults.missingMessage = '该输入项为必输项';
	$.fn.validatebox.defaults.rules.email.message = '请输入有效的电子邮件地址';
	$.fn.validatebox.defaults.rules.url.message = '请输入有效的URL地址';
	$.fn.validatebox.defaults.rules.length.message = '输入内容长度必须介于{0}和{1}之间';
	$.fn.validatebox.defaults.rules.remote.message = '请修正该字段';
}
if ($.fn.numberbox) {
	$.fn.numberbox.defaults.missingMessage = '该输入项为必输项';
}
if ($.fn.combobox) {
	$.fn.combobox.defaults.missingMessage = '该输入项为必输项';
	$.fn.combobox.defaults.listHeight = 80;
	$.fn.combo.defaults.multiple = false;
}
if ($.fn.combotree) {
	$.fn.combotree.defaults.missingMessage = '该输入项为必输项';
	$.fn.combotree.defaults.multiple = true;
	$.fn.combotree.defaults.lines = true;
}
if ($.fn.combogrid) {
	$.fn.combogrid.defaults.missingMessage = '该输入项为必输项';
	$.fn.combogrid.defaults.pagination = true;
	$.fn.combogrid.defaults.rownumbers = true;
	$.fn.combogrid.defaults.multiple = true;
	$.fn.combogrid.defaults.nowrap = false;
	$.fn.combogrid.defaults.idField = 'id';
	$.fn.combogrid.defaults.pageSize = 10;
	$.fn.combogrid.defaults.pageList = [ 10, 20, 30, 40, 50 ];
	$.fn.combogrid.defaults.checkOnSelect = !0;
	$.fn.combogrid.defaults.selectOnCheck = !0;
	$.fn.combogrid.defaults.fitColumns = !1;
}
if ($.fn.calendar) {
	$.fn.calendar.defaults.weeks = [ '日', '一', '二', '三', '四', '五', '六' ];
	$.fn.calendar.defaults.months = [ '一月', '二月', '三月', '四月', '五月', '六月', '七月',
			'八月', '九月', '十月', '十一月', '十二月' ];
}
if ($.fn.datebox) {
	$.fn.datebox.defaults.currentText = '今天';
	$.fn.datebox.defaults.closeText = '关闭';
	$.fn.datebox.defaults.okText = '确定';
	$.fn.datebox.defaults.missingMessage = '该输入项为必输项';
	$.fn.datebox.defaults.formatter = function(date) {
		var y = date.getFullYear();
		var m = date.getMonth() + 1;
		var d = date.getDate();
		return y + '-' + (m < 10 ? ('0' + m) : m) + '-'
				+ (d < 10 ? ('0' + d) : d);
	};
	$.fn.datebox.defaults.parser = function(s) {
		if (!s)
			return new Date();
		var ss = s.split('-');
		var y = parseInt(ss[0], 10);
		var m = parseInt(ss[1], 10);
		var d = parseInt(ss[2], 10);
		if (!isNaN(y) && !isNaN(m) && !isNaN(d)) {
			return new Date(y, m - 1, d);
		} else {
			return new Date();
		}
	};
}
if ($.fn.datetimebox && $.fn.datebox) {
	$.extend($.fn.datetimebox.defaults, {
		currentText : $.fn.datebox.defaults.currentText,
		closeText : $.fn.datebox.defaults.closeText,
		okText : $.fn.datebox.defaults.okText,
		missingMessage : $.fn.datebox.defaults.missingMessage
	});
	$.fn.datetimebox.defaults.required = false;
	$.fn.datetimebox.defaults.showSeconds = true;
}
$.fn.panel.defaults.onBeforeDestroy = function() {
	var frame = $('iframe', this);
	try {
		if (frame.length > 0) {
			for (var i = 0; i < frame.length; i++) {
				frame[i].contentWindow.document.write('');
				frame[i].contentWindow.close();
			}
			frame.remove();
			if ($.browser.msie) {
				CollectGarbage();
			}
		}
	} catch (e) {
	}
};
easyuiErrorFunction = function(a) {
	$.messager.progress("close");
	$.messager.alert("错误", "通讯异常,请重试!");
}
$.fn.datagrid.defaults.onLoadError = easyuiErrorFunction;
$.fn.treegrid.defaults.onLoadError = easyuiErrorFunction;
$.fn.tree.defaults.onLoadError = easyuiErrorFunction;
$.fn.combogrid.defaults.onLoadError = easyuiErrorFunction;
$.fn.combobox.defaults.onLoadError = easyuiErrorFunction;
$.fn.form.defaults.onLoadError = easyuiErrorFunction;
var createGridHeaderContextMenu = function(e, field) {
	e.preventDefault();
	var grid = $(this);
	var headerContextMenu = this.headerContextMenu;
	if (!headerContextMenu) {
		var tmenu = $('<div style="width:100px;"></div>').appendTo('body');
		var fields = grid.datagrid('getColumnFields');
		for (var i = 0; i < fields.length; i++) {
			var fildOption = grid.datagrid('getColumnOption', fields[i]);
			if (!fildOption.hidden) {
				$('<div field="' + fields[i] + '"/>').html(fildOption.title)
						.appendTo(tmenu);
			} else {
				$('<div iconCls="icon-empty" field="' + fields[i] + '"/>')
						.html(fildOption.title).appendTo(tmenu);
			}
		}
		headerContextMenu = this.headerContextMenu = tmenu.menu({
			onClick : function(item) {
				var field = $(item.target).attr('field');
				if (item.iconCls == 'icon-ok') {
					grid.datagrid('hideColumn', field);
					$(this).menu('setIcon', {
						target : item.target,
						iconCls : 'icon-empty'
					});
				} else {
					grid.datagrid('showColumn', field);
					$(this).menu('setIcon', {
						target : item.target,
						iconCls : 'icon-ok'
					});
				}
			}
		});
	}
	headerContextMenu.menu('show', {
		left : e.pageX,
		top : e.pageY
	});
};
$.fn.datagrid.defaults.onHeaderContextMenu = createGridHeaderContextMenu;
$.fn.treegrid.defaults.onHeaderContextMenu = createGridHeaderContextMenu;
$.extend($.fn.validatebox.defaults.rules, {
	eqPwd : {
		validator : function(a, b) {
			return a == $(b[0]).val();
		},
		message : "密码不一致！"
	}
});
$.fn.tree.defaults.loadFilter = function(data, parent) {
	var opt = $(this).data().tree.options;
	var idFiled, textFiled, parentField;
	if (opt.parentField) {
		idFiled = opt.idFiled || 'id';
		textFiled = opt.textFiled || 'text';
		parentField = opt.parentField;
		var i, l, treeData = [], tmpMap = [];
		for (i = 0, l = data.length; i < l; i++) {
			tmpMap[data[i][idFiled]] = data[i];
		}
		for (i = 0, l = data.length; i < l; i++) {
			if (tmpMap[data[i][parentField]]
					&& data[i][idFiled] != data[i][parentField]) {
				if (!tmpMap[data[i][parentField]]['children'])
					tmpMap[data[i][parentField]]['children'] = [];
				data[i]['text'] = data[i][textFiled];
				tmpMap[data[i][parentField]]['children'].push(data[i]);
			} else {
				data[i]['text'] = data[i][textFiled];
				treeData.push(data[i]);
			}
		}
		return treeData;
	}
	return data;
};
$.fn.treegrid.defaults.loadFilter = function(data, parentId) {
	var opt = $(this).data().treegrid.options;
	var idFiled, textFiled, parentField;
	if (opt.parentField) {
		idFiled = opt.idFiled || 'id';
		textFiled = opt.textFiled || 'text';
		parentField = opt.parentField;
		var i, l, treeData = [], tmpMap = [];
		for (i = 0, l = data.length; i < l; i++) {
			tmpMap[data[i][idFiled]] = data[i];
		}
		for (i = 0, l = data.length; i < l; i++) {
			if (tmpMap[data[i][parentField]]
					&& data[i][idFiled] != data[i][parentField]) {
				if (!tmpMap[data[i][parentField]]['children'])
					tmpMap[data[i][parentField]]['children'] = [];
				data[i]['text'] = data[i][textFiled];
				tmpMap[data[i][parentField]]['children'].push(data[i]);
			} else {
				data[i]['text'] = data[i][textFiled];
				treeData.push(data[i]);
			}
		}
		return treeData;
	}
	return data;
};
$.fn.combotree.defaults.loadFilter = $.fn.tree.defaults.loadFilter;
easyuiPanelOnMove = function(a, b) {
	var e, f, g, h, i, j, c = a, d = b;
	1 > c && (c = 1), 1 > d && (d = 1), e = parseInt($(this).parent().css(
			"width")) + 14, f = parseInt($(this).parent().css("height")) + 14,
			g = c + e, h = d + f, i = $(window).width(),
			j = $(window).height(), g > i && (c = i - e), h > j && (d = j - f),
			$(this).parent().css({
				left : c,
				top : d
			});
};
$.fn.dialog.defaults.onMove = easyuiPanelOnMove;
$.fn.window.defaults.onMove = easyuiPanelOnMove;
$.fn.panel.defaults.onMove = easyuiPanelOnMove;
serializeObject = function(form) {
	var o = {};
	$.each(form.serializeArray(), function(index) {
		if (o[this['name']]) {
			o[this['name']] = o[this['name']] + "," + this['value'];
		} else {
			o[this['name']] = this['value'];
		}
	});
	return o;
};
serializeObjectEx = function(form) {
	var o = {};
	$.each(form.serializeArray(), function(index) {
		if (o[this['name']]) {
			o[this['name']] = o[this['name']] + ","
					+ (this['value'] == '' ? ' ' : this['value']);
		} else {
			o[this['name']] = this['value'] == '' ? ' ' : this['value'];
		}
	});
	return o;
};
formatString = function(str) {
	for (var i = 0; i < arguments.length - 1; i++) {
		str = str.replace("{" + i + "}", arguments[i + 1]);
	}
	return str;
};
stringToList = function(value) {
	if (value != undefined && value != '') {
		var values = [];
		var t = value.split(',');
		for (var i = 0; i < t.length; i++) {
			values.push('' + t[i]);
		}
		return values;
	} else {
		return [];
	}
};
jQuery.cookie = function(name, value, options) {
	if (typeof value != 'undefined') {
		options = options || {};
		if (value === null) {
			value = '';
			options.expires = -1;
		}
		var expires = '';
		if (options.expires
				&& (typeof options.expires == 'number' || options.expires.toUTCString)) {
			var date;
			if (typeof options.expires == 'number') {
				date = new Date();
				date.setTime(date.getTime()
						+ (options.expires * 24 * 60 * 60 * 1000));
			} else {
				date = options.expires;
			}
			expires = '; expires=' + date.toUTCString();
		}
		var path = options.path ? '; path=' + options.path : '';
		var domain = options.domain ? '; domain=' + options.domain : '';
		var secure = options.secure ? '; secure' : '';
		document.cookie = [ name, '=', encodeURIComponent(value), expires,
				path, domain, secure ].join('');
	} else {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie
							.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	}
};