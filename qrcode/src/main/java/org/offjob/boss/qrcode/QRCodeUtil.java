package org.offjob.boss.qrcode;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Binarizer;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.EncodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;

/**
 * 〈一句话功能简述〉<br>
 * 〈功能详细描述〉
 *
 * @author kai.fantasy . The type Qr code util.
 * @see [相关类/方法]（可选）
 * @since [产品 /模块版本] （可选）
 */
public class QRCodeUtil {

	static String format = "png";

	/**
	 * 二维码的生成
	 * 
	 * @throws WriterException
	 * @throws IOException
	 * 
	 */
	public static void createCode(String text, int width, int height, HttpServletResponse response)
			throws WriterException, IOException {
		Map hints = new HashMap();
		hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
		BitMatrix bitMatrix = new MultiFormatWriter().encode(text, BarcodeFormat.QR_CODE, width, height, hints);
		// 生成二维码
		MatrixToImageWriter.writeToStream(bitMatrix, format, response.getOutputStream());
		// MatrixToImageWriter.writeToFile(bitMatrix, format, new
		// File("D:/test.png"));
	}

	/**
	 * 二维码的解析
	 * 
	 * @param file
	 * @throws IOException
	 * @throws NotFoundException
	 */
	public static String parseCode(InputStream inputStream) throws IOException, NotFoundException {
		MultiFormatReader formatReader = new MultiFormatReader();
		BufferedImage image = ImageIO.read(inputStream);
		LuminanceSource source = new BufferedImageLuminanceSource(image);
		Binarizer binarizer = new HybridBinarizer(source);
		BinaryBitmap binaryBitmap = new BinaryBitmap(binarizer);
		Map hints = new HashMap();
		hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
		Result result = formatReader.decode(binaryBitmap, hints);
		return JSON.toJSONString(result);
	}

	public static void main(String[] args) throws NotFoundException {
		try {
			createCode("www.baidu.com", 300, 300, null);
			System.err.println(parseCode(new FileInputStream(new File("D:/test.png"))));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
